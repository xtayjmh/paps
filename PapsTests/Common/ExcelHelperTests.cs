﻿using DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Paps.Common.Tests
{
    [TestClass()]
    public class ExcelHelperTests
    {
        private PapsEntities _entities = new PapsEntities();
        [TestMethod()]
        public void ExportTest()
        {
            var date = "2018-10-09";
            InterfaceServices.Uri = new Uri("http://192.168.1.3:9999/sdservice.asmx");
            var list = InterfaceServices.GetDayReport(date, date);
            ExcelHelper.GenerateDailyReport(list, "D:\\daily.xlsx", "", "");
        }

        [TestMethod]
        public void ExportMonthlyReport()
        {
            var date = "2018-10";
            InterfaceServices.Uri = new Uri("http://192.168.1.185:9999/sdservice.asmx");
            var list = InterfaceServices.GetMonthReport(date, "");
            ExcelHelper.GenerateMonthlyReport(list, "D:\\Monthly.xlsx", date, "");
            Process.Start("D:\\Monthly.xlsx");
        }

        [TestMethod]
        public void ExportOnlineDetail()
        {
            var path = "D:\\Detail.xlsx";
            var createTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var list = new List<OnlineRateModel>();
            var startTime = "2018-10-22 00:00:00";
            var endTime = "2018-10-29 23:59:59";
            list = DatabaseServices.GetOnlineRate(startTime, endTime, "", "");
            var unit = "h";
            var model = list.GroupBy(l => l.RegionName);
            var result = model.Select(x =>
                new OnlineRateModel
                {
                    RegionName = x.Key,
                    PointName = x.Count(y => y.RegionName == x.Key).ToString(),
                    OnlineHours = x.Sum(o => o.OnlineHours),
                    TotalHours = x.Sum(t => t.TotalHours),
                    OnlineRate = Math.Round(x.Sum(o => o.OnlineHours) / x.Sum(t => t.TotalHours) * 100, 2),
                    RegionId = x.FirstOrDefault()?.RegionId
                }).ToList();
            result.Add(new OnlineRateModel()
            {
                RegionName = "总计",
                PointName = result.Sum(i => int.Parse(i.PointName)).ToString(),
                OnlineHours = result.Sum(i => i.OnlineHours),
                TotalHours = result.Sum(i => i.TotalHours),
                OnlineRate = Math.Round(result.Sum(i => i.OnlineHours) / result.Sum(i => i.TotalHours) * 100, 2) //todo 尝试除以零
            });
            ExcelHelper.GenerateDetailReport(list, path, startTime, endTime);
            Process.Start(path);
        }

        [TestMethod]
        public void ExportSummary()
        {
            var path = "D:\\Summary.xlsx";
            var createTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var list = new List<OnlineRateModel>();
            var startTime = "2018-10-22 00:00:00";
            var endTime = "2018-10-29 23:59:59";
            list = DatabaseServices.GetOnlineRate(startTime, endTime, "", "");
            var unit = "h";
            var model = list.GroupBy(l => l.RegionName);
            var result = model.Select(x =>
                new OnlineRateModel
                {
                    RegionName = x.Key,
                    PointName = x.Count(y => y.RegionName == x.Key).ToString(),
                    OnlineHours = x.Sum(o => o.OnlineHours),
                    TotalHours = x.Sum(t => t.TotalHours),
                    OnlineRate = Math.Round(x.Sum(o => o.OnlineHours) / x.Sum(t => t.TotalHours) * 100, 2),
                    RegionId = x.FirstOrDefault()?.RegionId
                }).ToList();
            result.Add(new OnlineRateModel()
            {
                RegionName = "总计",
                PointName = result.Sum(i => int.Parse(i.PointName)).ToString(),
                OnlineHours = result.Sum(i => i.OnlineHours),
                TotalHours = result.Sum(i => i.TotalHours),
                OnlineRate = Math.Round(result.Sum(i => i.OnlineHours) / result.Sum(i => i.TotalHours) * 100, 2) //todo 尝试除以零
            });
            ExcelHelper.GenerateDetailReport(result, path, startTime, endTime);
            Process.Start(path);

        }
        [TestMethod]
        public void ExportOneKeyReport()
        {
            InterfaceServices.Uri = new Uri("http://192.168.1.3:9999/sdservice.asmx");

            var startTime = new DateTime(2018, 1, 1, 0, 0, 0);
            var dailyData = InterfaceServices.GetDailyDetail("", "", "", "2018");
            var monthData = new List<MonthDetail>();
            //按照卡号去重取出来所有的用户，添加到年，月视图中
            dailyData.GroupBy(i => i.CardNo).ToList().ForEach(i =>
            {
                //初始化一个月数据视图，其中日信息是空的
                monthData.Add(new MonthDetail()
                {
                    CardNo = i.Key,
                    DailyDetails = new List<DailyDetail>(),
                    Name = i.FirstOrDefault()?.Name
                });
            });
            for (int i = 0; i <= 11; i++) //十二个月
            {
                var st = startTime.AddMonths(i);//月开始时间
                var et = startTime.AddMonths(i + 1).AddSeconds(-1);//月结束时间
                var days = et.Subtract(st).Days;
                for (int j = 0; j <= days; j++)
                {
                    var sd = st.AddDays(j);//开始天
                    var ed = st.AddDays(j + 1).AddSeconds(-1);//结束天
                    var list = dailyData.Where(d => d.StartTime >= sd && d.StartTime <= ed).ToList(); //这个月的所有数据

                    //todo 这里的算法不很完美，处理的比较的慢的话需要优化

                    if (list.Any())
                    {
                        list.ForEach(l =>
                        {
                            l.Month = i + 1;
                            l.DayIndex = j + 1;
                            var monthDetail = monthData.FirstOrDefault(m => m.CardNo == l.CardNo);
                            if (monthDetail != null && l.EndTime != l.StartTime) monthDetail.DailyDetails.Add(l);
                            //                        var userYear = yearData.FirstOrDefault(y => y.CardNo == l.CardNo);
                            //                        userYear?.MonthDetails.Add(monthDetail);
                        });
                    }
                }
            }

            var path = "D:\\OneKeySummary.xlsx";
            ExcelHelper.GenerateOneKeyReport(dailyData, monthData,"测试工点","2018", path);
            Process.Start(path);
        }
    }
}