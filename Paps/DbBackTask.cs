﻿using FluentScheduler;
using Model;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace Paps
{
    public class DbBackJob : Registry
    {
        public DbBackJob()
        {
            int Interval = int.Parse(ConfigurationManager.AppSettings["Interval"]);
            Schedule<DoBack>().ToRunEvery(Interval).Days().At(00, 00); //每天凌晨00:00执行
        }
    }
    public class DoBack : IJob
    {
        void IJob.Execute()
        {
            int Amount = int.Parse(ConfigurationManager.AppSettings["Amount"]);
            var filePath = @"D:\DbBackUp";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            var folder = new DirectoryInfo(filePath);
            var list = folder.GetFiles("*.bak");
            if (list.Length >= Amount)
            {
                try
                {
                    var fileInfo = list.OrderByDescending(i => i.CreationTime).ToList().Last();
                    fileInfo.Delete();
                }
                catch { }
            }
            var fileName = DateTime.Now.ToFileTime() + ".bak";
            var cmdText = @"backup database Paps to disk= '" + filePath + "\\" + fileName + "'";
            DbBackTask.BakReductSql(cmdText, true);
        }
    }
    public class DbBackTask
    {
        /// <summary>
        /// 对数据库的备份和恢复操作，Sql语句实现
        /// </summary>
        /// <param name="cmdText">实现备份或恢复的Sql语句</param>
        /// <param name="isBak">该操作是否为备份操作，是为true否，为false</param>
        public static string BakReductSql(string cmdText, bool isBak)
        {
            using (var db = new PapsEntities())
            {
                SqlCommand cmdBakRst = new SqlCommand();
                SqlConnection conn = (SqlConnection)db.Database.Connection;
                try
                {
                    conn.Open();
                    cmdBakRst.Connection = conn;
                    cmdBakRst.CommandType = CommandType.Text;
                    if (!isBak)     //如果是恢复操作
                    {
                        string setOffline = "Alter database Paps Set Offline With rollback immediate ";
                        string setOnline = " Alter database Paps Set Online With Rollback immediate";
                        cmdBakRst.CommandText = setOffline + cmdText + setOnline;
                    }
                    else
                    {
                        cmdBakRst.CommandText = cmdText;
                    }
                    cmdBakRst.ExecuteNonQuery();
                    if (!isBak)
                    {
                        cmdBakRst.Dispose();
                        return "恭喜你，数据成功恢复为所选文档的状态！";
                    }
                    else
                    {
                        cmdBakRst.Dispose();
                        return "恭喜，你已经成功备份当前数据！";
                    }
                }
                catch (SqlException sexc)
                {
                    cmdBakRst.Dispose();
                    return "失败，可能是对数据库操作失败，原因：" + sexc;
                }
                catch (Exception ex)
                {
                    cmdBakRst.Dispose();
                    return "对不起，操作失败，可能原因：" + ex;
                }
            }
        }
    }
}
