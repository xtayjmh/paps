﻿using Model;

namespace Paps
{
    /// <summary>
    /// 
    /// </summary>
    public class SocketInstance
    {
        /// <summary>
        /// 给客户端发送消息
        /// </summary>
        /// <param name="msg">SocketMsg</param>
        /// <param name="isCallBack">SocketMsg</param>
        public static void SendMsgToClient(SocketMsg msg, bool isCallBack = true)
        {
            SocketConnect.Instance.SentMessage(msg, isCallBack);
        }
    }
}