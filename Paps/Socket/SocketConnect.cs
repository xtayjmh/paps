﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paps.SignalR;

namespace Paps
{
    public class SocketConnect
    {
        private static SocketConnect _instance;
        public string SelfId { get; set; }
        public static SocketConnect Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SocketConnect();
                }
                return _instance;
            }
        }
        public event Action NotConnectEvent;
        public event Action OnConnectedEvent;
        public event Action OnConnectingEvent;
        public event Action OnLoseConnectingEvent;

        public event Action<SocketMsg> OnWelcomeEvent;

        public bool Connected { get; private set; }
        public string IP { get; private set; }
        private Connection connection_ = new Connection(); // Connection to the device.
        private System.Timers.Timer timer = new System.Timers.Timer();

        public SocketConnect()
        {
            connection_.NotConnect += Connection__NotConnect;
            connection_.OnEventReceived += Connection__OnEventReceived;
        }
        public void StartCheck()
        {
            timer.Interval = 3000D;
            timer.Enabled = true;
            timer.Elapsed += (o, a) =>
            {
                if (!connection_.CheckConnected())
                {
                    OnLoseConnectingEvent?.Invoke();
                    timer.Enabled = false;
                    timer.Stop();
                }
            };
            timer.Enabled = true;
            timer.Start();
        }
        private void Connection__OnEventReceived(PlateSocketMsg meg)
        {
            if (meg == null)
            {
                return;
            }
            var hub = new ChatHub();
            hub.SocketSend(meg);

        }
        private void Connection__NotConnect()
        {
            NotConnectEvent?.Invoke();
        }
        public bool Connect(string ip)
        {
            OnConnectingEvent?.Invoke();
            IP = ip;
            Connected = connection_.Connect(ip);
            if (Connected)
            {
                StartCheck();
                IP = ip;
                OnConnectedEvent?.Invoke();
            }
            else
            {
                NotConnectEvent?.Invoke();
            }
            return Connected;
        }
        public void Close()
        {
            connection_.Close();
        }
        public bool SentMessage(SocketMsg msg, bool isCallBack = true)
        {
            return connection_.SendCommand(msg, isCallBack);
        }
    }
}
