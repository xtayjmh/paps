﻿using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Paps.Common;

namespace Paps
{
    public class Connection
    {

        public const int DEFAULT_CONTROL_PORT = 44444;
        public const int DEFAULT_DATA_PORT = 44444;
        public const int DEFAULT_TIMEOUT = 300;  // Timeout, in seconds.
        private byte[] control_ResponseDataBuffer = new byte[1024 * 9000];
        private byte[] data_ResponseDataBuffer = new byte[1024 * 9000];
        public event Action NotConnect;
        public event Action LoseConnect;
        System.Timers.Timer timer = new System.Timers.Timer();
        private object lockObj = new object();


        public Connection()
        {

        }
        public bool CheckConnected()
        {
            try
            {
                if (Control_socket_ == null)
                {
                    return false;
                }
                return Control_socket_.Connected;
            }
            catch
            {
                return false;
            }
        }

        public bool Connect(string ip, int timeout = DEFAULT_TIMEOUT)
        {
            try
            {
                lock (lockObj)
                {
                    if (Control_socket_ != null && Control_socket_.Connected)
                    {
                        return true;
                    }

                    IPEndPoint point = new IPEndPoint(IPAddress.Parse(ip), DEFAULT_CONTROL_PORT);
                    Control_socket_ = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                    Control_socket_.ReceiveTimeout = timeout;
                    Control_socket_.ReceiveBufferSize = control_ResponseDataBuffer.Length;
                    Control_socket_.SendBufferSize = control_ResponseDataBuffer.Length;
                    control_ResponseDataBuffer = new byte[1024 * 9000];
                    Control_socket_.Connect(point);
                    Control_socket_.BeginReceive(control_ResponseDataBuffer, 0, control_ResponseDataBuffer.Length, SocketFlags.None, new AsyncCallback(OnControl_DataReceived), null);

                    connection_open_ = true;
                    return true;
                }


            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void Close()
        {
            Control_socket_.Close();
            connection_open_ = false;
        }

        public bool SendCommand(SocketMsg cmd, bool isCallBack)
        {
            if (connection_open_ == true)
            {

                var msg = $"HandleWebInfo:{JsonConvert.SerializeObject(cmd)}";
                if (isCallBack == true)
                {
                    msg = $"TransCmd:{JsonConvert.SerializeObject(cmd)}";
                }
                QueuePacket(msg);
                return true;
            }
            else
            {
                NotConnect?.Invoke();
                return false;
            }
        }


        private void QueuePacket(byte[] data)
        {
            Control_socket_.Send(data);
        }
        private void QueuePacket(string msg)
        {
            try
            {
                var sendByte = System.Text.Encoding.GetEncoding("GBK").GetBytes(msg + "\r\n");
                Control_socket_.Send(sendByte);
            }
            catch (Exception e)
            {
                Control_socket_.Close();
            }
        }
        List<byte> tempBuffer = new List<byte>();
        private void ProcessDataEventReceived(byte[] data)
        {
            try
            {
                var strCommand = System.Text.Encoding.UTF8.GetString(data);
                strCommand = strCommand.TrimStart("#*".ToArray());
                strCommand = strCommand.Replace("*#", "");
                var evt = JsonConvert.DeserializeObject<PlateSocketMsg>(strCommand);
                if (evt != null)
                {
                    OnEventReceived?.Invoke(evt);
                }
            }
            catch (Exception e)
            {
                //Common.LogHelper.WriteLog("********************OnControl_DataReceived ProcessDataEventReceived***********************", e);
            }

        }
        private void OnControl_DataReceived(IAsyncResult ar)
        {
            try
            {
                if (Control_socket_ != null && Control_socket_.Connected)
                {
                    var receivedLength = Control_socket_.EndReceive(ar);
                    tempBuffer.AddRange(control_ResponseDataBuffer.Take(receivedLength));
                    var availableNext = Control_socket_.Available;
                    if (availableNext == 0 && (tempBuffer.LastOrDefault() == 35 || tempBuffer.LastOrDefault() == 10))
                    {
                        var temData = new byte[tempBuffer.Count];
                        tempBuffer.CopyTo(temData);
                        tempBuffer.Clear();
                        Task.Factory.StartNew(() =>
                        {
                            ProcessDataEventReceived(temData);
                        });
                    }
                }
            }
            catch (Exception e)
            {
            }
            try
            {

                control_ResponseDataBuffer = new byte[control_ResponseDataBuffer.Length];// clear the buffer
                Control_socket_.BeginReceive(control_ResponseDataBuffer, 0, control_ResponseDataBuffer.Length, SocketFlags.None, new AsyncCallback(OnControl_DataReceived), null);
            }
            catch (Exception e)
            {
                Close();
            }
        }
        public event Action<PlateSocketMsg> OnEventReceived;
        public bool connection_open_;
        public static Socket Control_socket_;
    }
}
