﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Paps.Common;
using FluentScheduler;
using WebGrease.Activities;
using Microsoft.AspNet.SignalR;
using System.Net.Sockets;

namespace Paps
{
    /// <summary>
    /// 
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        //private static string ServerPath = HttpContext.Current.Server.MapPath("~");
        //private static string ip = "";
        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(7200); //24小时不超时
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(5); //5秒发送一次保持连接的消息
            JobManager.Initialize(new DbBackJob()); //定时备份数据库，每天凌晨00:00执行
            JobManager.Initialize(new CheckOnLine()); //5分钟轮询判断客户端是否开启
            JobManager.Initialize(new SyncStructure()); //每天凌晨2点同步组织架构
            //string HostName = Dns.GetHostName(); //得到主机名
            //IPHostEntry IpEntry = Dns.GetHostEntry(HostName);
            //for (int i = 0; i < IpEntry.AddressList.Length; i++)
            //{
            //    //从IP地址列表中筛选出IPv4类型的IP地址
            //    //AddressFamily.InterNetwork表示此IP为IPv4,
            //    //AddressFamily.InterNetworkV6表示此地址为IPv6类型
            //    if (IpEntry.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        ip = IpEntry.AddressList[i].ToString();
            //    }
            //}
            var confIp = ConfigurationManager.AppSettings["socketAddr"];
            SocketConnect.Instance.Connect(confIp); //和Socket建立连接
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Error(object sender, EventArgs e)
        {
            //ExceptionHandler.Handle(Request, Server.GetLastError());
        }
    }
}
