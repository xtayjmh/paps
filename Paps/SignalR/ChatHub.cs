﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Model;
using Newtonsoft.Json;
using Paps.Common;

namespace Paps.SignalR
{
    /// <summary>
    /// 
    /// </summary>
    [HubName("chat")]
    public class ChatHub : Hub
    {
        /// <summary>
        /// 
        /// </summary>
        public static ConcurrentDictionary<string, string> OnLineUsers = new ConcurrentDictionary<string, string>();
        /// <summary>
        /// 用于记录最近接收到的消息
        /// </summary>
        public static ConcurrentDictionary<QueryModelMethodEnum, DateTime> MethodHistory = new ConcurrentDictionary<QueryModelMethodEnum, DateTime>();

        /// <summary>
        /// 群发消息
        /// </summary>
        /// <param name="message"></param>
        [HubMethodName("send")]
        public void Send(string message)
        {
            string clientName = OnLineUsers[Context.ConnectionId];
            message = HttpUtility.HtmlEncode(message)?.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
            Clients.All.receiveMessage(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), clientName, message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        [HubMethodName("keepAlive")]
        public void KeepAliveMethod(string msg)
        {

        }

        private static object o = new object();
        /// <summary>
        /// 往页面发送消息
        /// </summary>
        /// <param name="socketMsg"></param>
        [HubMethodName("SocketSend")]
        public void SocketSend(PlateSocketMsg socketMsg)
        {
            var receiveTime = MethodHistory.FirstOrDefault(h => h.Key == socketMsg.QueryModel.MethodName).Value;
            if (DateTime.Now.Subtract(receiveTime).TotalSeconds < 3)
            {
                return;
            }
            MethodHistory.AddOrUpdate(socketMsg.QueryModel.MethodName, DateTime.Now, (key, value) => DateTime.Now);

            int n = -1;
            try
            {
                lock (o)
                {
                    using (PapsEntities db = new PapsEntities())
                    {
                        //var clientData = db.ClientData.AsNoTracking().OrderByDescending(c => c.TableId).FirstOrDefault(c => c.DeviceCode == socketMsg.DeviceCode) ?? new ClientData { ClientId = socketMsg.DeviceCode, DeviceCode = socketMsg.DeviceCode };
                        var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
                        var clientId = socketMsg.SignId;
                        if (clientId == null && socketMsg.QueryModel.MethodName != QueryModelMethodEnum.GetBackGroundImage)
                        {
                            if (socketMsg.QueryModel.MethodName == QueryModelMethodEnum.GetYearData) Logger.Debug("收到消息：" + JsonConvert.SerializeObject(socketMsg));
                            return;
                        }
                        //这里根据返回的方法，调用不同的前台方法显示对应的内容
                        switch (socketMsg.QueryModel.MethodName)
                        {
                            //部门列表
                            case QueryModelMethodEnum.GetDeptList:
                                context.Clients.Client(clientId).SetDept(socketMsg.Result);
                                //clientData.DeptList = socketMsg.Result;
                                break;
                            //职务列表
                            case QueryModelMethodEnum.GetTitleList:
                                context.Clients.Client(clientId).SetTitle(socketMsg.Result);
                                //clientData.TitleList = socketMsg.Result;
                                break;
                            //基站列表
                            case QueryModelMethodEnum.GetBaseStationList:
                                context.Clients.Client(clientId).SetBaseStation(socketMsg.Result);
                                break;
                            //月考勤报表
                            case QueryModelMethodEnum.GetMonthReport:
                                context.Clients.Client(clientId).SetMonthReport(socketMsg.Result);
                                //clientData.MonthData = socketMsg.Result;
                                break;
                            //日考勤报表
                            case QueryModelMethodEnum.GetDayReport:
                                context.Clients.Client(clientId).SetDayReport(socketMsg.Result);
                                //clientData.DailyData = socketMsg.Result;
                                break;
                            //系统报警总数
                            case QueryModelMethodEnum.GetSysAlarmCount:
                                context.Clients.Client(clientId).SetSysAlarmCount(socketMsg.Result);
                                break;
                            //系统报警详细信息
                            case QueryModelMethodEnum.GetSysAlarmData:
                                context.Clients.Client(clientId).SetSysAlarmData(socketMsg.Result);
                                break;
                            //超员报警总数
                            case QueryModelMethodEnum.GetOverPerAlarmCount:
                                context.Clients.Client(clientId).SetOverPerAlarmCount(socketMsg.Result);
                                break;
                            //超员报警详细信息
                            case QueryModelMethodEnum.GetOverPerAlarmData:
                                context.Clients.Client(clientId).SetOverPerAlarmData(socketMsg.Result);
                                break;
                            //一键查询年视图
                            case QueryModelMethodEnum.GetYearData:
                                context.Clients.Client(clientId).SetYearData(socketMsg.Result);
                                //clientData.YearData = socketMsg.Result;
                                break;
                            //一键查询月视图
                            case QueryModelMethodEnum.GetMonthData:
                                Thread.Sleep(1000); //加延迟是为了防止页面还没有加载出来就把数据推出去，显示不出来。
                                context.Clients.Client(clientId).SetMonthData(socketMsg.Result);
                                break;
                            //一键查询日视图
                            case QueryModelMethodEnum.GetDayReportByCode:
                                context.Clients.Client(clientId).SetDayData(socketMsg.Result);
                                break;
                            //导出一键查询
                            case QueryModelMethodEnum.GetExcelPath:
                                context.Clients.Client(clientId).ReturnExcel(socketMsg.QueryModel.FileName);
                                break;
                            //配置信息
                            case QueryModelMethodEnum.GetConfigData:
                                context.Clients.Client(clientId).SetConfigData(socketMsg.Result);
                                //if (socketMsg.Result != "\"\"")
                                //{
                                //    clientData.ConfigData = socketMsg.Result;
                                //}
                                break;
                            //定位信息
                            case QueryModelMethodEnum.GetLocationData:
                                context.Clients.Client(clientId).SetLocationData(socketMsg.Result);
                                //clientData.LocationData = socketMsg.Result;
                                break;
                            //分类信息
                            case QueryModelMethodEnum.GetTypes:
                                if (!socketMsg.CanConnect && socketMsg.Result == null)
                                {
                                    context.Clients.Client(clientId).NoTypes(socketMsg.Result);
                                }
                                else
                                {
                                    context.Clients.Client(clientId).SetTypes(socketMsg.Result);
                                }
                                //clientData.TypeList = socketMsg.Result;
                                break;
                            case QueryModelMethodEnum.GetBackGroundImage:
                                context.Clients.All.SetBackGroundImage(socketMsg.Result);
                                break;
                        }
                        //db.ClientData.AddOrUpdate(clientData);
                        //n = db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //ignore
            }


        }
        /// <summary>
        /// 给指定用户发消息
        /// </summary>
        /// <param name="toUserId"></param>
        /// <param name="message"></param>
        [HubMethodName("sendOne")]
        public void Send(string toUserId, string message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();

            string clientName = OnLineUsers[Context.ConnectionId];
            message = HttpUtility.HtmlEncode(message)?.Replace("\r\n", "<br/>").Replace("\n", "<br/>");
            Clients.Caller.receiveMessage(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Format("您对 {1}", clientName, OnLineUsers[toUserId]), message);
            context.Clients.Client(toUserId).receiveMessage(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Format("{0} 对您", clientName), message);
        }
        /// <summary>
        /// 建立连接
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            string clientId = Context.QueryString["clientId"];
            string clientName = Context.QueryString["clientName"];
            OnLineUsers.AddOrUpdate(Context.ConnectionId, clientName, (key, value) => clientName);
            Clients.All.userChange(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Format("{0} 加入了。", clientName), OnLineUsers.ToArray());
            return base.OnConnected();
        }
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            string clientId = Context.QueryString["clientId"];
            string clientName = Context.QueryString["clientName"];

            Clients.All.userChange(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Format("{0} 离开了。", clientName), OnLineUsers.ToArray());
            OnLineUsers.TryRemove(Context.ConnectionId, out clientName);
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// 给Socket服务器发送请求
        /// </summary>
        [HubMethodName("GetData")]
        public void GetData(SocketMsg msg)
        {
            SocketInstance.SendMsgToClient(msg);
        }
    }
}