var jq = jQuery.noConflict();
jq(document).ready(function () {

	/*-----------------------------------/
	/*	TOP NAVIGATION AND LAYOUT
	/*----------------------------------*/

	jq('.btn-toggle-fullwidth').on('click', function() {
		if(!jq('body').hasClass('layout-fullwidth')) {
			jq('body').addClass('layout-fullwidth');

		} else {
			jq('body').removeClass('layout-fullwidth');
			jq('body').removeClass('layout-default'); // also remove default behaviour if set
		}

		jq(this).find('.lnr').toggleClass('lnr-arrow-left-circle lnr-arrow-right-circle');

		if(jq(window).innerWidth() < 1025) {
			if(!jq('body').hasClass('offcanvas-active')) {
				jq('body').addClass('offcanvas-active');
			} else {
				jq('body').removeClass('offcanvas-active');
			}
		}
	});

	jq(window).on('load', function() {
		if(jq(window).innerWidth() < 1025) {
			jq('.btn-toggle-fullwidth').find('.icon-arrows')
			.removeClass('icon-arrows-move-left')
			.addClass('icon-arrows-move-right');
		}

		// adjust right sidebar top position
		jq('.right-sidebar').css('top', jq('.navbar').innerHeight());

		// if page has content-menu, set top padding of main-content
		if(jq('.has-content-menu').length > 0) {
			jq('.navbar + .main-content').css('padding-top', jq('.navbar').innerHeight());
		}

		// for shorter main content
		if(jq('.main').height() < jq('#sidebar-nav').height()) {
			jq('.main').css('min-height', jq('#sidebar-nav').height());
		}
	});


	/*-----------------------------------/
	/*	SIDEBAR NAVIGATION
	/*----------------------------------*/

	jq('.sidebar a[data-toggle="collapse"]').on('click', function() {
		if(jq(this).hasClass('collapsed')) {
			jq(this).addClass('active');
		} else {
			jq(this).removeClass('active');
		}
	});

	if( jq('.sidebar-scroll').length > 0 ) {
		jq('.sidebar-scroll').slimScroll({
			height: '95%',
			wheelStep: 2,
		});
	}


	/*-----------------------------------/
	/*	PANEL FUNCTIONS
	/*----------------------------------*/

	// panel remove
	jq('.panel .btn-remove').click(function(e){

		e.preventDefault();
		jq(this).parents('.panel').fadeOut(300, function(){
			jq(this).remove();
		});
	});

	// panel collapse/expand
	var affectedElement = jq('.panel-body');

	jq('.panel .btn-toggle-collapse').clickToggle(
		function(e) {
			e.preventDefault();

			// if has scroll
			if( jq(this).parents('.panel').find('.slimScrollDiv').length > 0 ) {
				affectedElement = jq('.slimScrollDiv');
			}

			jq(this).parents('.panel').find(affectedElement).slideUp(300);
			jq(this).find('i.lnr-chevron-up').toggleClass('lnr-chevron-down');
		},
		function(e) {
			e.preventDefault();

			// if has scroll
			if( jq(this).parents('.panel').find('.slimScrollDiv').length > 0 ) {
				affectedElement = jq('.slimScrollDiv');
			}

			jq(this).parents('.panel').find(affectedElement).slideDown(300);
			jq(this).find('i.lnr-chevron-up').toggleClass('lnr-chevron-down');
		}
	);


	/*-----------------------------------/
	/*	PANEL SCROLLING
	/*----------------------------------*/

	if( jq('.panel-scrolling').length > 0) {
		jq('.panel-scrolling .panel-body').slimScroll({
			height: '430px',
			wheelStep: 2,
		});
	}

	if( jq('#panel-scrolling-demo').length > 0) {
		jq('#panel-scrolling-demo .panel-body').slimScroll({
			height: '175px',
			wheelStep: 2,
		});
	}

	/*-----------------------------------/
	/*	TODO LIST
	/*----------------------------------*/

	jq('.todo-list input').change( function() {
		if( jq(this).prop('checked') ) {
			jq(this).parents('li').addClass('completed');
		}else {
			jq(this).parents('li').removeClass('completed');
		}
	});


	/*-----------------------------------/
	/* TOASTR NOTIFICATION
	/*----------------------------------*/

	if(jq('#toastr-demo').length > 0) {
		toastr.options.timeOut = "false";
		toastr.options.closeButton = true;
		toastr['info']('Hi there, this is notification demo with HTML support. So, you can add HTML elements like <a href="#">this link</a>');

		jq('.btn-toastr').on('click', function() {
			jqcontext = jq(this).data('context');
			jqmessage = jq(this).data('message');
			jqposition = jq(this).data('position');

			if(jqcontext == '') {
				jqcontext = 'info';
			}

			if(jqposition == '') {
				jqpositionClass = 'toast-left-top';
			} else {
				jqpositionClass = 'toast-' + jqposition;
			}

			toastr.remove();
			toastr[jqcontext](jqmessage, '' , { positionClass: jqpositionClass });
		});

		jq('#toastr-callback1').on('click', function() {
			jqmessage = jq(this).data('message');

			toastr.options = {
				"timeOut": "300",
				"onShown": function() { alert('onShown callback'); },
				"onHidden": function() { alert('onHidden callback'); }
			}

			toastr['info'](jqmessage);
		});

		jq('#toastr-callback2').on('click', function() {
			jqmessage = jq(this).data('message');

			toastr.options = {
				"timeOut": "10000",
				"onclick": function() { alert('onclick callback'); },
			}

			toastr['info'](jqmessage);

		});

		jq('#toastr-callback3').on('click', function() {
			jqmessage = jq(this).data('message');

			toastr.options = {
				"timeOut": "10000",
				"closeButton": true,
				"onCloseClick": function() { alert('onCloseClick callback'); }
			}

			toastr['info'](jqmessage);
		});
	}
});

// toggle function
jq.fn.clickToggle = function( f1, f2 ) {
	return this.each( function() {
		var clicked = false;
		jq(this).bind('click', function() {
			if(clicked) {
				clicked = false;
				return f2.apply(this, arguments);
			}

			clicked = true;
			return f1.apply(this, arguments);
		});
	});

}


