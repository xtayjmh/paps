﻿using DAL;
using Model;
using Newtonsoft.Json;
using Paps.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TreeView = Model.TreeView;

namespace Paps.Areas.Systems.Controllers
{
    public class AccessController : Controller
    {
        // GET: Systems/Access
        public ActionResult Index()
        {
            using (var db = new PapsEntities())
            {
                var entity = db.SysAccess.ToList();
                if (!entity.Any()) entity = new List<SysAccess>()
                {
                    new SysAccess{ID=0},
                    new SysAccess{ID=1}
                };
                return View(entity);
            }
        }
        [HttpPost]
        public ActionResult Index(List<SysAccess> model)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    model.ForEach(i =>
                    {
                        var entity = db.SysAccess.FirstOrDefault(p => p.ID == i.ID);
                        if (entity == null)
                        {
                            db.SysAccess.Add(i);
                        }
                        else
                        {
                            entity.AppID = i.AppID;
                            entity.AppSecret = i.AppSecret;
                            entity.ApiKey = i.ApiKey;
                        }

                    });
                    var result = db.SaveChanges();
                    if (result >= 0)
                    {
                        TempData["result"] = "true";
                    }
                    else
                    {
                        TempData["result"] = "false";
                    }
                }
            }
            catch (Exception)
            {
                TempData["result"] = "false";
            }
            return View(model);
        }
        public ActionResult OnlineRate()
        {
            ViewBag.List = "";
            return View();
        }
        /// <summary>
        /// 获取在线率
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="selectNode">部门编号</param>
        /// <param name="workPoint">工点编号</param>
        /// <param name="pointTxt">工点文字模糊搜索</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetOnlineRate(string startTime, string endTime, string selectNode, string workPoint, string pointTxt)
        {
            ViewBag.StartTime = startTime;
            ViewBag.EndTime = endTime;
            ViewBag.WorkPoint = workPoint;
            ViewBag.SelectNode = selectNode;
            ViewBag.PointTxt = pointTxt;
            var listIds = "";
            if (!string.IsNullOrEmpty(workPoint))
            {
                var listId = new List<int> { int.Parse(workPoint) };
                listIds = GetRelatedNodeId(listId, int.Parse(workPoint));
            }
            var list = DatabaseServices.GetOnlineRate(startTime + " 00:00:00", endTime + " 23:59:59", pointTxt, listIds);
            Session["DetailReport"] = list;
            return View("OnlineRate", list);
        }
        //递归获取相关的用户id
        private string GetRelatedNodeId(List<int> listId, int id)
        {
            using (var db = new PapsEntities())
            {
                var list = db.Menus.Where(i => i.ParentID == id).Select(i => i.ID).ToList();
                if (!list.Any()) return string.Join(",", listId);
                list.ForEach(i =>
                {
                    listId.Add(i);
                    GetRelatedNodeId(listId, i);
                }
                );
            }
            return string.Join(",", listId);
        }

        [HttpGet]
        public string GetTree(int isSummary = 0)
        {
            var user = (Users)Session["UserInfo"];
            if (user == null) user = new Users { UserId = 447 };

            var treeViewList = new List<TreeView>();
            using (var db = new PapsEntities())
            {
                var roleUser = db.RoleUser.Where(i => i.UserID == user.UserId).Select(i => i.RoleID).ToList();
                var arrRole = db.ArrRoles.Where(i => roleUser.Contains(i.ID)).Select(i => i.RoleID).ToList();
                var roleMenu = db.RoleMenu.Where(i => arrRole.Contains(i.RoleID)).Select(i => i.MenuID).ToList();
                var topMenus = new List<Menus>();
                if (roleMenu.Contains(1))
                {
                    topMenus = db.Menus.Where(p => p.ParentID == 0 && p.IsDel == false && roleMenu.Contains(p.ID)).ToList(); //有默认控制中心权限
                }
                else
                {
                    topMenus = db.Menus.Where(p => p.ParentID == 1 && p.IsDel == false && roleMenu.Contains(p.ID)).ToList(); //没有默认控制中心权限
                }
                topMenus.ForEach(i => treeViewList.Add(new TreeView() { id = i.ID, text = i.Name, ParentId = 0 }));
                GetTreeView(treeViewList, roleMenu, isSummary);
            }
            return JsonConvert.SerializeObject(treeViewList);
        }

        private static void GetTreeView(List<TreeView> treeViewList, List<int> roleMenus, int isSummary = 0)
        {
            using (var pe = new PapsEntities())
            {
                treeViewList.ForEach(i =>
                {
                    var sonNodes = pe.Menus.Where(j => j.ParentID == i.id && roleMenus.Contains(j.ID)).ToList();
                    if (isSummary != 0) //如果是汇总的话，不显示工点级
                    {
                        sonNodes = sonNodes.Where(j => j.StructureType != 4).ToList();
                    }
                    if (!sonNodes.Any()) return;
                    i.nodes = new List<TreeView>();
                    foreach (var sonNode in sonNodes)
                    {
                        i.nodes.Add(new TreeView()
                        {
                            id = sonNode.ID,
                            text = sonNode.Name,
                            ParentId = sonNode.ParentID
                        });
                    }

                    GetTreeView(i.nodes, roleMenus, isSummary);
                });
            }
        }

        /// <summary>
        /// 汇总报表
        /// </summary>
        public ActionResult SummaryReport()
        {
            ViewBag.List = "";
            return View();
        }
        [HttpPost]
        public ActionResult GetSummaryReport(string startTime, string endTime, string selectNode, string workPoint, string pointTxt)
        {
            ViewBag.StartTime = startTime;
            ViewBag.EndTime = endTime;
            ViewBag.WorkPoint = workPoint;
            ViewBag.SelectNode = selectNode;
            ViewBag.PointTxt = pointTxt;
            var listIds = "";
            if (!string.IsNullOrEmpty(workPoint))
            {
                var listId = new List<int> { int.Parse(workPoint) };
                listIds = GetRelatedNodeId(listId, int.Parse(workPoint));
            }
            var list = DatabaseServices.GetOnlineRate(startTime + " 00:00:00", endTime + " 23:59:59", pointTxt, listIds);
            var model = list.GroupBy(l => l.RegionName);
            var result = model.Select(x =>
            new OnlineRateModel
            {
                RegionName = x.Key,
                PointName = x.Count(y => y.RegionName == x.Key).ToString(),
                OnlineHours = x.Sum(o => o.OnlineHours),
                TotalHours = x.Sum(t => t.TotalHours),
                OnlineRate = Math.Round(x.Sum(o => o.OnlineHours) / x.Sum(t => t.TotalHours) * 100, 2),
                RegionId = x.FirstOrDefault().RegionId
            }).ToList();
            result.Add(new OnlineRateModel()
            {
                RegionName = "总计",
                PointName = result.Sum(i => int.Parse(i.PointName)).ToString(),
                OnlineHours = result.Sum(i => i.OnlineHours),
                TotalHours = result.Sum(i => i.TotalHours),
                OnlineRate = Math.Round(result.Sum(i => i.OnlineHours) / result.Sum(i => i.TotalHours) * 100, 2)
            });
            Session["SummaryReport"] = result;
            return View("SummaryReport", result);
        }

        public ActionResult Detail(string startTime, string endTime, string selectNode, string workPoint,
            string pointTxt)
        {
            var listIds = "";
            if (!string.IsNullOrEmpty(workPoint))
            {
                var listId = new List<int> { int.Parse(workPoint) };
                listIds = GetRelatedNodeId(listId, int.Parse(workPoint));
            }
            var list = DatabaseServices.GetOnlineRate(startTime + " 00:00:00", endTime + " 23:59:59", pointTxt, listIds);
            return View(list);
        }

        public ActionResult ExportSummaryReport()
        {
            var path = Server.MapPath("~/temp/summaryReport.xlsx");
            var list = Session["SummaryReport"] as List<OnlineRateModel>;
            var st = Session["ReportST"].ToString();
            var et = Session["ReportET"].ToString();
            if (list == null)
            {
                ViewBag.List = "no data";
                return View("../Home/OnlineRate");
            }
            ExcelHelper.GenerateSummaryReport(list, path, st, et);
            return File(path, "application/vnd.ms-excel", $"在线率汇总报表_{DateTime.Now:yyyyMMdd}.xlsx");
        }

        public ActionResult ExportDetailReport()
        {
            var path = Server.MapPath("~/temp/detailReport.xlsx");
            var list = Session["DetailReport"] as List<OnlineRateModel>;
            var st = Session["ReportST"].ToString();
            var et = Session["ReportET"].ToString();
            if (list == null)
            {
                ViewBag.List = "no data";
                return View("../Home/OnlineRate");
            }
            ExcelHelper.GenerateDetailReport(list, path, st, et);
            return File(path, "application/vnd.ms-excel", $"在线率详情报表_{DateTime.Now:yyyyMMdd}.xlsx");
        }
    }
}