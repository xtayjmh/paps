﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Configuration;
using System.Web.Mvc;
using FluentScheduler;

namespace Paps.Areas.Systems.Controllers
{
    public class ManualSyncController : Controller
    {
        // GET: Systems/ManualSync
        public ActionResult Index()
        {
            ViewBag.Path = ConfigurationManager.AppSettings["SyncPath"];

            return View();
        }

        /// <summary>
        /// 手动同步组织机构数据
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [HttpGet]
        public bool Sync(string path)
        {
            JobManager.Initialize(new SyncStructure(true));
            return true;
        }
    }
}