﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Paps.Areas.Systems.Controllers
{
    public class DbBackController : Controller
    {
        private static readonly string filePath=@"D:\DbBackUp";
        // GET: Systems/DbBack
        public ActionResult Index()
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            var folder = new DirectoryInfo(filePath);
            var list = folder.GetFiles("*.bak");
            return View(list.OrderByDescending(i=>i.CreationTime).ToList());
        }

        /// <summary>
        /// 备份数据库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DoBackUp()
        {
            int Amount = int.Parse(ConfigurationManager.AppSettings["Amount"]);
            var folder = new DirectoryInfo(filePath);
            var list = folder.GetFiles("*.bak");
            if (list.Length >= Amount)
            {
                try
                {
                    var fileInfo = list.OrderByDescending(i => i.CreationTime).ToList().Last();
                    fileInfo.Delete();
                }
                catch { }
            }

            var fileName = DateTime.Now.ToFileTime() + ".bak";
            var cmdText = @"backup database Paps to disk= '" + filePath + "\\" + fileName + "'";
            return Json(DbBackTask.BakReductSql(cmdText, true), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 恢复数据库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DoRestore(string fileName)
        {
            if (fileName == "undefined")
                return Json("请选择一个备份文件", JsonRequestBehavior.AllowGet);
            var cmdText = @"restore database Paps from disk= '" + filePath + "\\" + fileName + "' with replace";
            return Json(DbBackTask.BakReductSql(cmdText, false), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除备份文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DeleteFile(string fileName)
        {
            var fileList = fileName.Split('$');
            var result = String.Empty;
            foreach (var fileItem in fileList)
            {
                if (fileItem.IndexOf(".bak") < 0)
                { continue; }
                try
                {
                    if (System.IO.File.Exists(filePath + "\\" + fileItem))
                    {
                        System.IO.File.Delete(filePath + "\\" + fileItem);
                    }

                    result += fileItem + " 删除成功<br/>";
                }
                catch (Exception ex)
                {
                    result += fileItem + " 删除失败：" + ex.Message + "<br/>";
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Setting()
        {
            return View();
        }

    }
}
