﻿using DAL.Common;
using Model;
using Newtonsoft.Json;
using PagedList;
using Paps.Controllers;
using Paps.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Paps.Areas.Systems.Controllers
{
    public class AdminController : Controller
    {
        // GET: Systems/Admin
        [BaseController(IsCheck = true)]
        public ActionResult Index()
        {
            ViewBag.TreeView = MenuTreeView();
            return View();
        }
        [HttpPost]
        public ActionResult Index(string Compans)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = new Menus();
                    entity.ParentID = 0;
                    entity.Name = Compans;
                    entity.IsDel = false;
                    db.Menus.Add(entity);
                    db.SaveChanges();
                    TempData["result"] = entity.ID > 0 ? "true" : "false";
                }
            }
            catch
            {
                TempData["result"] = "false";
            }
            ViewBag.TreeView = MenuTreeView();
            return View();
        }
        [BaseController(IsCheck = true)]
        public ActionResult List(int page = 1)
        {
            try
            {
                int size = 15;
                using (var db = new PapsEntities())
                {
                    var pageCount = db.Menus.Count(p => p.IsAsync != null);
                    var entity = db.Menus.Where(p => p.IsAsync != null).OrderByDescending(p => p.ID).Take(size).Skip((page - 1) * size).ToList();
                    var pagedlist = new StaticPagedList<Menus>(entity, page, size, pageCount);
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        public ActionResult AddMenu()
        {
            return View();
        }
        [HttpPost]
        public bool AddMenu(string name)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = new Menus()
                    {
                        Name = name,
                        IsAsync = true,
                        IsDel = false,
                        ParentID = 0,
                        StructureType = 2//公司
                    };

                    db.Menus.Add(entity);
                    return db.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        [BaseController(IsCheck = true)]
        public ActionResult Add()
        {
            var paras = int.Parse(Request.QueryString["MenuID"]);

            ViewBag.StructureType = Common.DbHelper.GetSingleModel<Menus>(i => i.ID == paras).StructureType;
            ViewBag.Paras = paras;
            return View();
        }
        [HttpPost]
        public ActionResult Add(Menus entity, int paras, int deviceId = 0)
        {
            ViewBag.StructureType = entity.StructureType;
            try
            {
                using (var db = new PapsEntities())
                {
                    var parent = db.Menus.FirstOrDefault(p => p.ID == paras);
                    parent.AccLoginName = "";
                    parent.AccLoginPwd = "";
                    parent.ItemIp = "";
                    parent.ServicePath = "";
                    var pwd = entity.AccLoginPwd == "" ? "" : entity.AccLoginPwd.ToStringEncrypt();
                    var guestPwd = entity.GuestPwd == "" ? "" : entity.GuestPwd.ToStringEncrypt();

                    entity.AccLoginPwd = pwd;
                    entity.GuestPwd = guestPwd;
                    entity.ParentID = paras;
                    entity.ServicePath = "#";//todo
                    if (parent.StructureType == 3)
                    {
                        entity.ServicePath = string.IsNullOrEmpty(entity.ItemIp) ? "#" : "RealTimeWatch?IpAddress=" + entity.ItemIp.ToStringEncrypt();
                    }

                    entity.StructureType = parent.StructureType + 1;
                    db.Menus.Add(entity);
                    db.SaveChanges();//保存之后entity即可获取保存后的数据，例如主键ID
                    var menu = db.Menus.FirstOrDefault(m => m.ID == entity.ID);
                    if (menu != null) menu.PIndex = menu.ID;
                    //var userIdList = db.Roles.Where(i => i.MenuID == Paras).Select(i => i.UserID).ToList();//获取所有拥有该节点权限的用户id，自动赋予他们访问新添加洞口的权限。
                    //userIdList.ForEach(i => { db.Roles.Add(new Roles() { MenuID = entity.ID, UserID = i }); });
                    //db.SaveChanges();
                    var roles = db.RoleMenu.Where(i => i.MenuID == paras || i.MenuID == parent.ID && i.IsChecked == true || i.MenuID == 1).ToList();
                    if (roles.Any())
                    {
                        roles.ForEach(i =>
                        {
                            if (db.RoleMenu.Local.Count(r => r.MenuID == entity.ID && r.MenuName == entity.Name && r.RoleID == i.RoleID) == 0)
                            {
                                db.RoleMenu.Add(new RoleMenu() { MenuID = entity.ID, RoleID = i.RoleID, MenuName = entity.Name });
                            }
                        });
                    }
                    if (parent.StructureType == 3)
                    {
                        //添加菜单和设备之间的映射关系
                        var device = db.Device.FirstOrDefault(d => d.DeviceId == deviceId);
                        if (device != null) device.MenuId = entity.ID;
                        //判断在线率记录中是否有没有映射就已经上线的记录
                        var onlineRecords = db.OnlineRate.Where(o => o.DeviceCode == device.DeviceCode && o.UserId == null).ToList();
                        onlineRecords.ForEach(r =>
                        {
                            r.UserId = entity.ID;
                            r.RegionName = parent.Name;
                            r.PointName = entity.Name;
                        });
                    }
                    db.SaveChanges();

                    TempData["result"] = entity.ID > 0 ? "true" : "false";
                }
            }
            catch (Exception ex)
            {
                TempData["result"] = "false";
            }

            return View();
        }
        [BaseController(IsCheck = true)]
        public ActionResult Edit()
        {
            using (var db = new PapsEntities())
            {
                var paras = Convert.ToInt32(Request.QueryString["MenuID"]);
                var entity = db.Menus.FirstOrDefault(p => p.ID == paras);
                return View(entity);
            }
        }
        [HttpPost]
        public ActionResult Edit(Menus entity)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    var subItem = db.Menus.Count(p => p.ParentID == entity.ID);
                    ViewBag.IsLastChild = subItem == 0 ? true : false;
                    var model = db.Menus.FirstOrDefault(p => p.ID == entity.ID);
                    var pwd = entity.AccLoginPwd == "" ? "" : entity.AccLoginPwd.ToStringEncrypt();
                    var guestPwd = entity.GuestPwd == "" ? "" : entity.GuestPwd.ToStringEncrypt();
                    if (model.StructureType == 3 && entity.Name != model.Name) //如果修改的是标段级名称
                    {
                        var pointIds = db.Menus.Where(m => m.ParentID == model.ID).Select(m => m.ID).ToList();
                        var onlineDataToEdit = db.OnlineRate.Where(o => o.UserId != null && pointIds.Contains((int)o.UserId)).ToList();
                        onlineDataToEdit.ForEach(e => { e.RegionName = entity.Name; });
                    }

                    if (model.StructureType == 4 && entity.Name != model.Name)
                    {
                        var onlineDataToEdit = db.OnlineRate.Where(o => o.UserId != null && o.UserId == model.ID).ToList();
                        onlineDataToEdit.ForEach(e => { e.PointName = entity.Name; });
                    }
                    model.Name = entity.Name;
                    model.AccLoginName = entity.AccLoginName;
                    model.AccLoginPwd = pwd;
                    model.IsType = entity.IsType;
                    model.IsShowOnline = entity.IsShowOnline;
                    model.DepartmentID = entity.DepartmentID;
                    //2018-8-30新增的客户端普通用户的账号密码Johnson
                    model.GuestUserName = entity.GuestUserName;
                    model.GuestPwd = guestPwd;
                    if (entity.IsType == 0)
                    {
                        model.ItemIp = entity.ItemIp == null ? "" : entity.ItemIp;
                        model.ServicePath = string.IsNullOrEmpty(entity.ItemIp) ? "#" : "RealTimeWatch?IpAddress=" + entity.ItemIp.ToStringEncrypt();
                    }

                    var result = db.SaveChanges();
                    TempData["result"] = result >= 0 ? "true" : "false";
                    return View(model);
                }
            }
            catch
            {
                TempData["result"] = "false";
            }
            return View();
        }
        public static string GetAsyncDate(string departID)
        {
            using (var db = new PapsEntities())
            {
                var entity = db.DepartSync.Where(p => p.DepartID == departID).OrderByDescending(p => p.AsyncDate).FirstOrDefault();
                return entity == null ? "未同步" : entity.AsyncDate.ToString();
            }
        }
        #region Index前端ajax方法，禁用启用
        /// <summary>
        /// 启用
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Unlock(int MenuID)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var result = 0;
                    List<int> list = new List<int>() { MenuID };
                    var parentID = db.Menus.Where(p => p.ID == MenuID).Select(p => p.ParentID).FirstOrDefault();
                    if (parentID > 0) { list.Add(parentID); GetParentIDs(list, db, parentID); }
                    foreach (var item in list)
                    {
                        var entity = db.Menus.Where(p => p.ID == item).FirstOrDefault();
                        entity.IsDel = false;
                        result = db.SaveChanges();
                    }

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                    paras.data = result >= 0 ? list : null;
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 禁用
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Lock(int MenuID)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var result = 0;
                    List<int> list = new List<int>();
                    list.Add(MenuID);
                    GetSubItems(list, MenuID, db);
                    foreach (var item in list)
                    {
                        var entity = db.Menus.Where(p => p.ID == item).FirstOrDefault();
                        entity.IsDel = true;
                        result = db.SaveChanges();
                    }

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                    paras.data = list;
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
                paras.data = null;
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UnAsync(int departId)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Menus.Where(p => p.ID == departId).FirstOrDefault();
                    entity.IsAsync = true;
                    var result = db.SaveChanges();

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                }
            }
            catch (Exception ex)
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult OnAsync(int departId)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Menus.Where(p => p.ID == departId).FirstOrDefault();
                    entity.IsAsync = false;
                    var result = db.SaveChanges();

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                }
            }
            catch (Exception ex)
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        private void GetSubItems(List<int> list, int parentId, PapsEntities db)
        {
            var subItem = db.Menus.Where(p => p.ParentID == parentId).ToList();
            if (subItem.Count == 0) return;
            foreach (var item in subItem)
            {
                list.Add(item.ID);
                GetSubItems(list, item.ID, db);
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int MenuID)
        {
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Menus.FirstOrDefault(p => p.ID == MenuID);
                    var child = db.Menus.Where(i => i.ParentID == MenuID);
                    var childChild = new List<Menus>();
                    child.ToList().ForEach(i =>
                    {
                        childChild.AddRange(db.Menus.Where(j => j.ParentID == i.ID));
                        db.Menus.Remove(i);
                        db.OnlineRate.Where(o => o.UserId == i.ID).ToList().ForEach(o => { db.OnlineRate.Remove(o); });

                    });
                    db.Menus.Remove(entity);
                    db.OnlineRate.Where(o => o.UserId == entity.ID).ToList().ForEach(o => { db.OnlineRate.Remove(o); });
                    childChild.ForEach(i =>
                    {
                        db.Menus.Remove(i);
                        db.OnlineRate.Where(o => o.UserId == i.ID).ToList().ForEach(o => { db.OnlineRate.Remove(o); });

                    });
                    var result = db.SaveChanges();
                    paras.code = result > 0 ? 200 : 500;
                    paras.status = result > 0;
                    paras.msg = result > 0 ? "Success" : "Field";
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region treeview方法
        public string MenuTreeView()
        {
            string strContent = "<ul class=\"main_parent\">";
            using (var db = new PapsEntities())
            {
                var allMenus = db.Menus.ToList();
                var currentItem = db.Menus.Where(p => p.ParentID == 0).ToList();
                var i = 0;
                foreach (var item in currentItem)
                {
                    var subCounts = db.Menus.Count(p => p.ParentID == item.ID);
                    var icos = subCounts == 0 ? "empty" : "plus";
                    var update = "< input type = 'button' class='btn btn-info btn-xs' value='修改' onclick=\"Edit('" + item.ID + "')\"/>";
                    var status = item.IsDel ? "<font id='font_" + item.ID + "' style='color:red;'><已禁用></font>" : "<font id='font_" + item.ID + "' style='color:green;'><已启用></font>";
                    var button = item.IsDel ? "<input id='Lock_" + item.ID + "' type='button' class='btn btn-primary btn-xs' value='启用' onclick=\"onDisable('" + item.ID + "')\" />" : "<input id='Lock_" + item.ID + "' type='button' class='btn btn-warning btn-xs' value='禁用' onclick=\"Disable('" + item.ID + "')\" />";
                    var delete = "<input type='button' class='btn btn-danger btn-xs' value='删除' onclick=\"onDelete('" + item.ID + "')\" />";
                    if (item.ID == 1)
                    {
                        update = "";
                        status = "";
                        button = "";
                        delete = "";
                    }
                    strContent += string.Format("<li onmouseover=\"manager('span_" + item.ID + "')\" onmouseout=\"overmanager('span_" + item.ID + "')\"><i id=\"li_" + item.ID + "\" class=\"{0}\" onclick=\"Fold('li_" + item.ID + "','ul_" + item.ID + "','" + item.StructureType + "');\"></i><span class=\"span-item\" style=\"white-space: nowrap;\">{1}{2}<span id='span_" + item.ID + "' style='display:none;'><input type='button' class='btn btn-primary btn-xs' value='添加' onclick=\"Add('" + item.ID + "')\" />{3}{4}{5}</span></span></li>", icos, item.Name, status, update, button, delete);
                    strContent += MenuSubTree(allMenus, item.ID, i);
                    strContent += "</li>";
                }
            }
            strContent += "</ul>";
            return strContent;
        }
        public string MenuSubTree(List<Menus> db, int intParentID, int i)
        {
            var currentItems = db.Where(p => p.ParentID == intParentID).OrderBy(p => p.PIndex).ToList();
            string strSubContent = currentItems.Count > 0 ? "<ul id=\"ul_" + intParentID + "\" class=\"sub-item\">" : "";
            i++;
            foreach (var item in currentItems)
            {
                int intLeft = i * 26;
                var subCounts = db.Count(p => p.ParentID == item.ID);
                var icos = subCounts == 0 ? "empty" : "plus";
                var status = item.IsDel ? "<font id='font_" + item.ID + "' style='color:red;'><已禁用></font>" : "<font id='font_" + item.ID + "' style='color:green;'><已启用></font>";
                var button = item.IsDel ? "<input id='Lock_" + item.ID + "' type='button' class='btn btn-warning btn-xs' value='启用' onclick=\"onDisable('" + item.ID + "')\" />" : "<input id='Lock_" + item.ID + "' type='button' class='btn btn-xs btn-warning' value='禁用' onclick=\"Disable('" + item.ID + "')\" />";
                var delete = "<input type='button' class='btn btn-danger btn-xs' value='删除' onclick=\"onDelete('" + item.ID + "')\" />";
                var canAdd = item.StructureType >= 4 ? "" : "<input type='button' class='btn btn-primary btn-xs' value='添加' onclick=\"Add('" + item.ID + "')\" />";
                var moveUp = item.StructureType == 4 && currentItems.IndexOf(item) > 0 ? "<input type='button' class='btn btn-success btn-xs' value='上移' onclick=\"MoveUp('" + item.ID + "', '" + intParentID + "')\" />" : ""; //向上移动按钮，第一个元素没有向上移动的按钮
                var moveDown = item.StructureType == 4 && currentItems.Last() != item ? "<input type='button' class='btn btn-success btn-xs' value='下移' onclick=\"MoveDown('" + item.ID + "', '" + intParentID + "')\" />" : ""; //向下移动按钮，最后一个元素没有向下移动按钮
                strSubContent += string.Format("<li onmouseover=\"manager('span_" + item.ID + "')\" onmouseout=\"overmanager('span_" + item.ID + "')\" style=\"padding-left:" + intLeft + "px;\" ><i id=\"li_" + item.ID + "\" class=\"{0}\" onclick=\"Fold('li_" + item.ID + "','ul_" + item.ID + "','" + item.StructureType + "');\"></i><span class=\"span-item\" style=\"white-space: nowrap;\">{1}{2}<span id='span_" + item.ID + "' style='display:none;'>{5}<input type='button' class='btn btn-info btn-xs' value='修改' onclick=\"Edit('" + item.ID + "')\" />{3}{4}{6}{7}</span></span></li>", icos, item.Name, status, button, delete, canAdd, moveUp, moveDown);
                strSubContent += MenuSubTree(db, item.ID, i);
                strSubContent += "</li>";
            }
            strSubContent += currentItems.Count > 0 ? "</ul>" : "";
            return strSubContent;
        }

        /// <summary>
        /// 调整洞口的显示顺序
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="plusOrMinus">-1表示上移,1表示下移</param>
        /// <param name="intParentId"></param>
        /// <returns></returns>
        [HttpGet]
        public string AdjIndex(int menuId, int intParentId, int plusOrMinus = 1)
        {
            var db = new PapsEntities();
            var menus = db.Menus.Where(m => m.ParentID == intParentId).OrderBy(m => m.PIndex).ToList();
            var mode = menus.FirstOrDefault(m => m.ID == menuId);
            var indexOf = menus.IndexOf(mode);
            if (plusOrMinus == 1 && mode != null) //下移的时候，下边那个往上移
            {
                var currentIndex = mode.PIndex;
                mode.PIndex = menus[indexOf + 1].PIndex;
                menus[indexOf + 1].PIndex = currentIndex;
            }
            else //上移的时候，上边那个往下移
            {
                if (mode != null)
                {
                    var currentIndex = mode.PIndex;
                    mode.PIndex = menus[indexOf - 1].PIndex;
                    menus[indexOf - 1].PIndex = currentIndex;
                }
            }
            db.SaveChanges();
            string strSubContent = "";
            var newList = menus.OrderBy(m => m.PIndex).ToList();
            foreach (var item in newList)
            {
                var icos = "empty";
                int intLeft = 3 * 26;
                var status = item.IsDel ? "<font id='font_" + item.ID + "' style='color:red;'><已禁用></font>" : "<font id='font_" + item.ID + "' style='color:green;'><已启用></font>";
                var button = item.IsDel ? "<input id='Lock_" + item.ID + "' type='button' class='btn btn-warning btn-xs' value='启用' onclick=\"onDisable('" + item.ID + "')\" />" : "<input id='Lock_" + item.ID + "' type='button' class='btn btn-xs btn-warning' value='禁用' onclick=\"Disable('" + item.ID + "')\" />";
                var delete = "<input type='button' class='btn btn-danger btn-xs' value='删除' onclick=\"onDelete('" + item.ID + "')\" />";
                var canAdd = item.StructureType >= 4 ? "" : "<input type='button' class='btn btn-primary btn-xs' value='添加' onclick=\"Add('" + item.ID + "')\" />";
                var moveUp = item.StructureType == 4 && newList.IndexOf(item) > 0 ? "<input type='button' class='btn btn-success btn-xs' value='上移' onclick=\"MoveUp('" + item.ID + "', '" + intParentId + "')\" />" : ""; //向上移动按钮，第一个元素没有向上移动的按钮
                var moveDown = item.StructureType == 4 && newList.Last() != item ? "<input type='button' class='btn btn-success btn-xs' value='下移' onclick=\"MoveDown('" + item.ID + "', '" + intParentId + "')\" />" : ""; //向下移动按钮，最后一个元素没有向下移动按钮
                strSubContent += string.Format("<li onmouseover=\"manager('span_" + item.ID + "')\" onmouseout=\"overmanager('span_" + item.ID + "')\" style=\"padding-left:" + intLeft + "px;\" ><i id=\"li_" + item.ID + "\" class=\"{0}\" onclick=\"Fold('li_" + item.ID + "','ul_" + item.ID + "','span_" + item.ID + "');\"></i><span class=\"span-item\" style=\"white-space: nowrap;\">{1}{2}<span id='span_" + item.ID + "' style='display:none;'>{5}<input type='button' class='btn btn-info btn-xs' value='修改' onclick=\"Edit('" + item.ID + "')\" />{3}{4}{6}{7}</span></span></li>", icos, item.Name, status, button, delete, canAdd, moveUp, moveDown);
                strSubContent += "</li>";
            }
            strSubContent += "";
            return strSubContent;
        }
        private void GetParentIDs(List<int> list, PapsEntities db, int menuId)
        {
            var parentID = db.Menus.Where(p => p.ID == menuId).Select(p => p.ParentID).FirstOrDefault();
            if (parentID != 0)
            {
                list.Add(parentID);
                GetParentIDs(list, db, parentID);
            }
        }
        #endregion

        [BaseController(IsCheck = true)]
        public ActionResult ListRoles(int page = 1)
        {
            try
            {
                int size = 15;
                using (var db = new PapsEntities())
                {
                    var pageCount = db.ArrRoles.Where(r => r.RoleID != 1).Count();  // updated by Ted on 20180613
                    var entity = db.ArrRoles.Where(r => r.RoleID != 1).OrderByDescending(p => p.ID).Skip((page - 1) * size).Take(size).ToList();  // updated by Ted on 20180613
                    var pagedlist = new StaticPagedList<ArrRoles>(entity, page, size, pageCount);
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [BaseController(IsCheck = true)]
        public ActionResult RoleMenu(int RoleID)
        {
            return View();
        }

        public ActionResult EditRole(int roleId)
        {
            using (var db = new PapsEntities())
            {
                var role = db.ArrRoles.FirstOrDefault(i => i.RoleID == roleId);
                return role != null ? View(role) : View();
            }
        }
        [HttpGet]
        public bool DeleteRole(int roleId)
        {
            using (var db = new PapsEntities())
            {
                var role = db.ArrRoles.FirstOrDefault(i => i.RoleID == roleId);
                if (role != null)
                {
                    db.ArrRoles.Remove(role);
                    return db.SaveChanges() > 0;
                }

                return false;
            }
        }
        public ActionResult AddRole()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddRole(ArrRoles role)
        {
            using (var db = new PapsEntities())
            {
                var newRole = new ArrRoles() { RoleName = role.RoleName, Remarks = role.Remarks, CreateTime = DateTime.Now };
                db.ArrRoles.Add(newRole);
                var r = db.SaveChanges();
                newRole.RoleID = newRole.ID;
                db.SaveChanges();
                TempData["result"] = r > 0;
                return View(role);
            }
        }
        public ActionResult SetRole(string roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }
        [HttpGet]
        public JsonResult SetRoleMenu(string strArr, int roleId, string unCheckedStr)
        {
            var list = JsonConvert.DeserializeObject<List<RoleMenu>>(strArr);
            var unCheckedList = JsonConvert.DeserializeObject<List<RoleMenu>>(unCheckedStr);
            int re = 0;
            using (var db = new PapsEntities())
            {
                if (list.Any())
                {
                    //重新赋予新的权限
                    foreach (var item in list)
                    {
                        var currentMenu = db.Menus.FirstOrDefault(i => i.ID == item.MenuID);
                        Menus parentMenu;
                        List<Menus> childMenus;
                        if (currentMenu != null)
                        {
                            //向上递归，不包括默认控制中心
                            parentMenu = db.Menus.FirstOrDefault(i => i.ID == currentMenu.ParentID && i.ParentID != 0);
                            if (parentMenu != null)
                            {
                                if (db.RoleMenu.Local.Count(i => i.RoleID == roleId && i.MenuName == parentMenu.Name && i.MenuID == parentMenu.ID) == 0 && db.RoleMenu.Count(i => i.RoleID == roleId && i.MenuName == parentMenu.Name && i.MenuID == parentMenu.ID) == 0) db.RoleMenu.Add(new RoleMenu() { RoleID = roleId, MenuName = parentMenu.Name, MenuID = parentMenu.ID }); //如果有父级菜单的话，同样赋予用户对应的权限
                                var grandParentMenu = db.Menus.FirstOrDefault(i => i.ID == parentMenu.ParentID);
                                if (grandParentMenu != null && grandParentMenu.ParentID != 0 && db.RoleMenu.Local.Count(i => i.RoleID == roleId && i.MenuName == grandParentMenu.Name && i.MenuID == grandParentMenu.ID) == 0 && db.RoleMenu.Count(i => i.RoleID == roleId && i.MenuName == grandParentMenu.Name && i.MenuID == grandParentMenu.ID) == 0) db.RoleMenu.Add(new RoleMenu() { RoleID = roleId, MenuName = grandParentMenu.Name, MenuID = grandParentMenu.ID });//添加祖父级权限，但是不包括默认控制中心
                            }
                            //向下递归
                            childMenus = db.Menus.Where(i => i.ParentID == currentMenu.ID).ToList();
                            if (childMenus.Any())
                            {
                                childMenus.ForEach(i =>
                                {
                                    if (db.RoleMenu.Count(rm => rm.RoleID == roleId && rm.MenuName == i.Name && rm.MenuID == i.ID) == 0 && db.RoleMenu.Local.Count(rm => rm.RoleID == roleId && rm.MenuName == i.Name && rm.MenuID == i.ID) == 0)
                                    {
                                        db.RoleMenu.Add(new RoleMenu() { RoleID = roleId, MenuName = i.Name, MenuID = i.ID });
                                    }
                                    var grandChildMenus = db.Menus.Where(j => j.ParentID == i.ID).ToList();
                                    if (grandChildMenus.Any())
                                    {
                                        grandChildMenus.ForEach(gcm =>
                                        {
                                            if (db.RoleMenu.Local.Count(rml => rml.RoleID == roleId && rml.MenuName == gcm.Name && rml.MenuID == gcm.ID) == 0 && db.RoleMenu.Count(rml => rml.RoleID == roleId && rml.MenuName == gcm.Name && rml.MenuID == gcm.ID) == 0)
                                            {
                                                db.RoleMenu.Add(new RoleMenu() { RoleID = roleId, MenuName = gcm.Name, MenuID = gcm.ID });
                                            }

                                            var gGChildMenus = db.Menus.Where(gg => gg.ParentID == gcm.ID).ToList();//如果勾选的是主控中心，就要遍历到第四级，也就是洞口
                                            if (gGChildMenus.Any())
                                            {
                                                gGChildMenus.ForEach(gg =>
                                                {
                                                    if (db.RoleMenu.Local.Count(rml => rml.RoleID == roleId && rml.MenuName == gg.Name && rml.MenuID == gg.ID) == 0 && db.RoleMenu.Count(rml => rml.RoleID == roleId && rml.MenuName == gg.Name && rml.MenuID == gg.ID) == 0)
                                                    {
                                                        db.RoleMenu.Add(new RoleMenu() { RoleID = roleId, MenuName = gg.Name, MenuID = gg.ID });
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            }


                        }

                        item.RoleID = roleId;
                        item.IsChecked = true;
                        if (db.RoleMenu.Local.Count(i => i.RoleID == roleId && i.IsChecked == true && i.MenuID == item.MenuID) == 0 && db.RoleMenu.Count(i => i.RoleID == roleId && i.IsChecked == true && i.MenuID == item.MenuID) == 0) db.RoleMenu.Add(item);
                    }

                    var currentRole = db.ArrRoles.FirstOrDefault(i => i.RoleID == roleId);
                    if (currentRole != null) currentRole.UpdateTime = DateTime.Now;
                    re += db.SaveChanges();
                }
                if (unCheckedList.Any())
                {
                    //删除掉去掉勾选的菜单及其子集
                    if (unCheckedList.Any())
                    {
                        unCheckedList.ForEach(i =>
                        {
                            var mode = db.RoleMenu.FirstOrDefault(rm => rm.MenuID == i.MenuID && rm.MenuName == i.MenuName && rm.RoleID == roleId);
                            if (mode != null)
                            {
                                db.RoleMenu.Remove(mode);
                            }

                            var childMode = db.Menus.Where(m => m.ParentID == i.MenuID).ToList();//根据去掉勾选的节点id找到子节点们
                            if (childMode.Any())
                            {
                                childMode.ForEach(cm =>
                                {
                                    var childRoleMenu = db.RoleMenu.FirstOrDefault(cml => cml.MenuName == cm.Name && cml.MenuID == cm.ID && cml.RoleID == roleId);
                                    if (childRoleMenu != null) db.RoleMenu.Remove(childRoleMenu);

                                    var grandChildList = db.Menus.Where(m => m.ParentID == cm.ID).ToList();
                                    if (grandChildList.Any())
                                    {
                                        grandChildList.ForEach(gcl =>
                                        {
                                            var grandChildRoleMenu = db.RoleMenu.Where(cml => cml.MenuID == gcl.ID && cml.MenuName == gcl.Name && cml.RoleID == roleId).ToList();
                                            if (grandChildRoleMenu.Any())
                                            {
                                                grandChildRoleMenu.ForEach(gcrm =>
                                                {
                                                    var gGChildList = db.Menus.Where(gg => gg.ParentID == gcrm.MenuID).ToList();//如果勾选的是主控中心，就要遍历到第四级，也就是洞口
                                                    if (gGChildList.Any())
                                                    {
                                                        gGChildList.ForEach(gg =>
                                                        {
                                                            var gGChildMenus = db.RoleMenu.Where(ggcm => ggcm.MenuID == gg.ID && ggcm.MenuName == gg.Name && ggcm.RoleID == roleId).ToList();
                                                            if (gGChildMenus.Any())
                                                            {
                                                                gGChildMenus.ForEach(gm => { db.RoleMenu.Remove(gm); });
                                                            }
                                                        });

                                                    }
                                                    db.RoleMenu.Remove(gcrm);
                                                });
                                            }

                                        });
                                    }
                                });
                            }
                        });
                        re += db.SaveChanges();
                    }
                }
                return Json(re > 0, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult SaveRole(ArrRoles role)
        {
            using (var db = new PapsEntities())
            {
                db.Entry(role).State = EntityState.Modified;
                return Json(db.SaveChanges() > 0);
            }
        }

        public ActionResult test()
        {
            return View();
        }

        /// <summary>
        /// 设备管理
        /// </summary>
        /// <returns></returns>
        [BaseController(IsCheck = true)]
        public ActionResult DeviceManage(string keyWord, int page = 1)
        {
            try
            {
                ViewBag.KeyWord = keyWord;
                int size = 12;
                using (var db = new PapsEntities())
                {
                    var pageCount = string.IsNullOrEmpty(keyWord) ? db.Device.Count() : db.Device.Count(d => d.DeviceName.Contains(keyWord) || d.DeviceCode.Contains(keyWord));
                    var entity = string.IsNullOrEmpty(keyWord) ? db.Device.OrderByDescending(d => d.DeviceId).Skip((page - 1) * size).Take(size).ToList() : db.Device.Where(d => d.DeviceName.Contains(keyWord) || d.DeviceCode.Contains(keyWord)).OrderByDescending(d => d.DeviceId).Skip((page - 1) * size).Take(size).ToList();
                    var pagedlist = new StaticPagedList<Device>(entity, page, size, pageCount);
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        /// <summary>
        /// 添加新设备视图
        /// </summary>
        /// <returns></returns>
        public ActionResult AddDevice()
        {
            return View();
        }
        /// <summary>
        /// 添加新设备方法
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddDevice(Device device)
        {
            var db = new PapsEntities();
            db.Device.Add(device);
            var res = db.SaveChanges();
            TempData["result"] = res > 0 ? "True" : "";
            return View();
        }

        /// <summary>
        /// 删除设备
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        [HttpGet]
        public bool DeleteDevice(int deviceId)
        {
            var db = new PapsEntities();
            var device = db.Device.FirstOrDefault(d => d.DeviceId == deviceId);
            SqlParameter deviceCode = new SqlParameter("@deviceCode", device.DeviceCode);
            var res = db.Database.ExecuteSqlCommand("exec DeleteDeviceData @deviceCode", deviceCode);
            return res > 0;
        }
        /// <summary>
        /// 获取所有设备信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDevices()
        {
            var db = new PapsEntities();
            var deviceList = db.Device.ToList();
            return Json(deviceList, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 设置设备和菜单之间的关系
        /// </summary>
        /// <param name="deviceId">设备id</param>
        /// <param name="menuId">菜单id</param>
        /// <returns></returns>
        [HttpGet]
        public bool SetDeviceInfo(int deviceId, int menuId)
        {
            var db = new PapsEntities();
            var list = db.Device.Where(d => d.MenuId == menuId).ToList();
            list.ForEach(l => { l.MenuId = null; });
            var device = db.Device.FirstOrDefault(d => d.DeviceId == deviceId);
            var menu = db.Menus.FirstOrDefault(m => m.ID == menuId);
            var parentMenu = db.Menus.FirstOrDefault(m => m.ID == menu.ParentID);
            if (device != null) device.MenuId = menuId;

            //更新一下在线率数据还是有必要的
            var onlineRecords = db.OnlineRate.Where(o => o.DeviceCode == device.DeviceCode && o.UserId == null).ToList();
            onlineRecords.ForEach(r =>
            {
                r.UserId = menuId;
                r.RegionName = parentMenu.Name;
                r.PointName = menu.Name;
            });
            return db.SaveChanges() > 0;
        }
    }
}