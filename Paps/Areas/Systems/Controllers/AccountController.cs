﻿using DAL;
using Model;
using Newtonsoft.Json;
using PagedList;
using Paps.Common;
using Paps.Controllers;
using Paps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UserInfo = Paps.Models.UserInfo;

namespace Paps.Areas.Systems.Controllers
{
    [BaseController(IsCheck = true)]
    public class AccountController : Controller
    {
        private static PapsEntities entity= new PapsEntities();
        private static List<Menus> dbMeuns = entity.Menus.ToList();
        private static List<Departments> dbDeparts= entity.Departments.ToList();
        public ActionResult List(int page = 1)
        {
            try
            {
                int size = 15;
                using (var db = new PapsEntities())
                {
                    var pageCount = db.Departments.Count(p => p.IsAsync != null && p.IsType == null);
                    var entity = db.Departments.Where(p => p.IsAsync != null && p.IsType == null).OrderByDescending(p => p.ID).Take(size).Skip((page - 1) * size).ToList();
                    var pagedlist = new StaticPagedList<Departments>(entity, page, size, pageCount);
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        public ActionResult Index(string Compans)
        {
            ViewBag.TreeView = MenuTreeView();
            ViewBag.Arr = "";
            if (Session?["Arr"] != null)
            {
                ViewBag.Arr = Session["Arr"];
            }
            return View();
        }


        // GET: Systems/Account
        public ActionResult ListUser(int DepartmentID, int page = 1)
        {
            var main = (Users)Session["UserInfo"];
            int size = 15;
            using (var db = new PapsEntities())
            {
                List<int> userIDs = db.DepartmentUsers.Where(u => u.DepartID == DepartmentID).Select(u => u.UserID).ToList();

                var pageCount = userIDs.Count();

                List<Users> users = new List<Users>();
                foreach (var userID in userIDs)
                {
                    var user = db.Users.FirstOrDefault(p => p.UserId == userID);
                    if (user != null)
                        users.Add(user);
                }

                var entity = users.Where(p => p.UserId != main.UserId).OrderBy(p => p.CreateDate).Take(size).Skip((page - 1) * size).ToList();
                var list = entity.Select(r => new UserInfo()
                {
                    UserID = r.UserId,
                    Name = r.UserName,
                    FullName = r.FullName,
                    Date = r.CreateDate.ToString(),
                    Role = "",
                    Status = r.IsActive == true ? "禁用" : "启用"
                }).ToList();
                list.ForEach(i =>
                {
                    var roleRelation = db.RoleUser.Where(j => j.UserID == i.UserID).ToList();
                    if (roleRelation.Any())
                    {
                        roleRelation.ForEach(r =>
                        {
                            var role = db.ArrRoles.FirstOrDefault(a => a.ID == r.RoleID);
                            if (role != null) i.Role += role.RoleName + ",";
                        });
                    }

                    i.Role = i.Role.Trim(',');
                });
                var pagedlist = new StaticPagedList<UserInfo>(list, page, size, pageCount);
                ViewBag.DeptId = DepartmentID;
                return View(pagedlist);
            }
        }

        [HttpPost]
        public JsonResult UnAsync(int departId)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Departments.FirstOrDefault(p => p.ID == departId);
                    entity.IsAsync = true;
                    var result = db.SaveChanges();

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                }
            }
            catch (Exception ex)
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult OnAsync(int departId)
        {
            JsonParas<int> paras = new JsonParas<int>();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Departments.Where(p => p.ID == departId).FirstOrDefault();
                    entity.IsAsync = false;
                    var result = db.SaveChanges();

                    paras.code = result >= 0 ? 200 : 500;
                    paras.status = result >= 0 ? true : false;
                    paras.msg = result >= 0 ? "Success" : "Field";
                }
            }
            catch (Exception ex)
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCompany()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCompany(Departments entity)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    entity.IsAsync = true;
                    entity.IsDel = false;
                    entity.ParentID = 0;
                    entity.StructureType = 2;//公司
                    db.Departments.Add(entity);
                    db.SaveChanges();
                    if (entity.ID >= 0)
                    {
                        TempData["result"] = "true";
                    }
                    else
                    {
                        TempData["result"] = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["result"] = "false";
            }
            return View(entity);
        }

        [BaseController(IsCheck = true)]
        public ActionResult EditDepartment()
        {
            using (var db = new PapsEntities())
            {
                var paras = Convert.ToInt32(Request.QueryString["DepartmentID"]);
                var entity = db.Departments.FirstOrDefault(p => p.ID == paras);
                return View(entity);
            }
        }
        [HttpPost]
        public ActionResult EditDepartment(Departments entity)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    var subItem = db.Departments.Count(p => p.ParentID == entity.ID);
                    ViewBag.IsLastChild = subItem == 0 ? true : false;
                    var model = db.Departments.FirstOrDefault(p => p.ID == entity.ID);
                    model.Name = entity.Name;
                    model.DepartmentID = entity.DepartmentID;
                    var result = db.SaveChanges();
                    TempData["result"] = result >= 0 ? "true" : "false";
                    return View(model);
                }
            }
            catch
            {
                TempData["result"] = "false";
            }
            return View();
        }
        [BaseController(IsCheck = true)]
        public ActionResult AddDepartment()
        {
            var paras = int.Parse(Request.QueryString["DepartmentID"]);

            ViewBag.StructureType = Common.DbHelper.GetSingleModel<Departments>(i => i.ID == paras).StructureType;
            ViewBag.Paras = paras;
            return View();
        }
        [HttpPost]
        public ActionResult AddDepartment(Departments entity, int Paras)
        {
            ViewBag.StructureType = entity.StructureType;
            try
            {
                using (var db = new PapsEntities())
                {
                    var parent = db.Departments.FirstOrDefault(p => p.ID == Paras);
                    entity.ParentID = Paras;
                    entity.StructureType = parent.StructureType + 1;
                    db.Departments.Add(entity);
                    db.SaveChanges();
                    var ids = db.DeptRoleRelation.FirstOrDefault(i => i.DeptId == Paras)?.RoleIds;
                    if (ids != null)
                    {
                        db.DeptRoleRelation.Add(new DeptRoleRelation
                        {
                            DeptId = entity.ID,
                            RoleIds = ids
                        });
                        db.SaveChanges();
                    }

                    TempData["result"] = entity.ID > 0 ? "true" : "false";
                }
            }
            catch (Exception ex)
            {
                TempData["result"] = "false";
            }
            return View();
        }


        public ActionResult Edit(int page, string userId)
        {
            try
            {
                int size = 15;
                using (var db = new PapsEntities())
                {
                    var pageCount = db.ArrRoles.Count();
                    var entity = db.ArrRoles.OrderByDescending(p => p.ID).Skip((page - 1) * size).Take(size).ToList();
                    var pagedlist = new StaticPagedList<ArrRoles>(entity, page, size, pageCount);
                    ViewBag.UserId = userId;
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetUserInfo(string userId)
        {
            using (var db = new PapsEntities())
            {
                var uid = int.Parse(userId);
                var user = db.Users.FirstOrDefault(i => i.UserId == uid);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveUserRole(string roleIds, Users user)
        {
            bool result;
            var ids = JsonConvert.DeserializeObject<List<int>>(roleIds);
            using (var db = new PapsEntities())
            {
                var existUser = db.Users.FirstOrDefault(i => i.UserId == user.UserId);

                if (existUser != null)
                {
                    existUser.UserName = user.UserName;
                    existUser.FullName = user.FullName;
                    if (existUser.Password != user.Password)
                    {
                        existUser.Password = EncryptHelper.CreateMd5Hash(user.Password);
                    }
                }

                var list = db.RoleUser.Where(i => i.UserID == user.UserId).ToList();
                list.ForEach(i => { db.RoleUser.Remove(i); });//删除之前的角色
                ids.ForEach(i => { db.RoleUser.Add(new RoleUser() { UserID = user.UserId, RoleID = i }); });//重新添加新的角色
                result = db.SaveChanges() > 0;
            }
            return Json(result);
        }
        [HttpPost]
        public ActionResult Edit(Users entity)
        {
            try
            {
                using (var db = new PapsEntities())
                {

                    var user = db.Users.FirstOrDefault(p => p.UserId == entity.UserId);

                    if (user.UserName == entity.UserName || !db.Users.Any(p => p.UserName == entity.UserName))
                    {
                        user.RoleId = entity.RoleId;
                        user.UserName = entity.UserName;
                        user.Password = entity.Password == "******" ? user.Password : EncryptHelper.CreateMd5Hash(entity.Password);
                        var result = db.SaveChanges();
                        TempData["result"] = result >= 0 ? "true" : "false";
                        return View(user);
                    }
                    else
                    {
                        TempData["result"] = "UserNameError";
                    }

                }
            }
            catch (Exception)
            {
                TempData["result"] = "false";
            }
            return View(entity);
        }
        public ActionResult Add(string deptId)
        {
            //            GetRoles();
            ViewBag.DeptId = deptId;
            return View();
        }
        [HttpPost]
        public ActionResult Add(Users entity, int deptId)
        {
            ViewBag.DeptId = deptId;
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    if (!db.Users.Any(p => p.UserName == entity.UserName))
                    {
                        entity.IsActive = false;
                        entity.CreateDate = DateTime.Now;
                        entity.Password = EncryptHelper.CreateMd5Hash(entity.Password);
                        db.Users.Add(entity);
                        db.SaveChanges();
                        var roles = db.DeptRoleRelation.FirstOrDefault(i => i.DeptId == deptId)?.RoleIds;
                        if (roles != null)
                        {
                            var idList = roles.Split(',');
                            foreach (var s in idList)
                            {
                                var rid = int.Parse(s);
                                db.RoleUser.Add(new RoleUser { UserID = entity.UserId, RoleID = rid });
                            }
                        }

                        db.DepartmentUsers.Add(new DepartmentUsers { UserID = entity.UserId, DepartID = deptId });
                        TempData["result"] = db.SaveChanges() > 0 ? "true" : "false";
                    }
                    else
                    {
                        TempData["result"] = "UserNameError";
                    }

                }
            }
            catch (Exception)
            {
                TempData["result"] = "false";
            }
            GetRoles();
            return View(entity);
        }
        private void GetRoles()
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    ViewBag.RoleItemList = db.ArrRoles.OrderBy(p => p.ID).Take(2).Select(r => new SelectListItem()
                    {
                        Text = r.RoleName,
                        Value = r.RoleID.ToString(),
                        Selected = r.ID == 2
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
            }

        }

        /// <summary>
        /// 禁用
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int UserID)
        {
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Users.FirstOrDefault(p => p.UserId == UserID);
                    var u = db.DepartmentUsers.FirstOrDefault(i => i.UserID == UserID);
                    db.Users.Remove(entity);
                    db.DepartmentUsers.Remove(u);
                    var result = db.SaveChanges();
                    paras.code = result > 0 ? 200 : 500;
                    paras.status = result > 0;
                    paras.msg = result > 0 ? "Success" : "Field";
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 删除组织结构
        /// </summary>
        /// <param name="departId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteDepartMents(int departId)
        {
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Departments.FirstOrDefault(p => p.ID == departId);
                    var deptUsers = db.DepartmentUsers.Where(i => i.DepartID == departId).ToList();
                    if (deptUsers.Any())
                    {
                        deptUsers.ForEach(u =>
                        {
                            var user = db.Users.FirstOrDefault(i => i.UserId == u.UserID);
                            db.DepartmentUsers.Remove(u);//删除用户部门关系
                            if (user != null)
                            {
                                db.Users.Remove(user);//删除用户
                                var roleUsers = db.RoleUser.Where(ru => ru.UserID == user.UserId).ToList();
                                if (roleUsers.Any())
                                {
                                    roleUsers.ForEach(r =>
                                    {
                                        var rr = db.RoleUser.FirstOrDefault(j => j.ID == r.ID);
                                        if (rr != null)
                                        {
                                            db.RoleUser.Remove(r);//删除用户角色关系
                                        }
                                    });
                                }
                            }
                        });
                    }
                    if (entity != null) db.Departments.Remove(entity);//删除部门
                    var readyToRemove = db.DeptRoleRelation.FirstOrDefault(i => i.DeptId == entity.ID);
                    if (readyToRemove != null) db.DeptRoleRelation.Remove(readyToRemove);

                    var result = db.SaveChanges();
                    paras.code = result > 0 ? 200 : 500;
                    paras.status = result > 0;
                    paras.msg = result > 0 ? "Success" : "Field";
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 启用
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Unlock(int UserID)
        {
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Users.Where(p => p.UserId == UserID).FirstOrDefault();
                    entity.IsActive = false;
                    var result = db.SaveChanges();
                    paras.code = result == 1 ? 200 : 500;
                    paras.status = result == 1 ? true : false;
                    paras.msg = result == 1 ? "Success" : "Field";
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 禁用
        /// </summary>
        /// <param name="MenuID"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult OnLock(int UserID)
        {
            JsonParas paras = new JsonParas();
            try
            {
                using (var db = new PapsEntities())
                {
                    var entity = db.Users.Where(p => p.UserId == UserID).FirstOrDefault();
                    entity.IsActive = true;
                    var result = db.SaveChanges();
                    paras.code = result == 1 ? 200 : 500;
                    paras.status = result == 1 ? true : false;
                    paras.msg = result == 1 ? "Success" : "Field";
                }
            }
            catch
            {
                paras.code = 500;
                paras.status = false;
                paras.msg = "Field";
            }
            return Json(paras, JsonRequestBehavior.AllowGet);
        }
        #region treeview方法
        public string MenuTreeView()
        {
            string strContent = "<ul class=\"main_parent\">";
            using (var db = new PapsEntities())
            {
                var allDepartments = db.Departments.ToList();
                var currentItem = db.Departments.Where(p => p.ParentID == 0).ToList();
                var i = 0;
                foreach (var item in currentItem)
                {
                    var subCounts = db.Departments.Count(p => p.ParentID == item.ID);
                    var icos = subCounts == 0 ? "empty" : "plus";

                    var user = "<input type='button' class='btn btn-warning userList btn-xs' value='用户列表' onclick=\"listUser('" + item.ID + "')\" />";
                    var authorize = "<input type='button' class='btn btn-success userRole btn-xs' value='设置角色' onclick=\"authorizeRole('" + item.ID + "')\" />";

                    var delete = "<input type='button' class='btn btn-danger btn-xs' value='删除' onclick=\"onDelete('" + item.ID + "')\" />";
                    strContent += string.Format("<li onmouseover=\"manager('span_" + item.ID + "')\" id=" + item.ID + " onmouseout=\"overmanager('span_" + item.ID + "')\"><i id=\"li_" + item.ID + "\" class=\"{0}\" onclick=\"Fold('li_" + item.ID + "','ul_" + item.ID + "','span_" + item.ID + "');\"></i><span class=\"span-item\" style=\"white-space: nowrap;\"><span class=\"css" + item.ID + "\">{1}</span><span id='span_" + item.ID + "' style='display:none;'><input type='button' class='btn btn-primary btn-xs' value='添加' onclick=\"Add('" + item.ID + "')\" /><input type='button' class='btn btn-info btn-xs' value='修改' onclick=\"Edit('" + item.ID + "')\"/>{2}{3}{4}</span></span></li>", icos, item.Name, user, authorize, delete);
                    strContent += MenuSubTree(allDepartments, item.ID, i);
                    strContent += "</li>";
                }
            }
            strContent += "</ul>";
            return strContent;
        }
        public string MenuSubTree(List<Departments> db, int intParentID, int i)
        {
            var currentItems = db.Where(p => p.ParentID == intParentID).ToList();
            string strSubContent = currentItems.Count > 0 ? "<ul id=\"ul_" + intParentID + "\" class=\"sub-item\">" : "";
            i++;
            foreach (var item in currentItems)
            {
                int intLeft = i * 26;
                var subCounts = db.Count(p => p.ParentID == item.ID);
                var icos = subCounts == 0 ? "empty" : "plus";
                var user = "<input type='button' class='btn btn-warning btn-xs' value='用户列表' onclick=\"listUser('" + item.ID + "')\" />";
                var authorize = "<input type='button' class='btn btn-success btn-xs' value='设置角色' onclick=\"authorizeRole('" + item.ID + "')\" />";
                var delete = "<input type='button' class='btn btn-danger btn-xs' value='删除' onclick=\"onDelete('" + item.ID + "')\" />";
                var canAdd = "<input type='button' class='btn btn-primary btn-xs' value='添加' onclick=\"Add('" + item.ID + "')\" />";

                strSubContent += string.Format("<li onmouseover=\"manager('span_" + item.ID + "')\" id=" + item.ID + " onmouseout=\"overmanager('span_" + item.ID + "')\" style=\"padding-left:" + intLeft + "px;\" ><i id=\"li_" + item.ID + "\" class=\"{0}\" onclick=\"Fold('li_" + item.ID + "','ul_" + item.ID + "','span_" + item.ID + "');\"></i><span class=\"span-item\" style=\"white-space: nowrap;\"><span class=\"css" + item.ID + "\">{1}</span><span id='span_" + item.ID + "' style='display:none;'>{5}<input type='button' class='btn btn-info btn-xs' value='修改' onclick=\"Edit('" + item.ID + "')\" />{2}{3}{4}</span></span></li>", icos, item.Name, user, authorize, delete, canAdd);
                strSubContent += MenuSubTree(db, item.ID, i);
                strSubContent += "</li>";
            }
            strSubContent += currentItems.Count > 0 ? "</ul>" : "";
            return strSubContent;
        }
        [HttpPost]
        public JsonResult ActiveMenu(int ActiveMenuID)
        {

            List<int> list = new List<int>();
            var parentID = dbDeparts.Where(p => p.ID == ActiveMenuID).Select(p => p.ParentID).FirstOrDefault();
            list.Add(parentID);
            GetParentIDs(list, parentID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        private void GetParentIDs(List<int> list, int menuId)
        {
            var parentID = dbDeparts.Where(p => p.ID == menuId).Select(p => p.ParentID).FirstOrDefault();
            if (parentID != 0)
            {
                list.Add(parentID);
                GetParentIDs(list, parentID);
            }
        }
        #endregion


        [BaseController(IsCheck = true)]
        public ActionResult RoleDepartment(int DepartmentID)
        {
            return View();
        }

        [BaseController(IsCheck = true)]
        public ActionResult RoleUser(int UserID)
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetMenus(string roleId)
        {
            var l = DatabaseServices.GetMenus();
            var rm = new List<RoleMenu>();
            var indexs = new List<int>();
            var treeIndex = 0;
            if (!string.IsNullOrEmpty(roleId)) rm = DatabaseServices.GetRoleMenu(roleId).OrderBy(i => i.MenuID).ToList();
            GetReadyToCheckedIds(l, rm, indexs, treeIndex);
            return Json(new { list = l, roleMenu = rm, ids = indexs }, JsonRequestBehavior.AllowGet);
        }
        private int GetReadyToCheckedIds(List<TreeData> td, List<RoleMenu> rm, List<int> indexs, int treeIndex)
        {
            td.ForEach(t =>//遍历树节点
            {
                if (rm.Count(i => i.MenuID == t.id && i.MenuName == t.text) > 0)
                {
                    indexs.Add(treeIndex);
                }
                treeIndex++;
                if (t.nodes != null)
                {
                    treeIndex = GetReadyToCheckedIds(t.nodes, rm, indexs, treeIndex);
                }
            });
            return treeIndex;
        }


        [HttpGet]
        public JsonResult GetMenusByUserId(string userId)
        {
            var l = DatabaseServices.GetMenus();
            var rm = new List<RoleMenu>();
            if (!string.IsNullOrEmpty(userId)) rm = DatabaseServices.GetRoleMenuByUserId(userId);
            return Json(new { list = l, roleMenu = rm }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUserRoles(int userId)
        {
            using (var db = new PapsEntities())
            {
                var list = db.RoleUser.Where(i => i.UserID == userId).ToList();
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AuthorizeRole(int page, int deptId, string selectedIds)
        {
            try
            {
                int size = 15;
                var roleIds = "";
                using (var db = new PapsEntities())
                {
                    var pageCount = db.ArrRoles.Count();
                    var entity = db.ArrRoles.OrderByDescending(p => p.ID).Skip((page - 1) * size).Take(size).ToList();
                    var pagedlist = new StaticPagedList<ArrRoles>(entity, page, size, pageCount);
                    var rIds = db.DeptRoleRelation.Where(i => i.DeptId == deptId).Select(i => i.RoleIds).Distinct().ToList();
                    if (rIds.Any())
                    {
                        rIds.ForEach(i =>
                        {
                            var arr = i.Split(',');
                            foreach (var a in arr)
                            {
                                if (roleIds.Contains(a)) continue;
                                roleIds += i + ",";
                            }
                        });
                    }
                    ViewBag.DeptId = deptId;
                    ViewBag.RoleIds = roleIds;
                    return View(pagedlist);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleIds"></param>
        /// <param name="departId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SaveAuthorize(string roleIds, string deptId)
        {
            var listIds = JsonConvert.DeserializeObject<List<int>>(roleIds);
            listIds = listIds.Distinct().ToList();
            var deptIds = RoleHandler.RecursionDepts(int.Parse(deptId));
            var r = false;
            if (deptId.Length > 0)
            {
                r = RoleHandler.SaveDeptRoleRelation(deptIds, listIds);
            }
            Session["sIds"] = "";
            return Json(r, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddUserGroup()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddUserGroup(Departments entity)
        {
            try
            {
                using (var db = new PapsEntities())
                {
                    entity.IsAsync = true;
                    entity.IsDel = false;
                    entity.ParentID = 0;
                    entity.IsType = 1;//用户组，不是公司
                    entity.StructureType = 2;//公司
                    db.Departments.Add(entity);
                    db.SaveChanges();
                    if (entity.ID >= 0)
                    {
                        TempData["result"] = "true";
                    }
                    else
                    {
                        TempData["result"] = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["result"] = "false";
            }
            return View(entity);
        }
        [HttpGet]
        public JsonResult UpdatesIds(string sids)
        {
            var list = JsonConvert.DeserializeObject<List<string>>(sids);
            list.ForEach(i =>
            {
                if (!string.IsNullOrEmpty(i)) Session["sIds"] += i + ",";
            });
            var ids = Session["sIds"].ToString().Trim(',').Split(',').Distinct().ToList();
            var result = string.Join(",", ids);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult RefreshIds(string sids)
        {
            var list = JsonConvert.DeserializeObject<List<string>>(sids);
            Session["sIds"] = "";
            list.ForEach(i =>
            {
                if (!string.IsNullOrEmpty(i)) Session["sIds"] += i + ",";
            });
            return Json(Session["sIds"].ToString().Trim(','), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult refreshArr(string arr)
        {
            Session["Arr"] = arr;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}