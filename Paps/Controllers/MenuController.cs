﻿using Microsoft.Ajax.Utilities;
using Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Paps.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult Index()
        {
            var user = (Users)Session["UserInfo"];
            if (user == null) user = new Users { UserId = 447 };
            using (var db = new PapsEntities())
            {
                List<int> roles = null;// comment by Ted on 20180524 db.Roles.Where(p => p.UserID == user.UserId && p.IsDel == false).Select(p => p.MenuID).ToList();
                var roleUser = db.RoleUser.Where(i => i.UserID == user.UserId).Select(i => i.RoleID).ToList();
                var arrRole = db.ArrRoles.Where(i => roleUser.Contains(i.ID)).Select(i => i.RoleID).ToList();
                var roleMenu = db.RoleMenu.Where(i => arrRole.Contains(i.RoleID)).Select(i => i.MenuID).ToList();
                ViewBag.TreeView = MenuTreeView(roleMenu);
                return View();
            }
        }

        [HttpGet]
        public string SearchMenu(string keyWord = "")
        {
            var user = (Users)Session["UserInfo"] ?? new Users { UserId = 447 };
            using (var db = new PapsEntities())
            {
                var roleUser = db.RoleUser.Where(i => i.UserID == user.UserId).Select(i => i.RoleID).ToList();
                var arrRole = db.ArrRoles.Where(i => roleUser.Contains(i.ID)).Select(i => i.RoleID).ToList();
                var roleMenu = db.RoleMenu.Where(i => arrRole.Contains(i.RoleID)).Select(i => i.MenuID).ToList();
                return MenuTreeView(roleMenu, keyWord);
            }
        }
        #region treeview方法
        public string MenuTreeView(List<int> roles, string keyWord = "")
        {
            if (roles == null || roles.Count == 0) return "";
            var strContent = $"<div class='col-sm-12 form-inline'><input type='text' id='menuKeyWord' class='form-control col-sm-8 input-sm' value='{keyWord}' placeholder='关键字'/><input type='button' class='col-sm-3 btn btn-xs btn-default' onclick='searchMenu()' id='searchMenu' value='搜索' /></div>";
            strContent += "<br/>";
            strContent += "<ul class=\"menus\" id=\"nav\">";
            using (var db = new PapsEntities())
            {
                var currentItem = db.Menus.Where(p => p.ParentID == 1 && p.IsDel == false).ToList();
                var menus = db.Menus.Where(m => roles.Contains(m.ID)).ToList();
                menus.Where(m => m.StructureType != 4).ForEach(m => { m.ServicePath = $"charts?menuId={m.ID}"; });
                menus.Where(m => m.StructureType == 4).ForEach(m =>
                    {
                        if (m.IsType == 1 && !string.IsNullOrEmpty(m.ServicePath))
                        {
                            m.ServicePath = $"RTWNew?MendId={m.ID}";
                        }
                    });
                if (!string.IsNullOrEmpty(keyWord))
                {
                    var lastLevelMenus = menus.Where(m => m.StructureType == 4 && m.Name.Contains(keyWord)).ToList();
                    var lastLevelParentIds = lastLevelMenus.Select(l => l.ParentID).ToList();
                    var thirdLevelMenus = menus.Where(m => m.StructureType == 3 && m.Name.Contains(keyWord) || lastLevelParentIds.Contains(m.ID)).ToList();
                    var thirdLevelParentIds = thirdLevelMenus.Select(l => l.ParentID).ToList();
                    var secondLevelMenus = menus.Where(m => m.StructureType == 2 && m.Name.Contains(keyWord) || thirdLevelParentIds.Contains(m.ID)).ToList();
                    menus = menus.Where(m => lastLevelMenus.Contains(m) || secondLevelMenus.Contains(m) || thirdLevelMenus.Contains(m)).ToList();
                }
                var i = 0;
                foreach (var item in currentItem)
                {
                    if (!roles.Contains(item.ID) || !menus.Contains(item))
                    {
                        continue;
                    }
                    var icos = "<i class=\"files fa fa-plus-square-o\"></i>";
                    var strServicePath = string.IsNullOrEmpty(item.ServicePath) ? "" + item.Name + "" : "<a data-href='" + item.ServicePath + "' onclick=\"ShowMenu('" + item.ID + "','" + item.Name + "')\">" + item.Name + "</a>";
                    var childItems = menus.Where(m => m.ParentID == item.ID);
                    var totalItem = 0;
                    var onlineItem = 0;
                    childItems.ForEach(c =>
                    {
                        totalItem += menus.Count(cm => cm.ParentID == c.ID && !cm.IsDel);
                        onlineItem += menus.Count(oi => oi.ParentID == c.ID && oi.IsActive && !oi.IsDel);//禁用的标段或者隧道也不参与在线数计算
                    });

                    var onlineNum = item.IsShowOnline ? $"<span class='badge bg-danger'> {onlineItem}/{totalItem} </span>" : "";//在线设备数量
                    strContent += string.Format("<li id=\"menu_{0}\" name=\"{3}\" onclick=\"ActiveMenu(this.id)\">{1} {2} {4}</li>", item.ID, icos, strServicePath, item.Name, onlineNum);
                    strContent += MenuSubTree(menus, item.ID, i, roles);
                    strContent += "</li>";
                }
            }
            strContent += "</ul>";
            return strContent;
        }
        public string MenuSubTree(List<Menus> db, int intParentID, int i, List<int> roles)
        {
            var currentItems = db.Where(p => p.ParentID == intParentID && p.IsDel == false).OrderBy(p => p.PIndex).ToList();
            var _menus = db.Where(m => roles.Contains(m.ID)).ToList();
            string strSubContent = currentItems.Count > 0 ? "<ul id=\"sub_menu_" + intParentID + "\" class=\"sub-item\">" : "";
            i++;
            foreach (var item in currentItems)
            {
                if (roles.Contains(item.ID))
                {
                    int intLeft = i * 26;
                    var subCounts = db.Count(p => p.ParentID == item.ID && p.IsDel == false);
                    var icos = subCounts == 0 ? "<i class=\"pages fa fa-plus-square-o\" style='margin-left:" + intLeft + "px' ></i>" : "<i style='margin-left:" + intLeft + "px' class=\"files fa fa-plus-square-o\"></i>";
                    var onclick = "onclick=\"ActiveSubMenu(this.id)\"";
                    var totalItem = _menus.Count(m => m.ParentID == item.ID && !m.IsDel);
                    var onlineItem = 0;
                    onlineItem = _menus.Count(m => m.ParentID == item.ID && m.IsActive && !m.IsDel);
                    var css = item.IsActive ? "IsOnline" : "IsOffline";
                    if (item.StructureType != 4 && onlineItem > 0)
                    {
                        css += " canopen";
                    }

                    var cssIco = item.IsActive ? "onlineIco" : "offlineIco";
                    var strServicePath = string.IsNullOrEmpty(item.ServicePath) ? "" + item.Name + "" : "<a class='" + css + "' data-href='" + item.ServicePath + "' onclick=\"SaveActiveSubMenu('" + item.ID + "','" + item.Name + "', '" + css + "', '" + item.StructureType + "')\">" + item.Name + "</a>";
                    var onlineNum = item.IsShowOnline ? $"<span class='badge bg-danger'> {onlineItem}/{totalItem} </span>" : "";//在线设备数量
                    if (item.StructureType == 4)
                    {
                        icos = "<i class=\"pages " + cssIco + " fa fa-map-marker\" style='margin-left:" + intLeft + "px' ></i>";
                    }
                    strSubContent += string.Format("<li id=\"menu_{0}\" name=\"{4}\" {3}>{1} {2} {5}</li>", item.ID, icos, strServicePath, onclick, item.Name, onlineNum);
                    strSubContent += MenuSubTree(db, item.ID, i, roles);
                    strSubContent += "</li>";
                }
            }
            strSubContent += currentItems.Count > 0 ? "</ul>" : "";
            return strSubContent;
        }
        [HttpPost]
        public JsonResult ActiveMenu(int ActiveMenuID)
        {
            using (var db = new PapsEntities())
            {
                List<int> list = new List<int>();
                var parentID = db.Menus.Where(p => p.ID == ActiveMenuID).Select(p => p.ParentID).FirstOrDefault();
                list.Add(parentID);
                GetParentIDs(list, db, parentID);
                return Json(list, JsonRequestBehavior.AllowGet);
            }

        }
        private void GetParentIDs(List<int> list, PapsEntities db, int menuId)
        {
            var parentID = db.Menus.Where(p => p.ID == menuId).Select(p => p.ParentID).FirstOrDefault();
            if (parentID != 0)
            {
                list.Add(parentID);
                GetParentIDs(list, db, parentID);
            }
        }
        #endregion
    }
}