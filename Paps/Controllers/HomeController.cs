﻿using DAL;
using DAL.Common;
using Model;
using Newtonsoft.Json;
using Paps.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Paps.SignalR;

namespace Paps.Controllers
{
    [BaseController(IsCheck = true)]
    public class HomeController : Controller
    {
        private static string SignId;
        private PapsEntities _entities = new PapsEntities();
        private static string CurrentMenuId;
        private static SocketMsg SocketMsgInstance = new SocketMsg { IsWebFrom = true, QueryModel = new QueryModel() };
        public ActionResult Index()
        {
            SignId = Guid.NewGuid().ToString("N");
            SocketMsgInstance.SignId = SignId;
            ViewBag.ClientId = SignId;
            SocketMsgInstance.QueryModel.MethodName = QueryModelMethodEnum.GetDeptList;
            SocketInstance.SendMsgToClient(SocketMsgInstance, false); //获取部门
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Logout()
        {
            return View();
        }
        [BaseController(IsCheck = false)]
        public ActionResult Login(string returnUrl)
        {
            if (Session != null) Session["Ip"] = null;
            ViewBag.IsAdmin = Request.QueryString["IsAdmin"];
            ViewBag.IsUser = Request.QueryString["IsUser"];
            ViewBag.IsDelete = Request.QueryString["IsDelete"];
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [BaseController(IsCheck = false)]
        public ActionResult DoLogin(string userName, string password, string returnUrl)
        {
            password = EncryptHelper.CreateMd5Hash(password);
            var userModel = DatabaseServices.Login(new Users { UserName = userName, Password = password });
            Session["UserInfo"] = userModel;
            if (userModel != null)
            {
                Session["UserName"] = userModel.FullName;
                if (userModel.RoleId == 1)
                {
                    return Redirect("/Systems/Admin/Index");
                }
                if ((!string.IsNullOrEmpty(returnUrl) && userModel != null) || userModel.IsActive == true)
                {
                    if (userModel.IsActive == true)
                    {
                        TempData["result"] = "IsActive";
                    }
                    return RedirectToAction(returnUrl ?? "Login");
                }
            }
            else
            {
                TempData["result"] = "IsExits";
            }
            if (userModel != null)
            {
                if (DatabaseServices.IsAdmin(userModel.UserId))  // added by Ted on 20180613
                {
                    return Redirect("/Systems/Admin/Index");
                }
            }
            return RedirectToAction(userModel == null ? "Login" : "Index");
            //return RedirectToAction(userModel == null ? "Login" : "RealTimeWatch");
        }

        /// <summary>
        /// 新客户端的实时预览界面
        /// </summary>
        /// <returns></returns>
        public ActionResult RtwNew()
        {
            CurrentMenuId = Request.QueryString["MendId"];
            var db = new PapsEntities();
            var intMenuId = int.Parse(CurrentMenuId);
            var deviceCode = db.Device.FirstOrDefault(d => d.MenuId == intMenuId)?.DeviceCode;
            ViewBag.CurrentMenuId = deviceCode;
            var bgPath = Server.MapPath($"/temp/{deviceCode}bg.jpg");
            SocketMsgInstance.SelfId = CurrentMenuId;
            SocketMsgInstance.DeviceCode = deviceCode;
            if (!System.IO.File.Exists(bgPath))
            {
                SocketMsgInstance.QueryModel.MethodName = QueryModelMethodEnum.GetBackGroundImage;
                SocketInstance.SendMsgToClient(SocketMsgInstance);
            }
            SocketMsgInstance.QueryModel.MethodName = QueryModelMethodEnum.GetDeptList;
            SocketInstance.SendMsgToClient(SocketMsgInstance); //获取部门
            SocketMsgInstance.QueryModel.MethodName = QueryModelMethodEnum.GetTitleList;
            SocketInstance.SendMsgToClient(SocketMsgInstance); //获取职务
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [BaseController(IsCheck = false)]
        public JsonResult GetClientData()
        {
            var db = new PapsEntities();
            var clientData = db.ClientData.AsNoTracking().OrderByDescending(c => c.TableId).FirstOrDefault(d => d.DeviceCode == SocketMsgInstance.DeviceCode);
            return Json(clientData, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 一键查询界面
        /// </summary>
        /// <returns></returns>
        public ActionResult OneKeySearch()
        {
            return View();
        }
        /// <summary>
        /// 木兰客户端的实时预览界面
        /// </summary>
        /// <returns></returns>
        public ActionResult RealTimeWatch()
        {
            ViewBag.Connected = false;
            var ip = Request.QueryString["IpAddress"].ToStringDecrypt();
            if (Session["Ip"] != null && string.IsNullOrEmpty(ip)) ip = Session["Ip"].ToString();
            if (!string.IsNullOrEmpty(ip))
            {
                Session["Ip"] = ip;
                var uri = new Uri(ToUri(ip));
                InterfaceServices.Uri = uri;
                try
                {
                    var rtpsd = InterfaceServices.GetRealTimePersonStatisticsData();
                    var tipi = new TunnelInPerInfo();
                    if (rtpsd != null)
                    {
                        tipi = InterfaceServices.GetTunnelInPerInfos();
                        ViewBag.Connected = true;
                    }

                    ViewBag.RealTimePersonStatisticsData = rtpsd ?? new RealTimePersonStatisticsDataModel();
                    ViewBag.TunnelInPerInfos = tipi;
                    if (rtpsd != null) Session["TunnelType"] = rtpsd.TunnelType;
                    else
                    {
                        Session["TunnelType"] = "单洞";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.RealTimePersonStatisticsData = new RealTimePersonStatisticsDataModel();
                    ViewBag.TunnelInPerInfos = new TunnelInPerInfo();
                }

            }
            else
            {
                ViewBag.RealTimePersonStatisticsData = new RealTimePersonStatisticsDataModel();
                ViewBag.TunnelInPerInfos = new TunnelInPerInfo();
            }

            return View();
        }
        [HttpPost]
        public JsonResult GetPerNum()
        {
            var data = InterfaceServices.GetRealTimePersonStatisticsData();
            return Json(data);
        }
        private string ToUri(string str)
        {
            var uriStr = "";
            //            (!str.ToLower().Contains("asmx") ? (str.TrimEnd('/') + "/SDService.asmx") :
            if (str.ToLower().Contains("http"))
            {
                uriStr = str;
            }
            else
            {
                uriStr = "http://" + str;
            }

            if (!str.ToLower().Contains("asmx")) uriStr = uriStr.TrimEnd('/') + "/SDService.asmx";
            return uriStr;
        }
        public ActionResult GetBsPerInfos(int id)
        {
            var tunnelType = Session["TunnelType"]?.ToString() ?? "单洞";
            var bsPerInfos = new List<BSPerInfo>();
            if (id == 1)
                bsPerInfos = InterfaceServices.GetBsPerInfos(id);
            else if (id == 2 && tunnelType == "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(id);
            }
            else if (id == 2 && tunnelType != "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(id).Where(i => i.SideName == "左").ToList();
            }
            else if (id == 3 && tunnelType != "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(2).Where(i => i.SideName == "右").ToList();
            }
            if (bsPerInfos == null) bsPerInfos = new List<BSPerInfo>();
            return View(bsPerInfos.OrderByDescending(i => i.EnterTime).ToList());
        }

        /// <summary>
        /// 月考勤视图
        /// </summary>
        /// <returns></returns>
        public ActionResult MonthlyReport()
        {
            return View();
        }

        [HttpPost]
        [BaseController(IsCheck = false)]
        public JsonResult GetOnlineRate(string startTime, string endTime, string listIds, string pointTxt, string unit)
        {
            var createTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var list = new List<OnlineRateModel>();
            startTime = startTime + " 00:00:00";
            endTime = endTime + " 23:59:59";
            list = DatabaseServices.GetOnlineRate(startTime, endTime, pointTxt, listIds);
            if (unit == "d") //如果统计指标是天
            {
                var days = Convert.ToDateTime(endTime).Subtract(Convert.ToDateTime(startTime)).Duration().Days;
                if (days <= 0) return null;
                var st = Convert.ToDateTime(startTime);
                var et = Convert.ToDateTime(endTime);
                var listRate = _entities.OnlineRate.Where(o =>
                    o.OnlineTime >= st &&
                    o.OfflineTime <= et).ToList();
                if (!string.IsNullOrEmpty(listIds))
                {
                    var newList = listIds.Split(',');
                    var intList = Array.ConvertAll(newList, int.Parse);
                    listRate = listRate.Where(l => Array.IndexOf(intList, l.UserId) > 0).ToList();
                }

                if (!string.IsNullOrEmpty(pointTxt))
                {
                    listRate = listRate.Where(l => l.PointName == pointTxt).ToList();
                }

                var res = listRate.GroupBy(i => i.UserId).ToList();
                var re = new List<OnlineRateModel>();
                res.ForEach(r =>
                {
                    var onlineDays = 0;
                    for (int i = 0; i <= days; i++)
                    {
                        var sTime = st.AddDays(i);
                        var eTime = sTime.AddHours(23).AddMinutes(59).AddSeconds(59);
                        if (r.Count(x => x.OnlineTime >= sTime && x.OnlineTime <= eTime) > 0)
                        {
                            onlineDays++;
                        }
                    }
                    re.Add(new OnlineRateModel()
                    {
                        OnlineHours = onlineDays,
                        OnlineRate = Math.Round((decimal)(onlineDays / days), 2),
                        PointName = r.FirstOrDefault()?.PointName,
                        RegionId = r.FirstOrDefault()?.RegionId,
                        RegionName = r.FirstOrDefault()?.RegionName,
                        TotalHours = days
                    });
                });

                var summary = re.GroupBy(i => i.RegionName);
                var suList = summary.Select(s => new OnlineRateModel
                {
                    RegionName = s.Key,
                    PointName = s.Count(y => y.RegionName == s.Key).ToString(),
                    RegionId = s.FirstOrDefault()?.RegionId,
                    OnlineHours = s.Sum(x => x.OnlineHours),
                    TotalHours = s.Sum(x => x.TotalHours),
                    OnlineRate = Math.Round(s.Sum(o => o.OnlineHours) / s.Sum(t => t.TotalHours) * 100, 2),
                }).ToList();
                Session["DetailReport"] = re;
                Session["SummaryReport"] = suList;
                return Json(new { detail = re, summary = suList, createtime = createTime });

            }
            var model = list.GroupBy(l => l.RegionName);
            var result = model.Select(x =>
                new OnlineRateModel
                {
                    RegionName = x.Key,
                    PointName = x.Count(y => y.RegionName == x.Key).ToString(),
                    OnlineHours = x.Sum(o => o.OnlineHours),
                    TotalHours = x.Sum(t => t.TotalHours),
                    OnlineRate = Math.Round(x.Sum(o => o.OnlineHours) / x.Sum(t => t.TotalHours) * 100, 2),
                    RegionId = x.FirstOrDefault()?.RegionId
                }).ToList();
            result.Add(new OnlineRateModel()
            {
                RegionName = "总计",
                PointName = result.Sum(i => int.Parse(i.PointName)).ToString(),
                OnlineHours = result.Sum(i => i.OnlineHours),
                TotalHours = result.Sum(i => i.TotalHours),
                OnlineRate = Math.Round(result.Sum(i => i.OnlineHours) / result.Sum(i => i.TotalHours) * 100, 2) //todo 尝试除以零
            });
            Session["DetailReport"] = list;
            Session["SummaryReport"] = result;
            return Json(new { detail = list, summary = result, createtime = createTime });
        }
        public ActionResult DailyReport()
        {
            return View();
        }
        public delegate int AddHandler(int a, int b);
        /// <summary>
        /// 获取日考勤报表
        /// </summary>
        /// <param name="date"></param>
        /// <param name="deptCode"></param>
        /// <returns></returns>

        public ActionResult SysAlarm()
        {

            return View();
        }
        [HttpPost]
        public JsonResult GetSysAlarmData(string startTime, string endTime, string perName, string deptNo, string baseStationNo, string titleNo)
        {
            var list = InterfaceServices.GetSysAlarmData(new SysAlarmQueryModel()
            {
                StartTime = startTime,
                EndTime = endTime,
                DeptNo = deptNo,
                BaseStationNo = baseStationNo,
                PerName = perName
            });
            return Json(list);
        }
        /// <summary>
        /// 超员报警视图
        /// </summary>
        /// <returns></returns>
        public ActionResult OverPerAlarm()
        {
            return View();
        }

        /// <summary>
        /// 获取超员报警信息
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pageNum"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetOverPerAlarmData(string startTime, string endTime, int pageNum = 0, int pageSize = 15)
        {
            SocketMsgInstance.QueryModel = new QueryModel { MethodName = QueryModelMethodEnum.GetOverPerAlarmData, StartTime = startTime, EndTime = endTime, PageIndex = 0, PageSize = 1000 };
            SocketInstance.SendMsgToClient(SocketMsgInstance); //获取部门
            return Json("");
        }
        /// <summary>
        /// 修改密码视图
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            return View();
        }
        /// <summary>
        /// 修改密码事件
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <param name="confirmPassword"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangePassword(string oldPassword, string newPassword, string confirmPassword)
        {
            var result = String.Empty;
            var currentUser = (Users)Session["UserInfo"];
            if (currentUser == null)
            {
                result = "请重新登录";
                return Json(result);
            }
            if (EncryptHelper.CreateMd5Hash(oldPassword) != currentUser.Password)
                result = "请输入正确的原密码";
            else if (newPassword != confirmPassword)
                result = "新密码必须与确认密码相同";

            if (result != String.Empty)
                return Json(result);

            using (var db = new PapsEntities())
            {
                var user = db.Users.FirstOrDefault(p => p.UserId == currentUser.UserId);
                if (user != null)
                {
                    user.Password = EncryptHelper.CreateMd5Hash(newPassword);
                    var saveResult = db.SaveChanges();
                    result = saveResult >= 0 ? "" : "修改失败";
                }
                else
                {
                    result = "修改失败";
                }
            }
            return Json(result);
        }

        public ActionResult OnlineRate()
        {
            return View();
        }

        public ActionResult YearDetail()
        {
            return View();
        }

        public ActionResult MonthDetail()
        {
            return View();
        }
        
        /// <summary>
        /// 标段图标
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public ActionResult Charts(int menuId)
        {
            ViewBag.MenuID = menuId;
            return View();
        }

        public ActionResult LayuiRate()
        {
            return View();
        }
        /// <summary>
        /// 获取在线率统计数据
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="listIds"></param>
        /// <param name="pointTxt"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        [HttpPost]
        [BaseController(IsCheck = false)]
        public JsonResult GetOnlineRateForLayui(string startTime, string endTime, string listIds, string pointTxt, string unit)
        {
            var createTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var list = new List<OnlineRateModel>();
            Session["ReportST"] = startTime;
            Session["ReportET"] = endTime;
            startTime = startTime + " 00:00:00";
            endTime = endTime + " 23:59:59";
            if (listIds == "1") listIds = "";
            if (!string.IsNullOrEmpty(listIds))
            {
                var listId = new List<int> { int.Parse(listIds) };
                listIds = GetRelatedNodeId(listId, int.Parse(listIds));
            }

            list = DatabaseServices.GetOnlineRate(startTime, endTime, pointTxt, listIds);
            var user = (Users)Session["UserInfo"];
            var roleUser = _entities.RoleUser.AsNoTracking().Where(i => i.UserID == user.UserId).Select(i => i.RoleID).ToList();
            var arrRole = _entities.ArrRoles.AsNoTracking().Where(i => roleUser.Contains(i.ID)).Select(i => i.RoleID).ToList();
            var roleMenuIds = _entities.RoleMenu.AsNoTracking().Where(i => arrRole.Contains(i.RoleID)).Select(i => i.MenuID).ToList();
            var allMenus = _entities.Menus.Where(m=>roleMenuIds.Contains(m.ID)).ToList();
            list = list.Where(l => l.UserId != null && roleMenuIds.Contains((int)l.UserId)).ToList(); //筛选只显示用户拥有权限的标段的在线率
            if (unit == "d") //如果统计指标是天
            {
                var days = Convert.ToDateTime(endTime).Subtract(Convert.ToDateTime(startTime)).Duration().Days + 1;
                if (days <= 0) return null;
                var st = Convert.ToDateTime(startTime);
                var et = Convert.ToDateTime(endTime);
                var listRate = _entities.OnlineRate.Where(o => o.OnlineTime >= st && o.OfflineTime <= et && roleMenuIds.Contains((int)o.UserId)).ToList();
                if (!string.IsNullOrEmpty(listIds))
                {
                    var newList = listIds.Split(',');
                    var intList = Array.ConvertAll(newList, int.Parse);
                    listRate = listRate.Where(l => Array.IndexOf(intList, l.UserId) > 0).ToList();
                }

                if (!string.IsNullOrEmpty(pointTxt))
                {
                    listRate = listRate.Where(l => l.PointName.Contains(pointTxt) || l.RegionName.Contains(pointTxt)).ToList();
                }

                var res = listRate.GroupBy(i => i.UserId).ToList();
                var re = new List<OnlineRateModel>();
                res.ForEach(r =>
                {
                    var onlineDays = 0;
                    for (int i = 0; i <= days; i++)
                    {
                        var sTime = st.AddDays(i);
                        var eTime = sTime.AddHours(23).AddMinutes(59).AddSeconds(59);
                        if (r.Count(x => x.OnlineTime >= sTime && x.OnlineTime <= eTime) > 0)
                        {
                            onlineDays++;
                        }
                    }
                    re.Add(new OnlineRateModel()
                    {
                        OnlineHours = onlineDays,
                        OnlineRate = Math.Round((decimal)((float)onlineDays / (float)days), 2) * 100,
                        PointName = r.FirstOrDefault()?.PointName,
                        RegionId = r.FirstOrDefault()?.RegionId,
                        RegionName = r.FirstOrDefault()?.RegionName,
                        TotalHours = days
                    });
                });

                var summary = re.GroupBy(i => i.RegionName);

                var suList = summary.Select(s => new OnlineRateModel
                {
                    RegionName = s.Key,
                    PointName = s.Count(y => y.RegionName == s.Key).ToString(),
                    RegionId = s.FirstOrDefault()?.RegionId,
                    OnlineHours = s.Sum(x => x.OnlineHours),
                    TotalHours = s.Sum(x => x.TotalHours),
                    OnlineRate = Math.Round(s.Sum(o => o.OnlineHours) / s.Sum(t => t.TotalHours) * 100, 2),
                }).ToList();
                suList = suList.OrderByDescending(s => s.OnlineRate).ToList();
                for (int i = 0; i < suList.Count; i++) //根据在线率计算排名
                {
                    suList[i].Pm = i + 1;
                }
                Session["DetailReport"] = re;
                Session["SummaryReport"] = suList;
                return Json(new { code = 0, count = 100, data = re, summaryData = suList });

            }
            var model = list.GroupBy(l => l.RegionName);
            var result = model.Select(x =>
                new OnlineRateModel
                {
                    RegionName = x.Key,
                    PointName = allMenus.Count(p => p.ParentID == allMenus.FirstOrDefault(m => m.Name == x.Key).ID).ToString(),
                    OnlineHours = x.Sum(o => o.OnlineHours),
                    TotalHours = x.Sum(t => t.TotalHours),
                    OnlineRate = Math.Round(x.Sum(o => o.OnlineHours) / x.Sum(t => t.TotalHours) * 100, 2),
                    RegionId = x.FirstOrDefault()?.RegionId
                }).ToList();
            result = result.OrderByDescending(r => r.OnlineRate).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                result[i].Pm = i + 1;
            }
            Session["DetailReport"] = list;
            Session["SummaryReport"] = result;
            return Json(new { code = 0, count = 100, data = list, summaryData = result });
        }

        /// <summary>
        /// 获取一键查询数据的接口
        /// </summary>
        /// <param name="workPro"></param>
        /// <param name="type"></param>
        /// <param name="year"></param>
        /// <param name="keyWord"></param>
        /// <returns></returns>
        [HttpPost, Route("GetOneKeyData")]
        public string GetOneKeyData(string workPro, string type, int year, string keyWord)
        {
            SocketMsgInstance.QueryModel.MethodName = QueryModelMethodEnum.GetYearData;
            SocketMsgInstance.QueryModel.Year = year;
            SocketMsgInstance.QueryModel.Keyword = keyWord;
            SocketInstance.SendMsgToClient(SocketMsgInstance);
            return "";
        }
        private string GetRelatedNodeId(List<int> listId, int id)
        {
            using (var db = new PapsEntities())
            {
                var list = db.Menus.Where(i => i.ParentID == id).Select(i => i.ID).ToList();
                if (!list.Any()) return string.Join(",", listId);
                list.ForEach(i =>
                    {
                        listId.Add(i);
                        GetRelatedNodeId(listId, i);
                    }
                );
            }
            return string.Join(",", listId);
        }

        [BaseController(IsCheck = false)]
        public ActionResult RedirectLogin(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult ExportMonthlyReport(string month, string name)
        {
            var path = Server.MapPath("~/temp/MonthlyReport.xlsx");
            var db = new PapsEntities();
            var clientData = db.ClientData.FirstOrDefault(c => c.DeviceCode == SocketMsgInstance.DeviceCode);
            var list = JsonConvert.DeserializeObject<List<MonthReport>>(clientData.MonthData);
            if (list == null)
            {
                ViewBag.List = "no data";
                return Redirect("MonthlyReport");
            }
            ExcelHelper.GenerateMonthlyReport(list, path, month, name);
            return File(path, "application/vnd.ms-excel", $"{name}工点{month}月考勤报表_{DateTime.Now:yyyyMMdd}.xlsx");
        }
        public ActionResult ExportDailyReport(string date, string name, string dailyData)
        {
            var path = Server.MapPath("~/temp/DailyReport.xlsx");
            var db = new PapsEntities();
            var clientData = db.ClientData.FirstOrDefault(d => d.DeviceCode == SocketMsgInstance.DeviceCode);
            var list = JsonConvert.DeserializeObject<List<NewDayReport>>(clientData.DailyData);
            var listDayReport = new List<DayReport>();
            list.ForEach(l =>
            {
                listDayReport.Add(new DayReport()
                {
                    BsPerInfo = new BSPerInfo()
                    {
                        IdCardNo = l.IdCardNo,
                        Name = l.Name,
                        Dept = l.Dept,
                        Title = l.Title,
                        EnterTime = l.EnterTime
                    },
                    WorkingTime = l.WorkingTime
                });
            });
            ExcelHelper.GenerateDailyReport(listDayReport, path, date, name);
            return File(path, "application/vnd.ms-excel", $"{name}工点{date}日考勤报表_{DateTime.Now:yyyyMMdd}.xlsx");
        }

        public ActionResult TabsPage()
        {
            return View();
        }

        public ActionResult testPartiView()
        {
            return PartialView();
        }

        /// <summary>
        /// 在客户端生成文件之后，平台再下载，要不然客户端如果有100w进出记录，全都接收过来再生成会有很多问题
        /// </summary>
        /// <param name="name"></param>
        /// <param name="year"></param>
        /// <param name="keyWord"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ExportYearDetail(string name, int year, string keyWord)
        {
            name = name.Replace("shinetech", "#");
            var fileName = $"{year}年{name}工点统计报表_{DateTime.Now:yyyyMMdd}.xlsx";
            var fullFileName = Server.MapPath("~/temp/" + fileName);
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetExcelPath,
                Year = year,
                Keyword = keyWord,
                FileName = fileName,
                FullFileName = fullFileName
            };
            return Json(SocketMsgInstance, JsonRequestBehavior.AllowGet);
        }

        [BaseController(IsCheck = false)]
        public ActionResult Chat()
        {
            var clientName = "聊客-" + Guid.NewGuid().ToString("N"); //这里是唯一标识客户端的地方
            ViewBag.ClientName = clientName;
            var onLineUserList = ChatHub.OnLineUsers.Select(u => new SelectListItem() { Text = u.Value, Value = u.Key }).ToList();
            onLineUserList.Insert(0, new SelectListItem() { Text = "-所有人-", Value = "" });
            ViewBag.OnLineUsers = onLineUserList;
            return View();
        }

        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public void GetConfigData()
        {
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetConfigData
            };
            SocketInstance.SendMsgToClient(SocketMsgInstance);
        }
        /// <summary>
        /// 获取定位信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public void GetLocationData()
        {
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetLocationData
            };
            SocketInstance.SendMsgToClient(SocketMsgInstance);
        }
        /// <summary>
        /// 获取客户端Catalog
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetTypes()
        {
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetTypes
            };
            SocketInstance.SendMsgToClient(SocketMsgInstance);
            return "";
        }
        /// <summary>
        /// 重新获取部门信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetDeptList()
        {
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetDeptList
            };
            SocketInstance.SendMsgToClient(SocketMsgInstance);
            return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetBackgroundImg()
        {
            SocketMsgInstance.QueryModel = new QueryModel
            {
                MethodName = QueryModelMethodEnum.GetBackGroundImage
            };
            SocketInstance.SendMsgToClient(SocketMsgInstance);
            return "";
        }

        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpGet]
        public bool IsExcelExist(string fileName)
        {
            return System.IO.File.Exists(Server.MapPath($"~/temp/{fileName}"));
        }
    }
}