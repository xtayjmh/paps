﻿using DAL;
using Model;
using Newtonsoft.Json;
using Paps.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using static Model.LoginModel;

namespace Paps.Controllers
{
    /// <summary>
    /// 为移动端封装的JSON格式的接口
    /// </summary>
    [RoutePrefix("api/Services")]
    public class ServicesController : ApiController
    {
        /// <summary>
        /// 移动端登录-手机app登录使用
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, Route("ClientLogin")]
        public IHttpActionResult ClientLogin(Users user)
        {
            user.Password = EncryptHelper.CreateMd5Hash(user.Password);
            var userModel = DatabaseServices.Login(user);
            return Json(userModel);
        }

        /// <summary>
        /// 基站列表
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetBaseStationList")]
        public IHttpActionResult GetBaseStationList()
        {
            var list = InterfaceServices.GetBaseStationList();
            return Json(list);
        }
        /// <summary>
        /// 日考勤报表
        /// </summary>
        /// <param name="date">xxxx-xx-xx日期格式</param>
        /// <param name="deptNo">部门编号</param>
        /// <returns></returns>
        [HttpGet, Route("GetDayReport")]
        public IHttpActionResult GetDayReport(string date, string deptNo)
        {
            return Json(InterfaceServices.GetDayReport(date, deptNo));
        }

        /// <summary>
        /// 基站人员信息
        /// </summary>
        /// <param name="BSType">1-考勤基站，获取当前隧道内人员的信息。2-掌子面基站，获取当前掌子面定位基站附近人员的信息</param>
        /// <param name="TunnelType">隧道类型：单洞、双洞、斜井</param>
        /// <returns></returns>
        [HttpGet, Route("GetBSPerInfo")]
        public IHttpActionResult GetBsPerInfo(int BSType, string TunnelType)
        {
            var bsPerInfos = new List<BSPerInfo>();
            if (BSType == 1)
                bsPerInfos = InterfaceServices.GetBsPerInfos(BSType);
            else if (BSType == 2 && TunnelType == "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(BSType);
            }
            else if (BSType == 2 && TunnelType != "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(BSType).Where(i => i.SideName == "左").ToList();
            }
            else if (BSType == 3 && TunnelType != "单洞")
            {
                bsPerInfos = InterfaceServices.GetBsPerInfos(2).Where(i => i.SideName == "右").ToList();
            }
            return Json(bsPerInfos);
        }
        /// <summary>
        /// 部门列表
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetDeptList")]
        public IHttpActionResult GetDeptList()
        {
            return Json(InterfaceServices.GetDeptList());
        }
        /// <summary>
        /// 月考勤报表
        /// </summary>
        /// <param name="date">月份格式xxxx-xx</param>
        /// <param name="deptNo">部门编号</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMonthReport")]
        public IHttpActionResult GetMonthReport(string date, string deptNo)
        {
            return Json(InterfaceServices.GetMonthReport(date, deptNo));
        }
        /// <summary>
        /// 超员报警总数
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet, Route("GetOverPerAlarmCount")]
        public IHttpActionResult GetOverPerAlarmCount(string startTime, string endTime)
        {
            return Json(InterfaceServices.GetOverPerAlarmDataCount(startTime, endTime));
        }

        /// <summary>
        /// 超员报警详细信息
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页显示数量</param>
        /// <returns></returns>
        [HttpGet, Route("GetOverPerAlarmData")]
        public IHttpActionResult GetOverPerAlarmData(string startTime, string endTime, int pageIndex, int pageSize)
        {
            return Json(InterfaceServices.GetOverPerAlarmData(startTime, endTime, pageIndex, pageSize));
        }
        /// <summary>
        /// 实施人员统计数据
        /// </summary>
        /// <returns>返回值包含TunnelType：单洞、双洞、斜井，Left对应左掌子面人数，Right对应右掌子面人数</returns>
        [HttpGet, Route("GetRealTimePersonStatisticsData")]
        public IHttpActionResult GetRealTimePersonStatisticsData()
        {
            var mode = InterfaceServices.GetRealTimePersonStatisticsData();
            mode.TunnelType = mode.TunnelType ?? "单洞";
            return Json(mode);
        }

        /// <summary>
        /// 系统报警总数
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("GetSysAlarmCount")]
        public IHttpActionResult GetSysAlarmCount(SysAlarmQueryModel queryModel)
        {
            return Json(InterfaceServices.GetSysAlarmCount(queryModel));
        }

        /// <summary>
        /// 系统报警详细信息
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost, Route("GetSysAlarmData")]
        public IHttpActionResult GetSysAlarmData(SysAlarmQueryModel queryModel)
        {
            return Json(InterfaceServices.GetSysAlarmData(queryModel));
        }
        /// <summary>
        /// 获取职务列表
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTitleList")]
        public IHttpActionResult GetTitleList()
        {
            return Json(InterfaceServices.GetTitleList());
        }
        /// <summary>
        /// 隧道内人员信息
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetTunnelInPerInfo")]
        public IHttpActionResult GetTunnelInPerInfo()
        {
            return Json(InterfaceServices.GetTunnelInPerInfos());
        }

        /// <summary>
        /// 设置客户端ip地址
        /// </summary>
        /// <param name="ip">客户端IP地址192.168.1.1</param>
        /// <returns></returns>
        [HttpGet, Route("SetUrl")]
        public IHttpActionResult SetUrl(string ip)
        {
            var uri = new Uri(ToUri(ip));
            InterfaceServices.Uri = uri;
            return Json(new { Result = "OK" });
        }
        private string ToUri(string str)
        {
            var uriStr = "";
            if (str.ToLower().Contains("http"))
            {
                uriStr = str;
            }
            else
            {
                uriStr = "http://" + str;
            }

            if (!str.ToLower().Contains("asmx")) uriStr = uriStr.TrimEnd('/') + "/SDService.asmx";
            return uriStr;
        }
        /// <summary>
        /// 获取树形结构
        /// </summary>
        /// <param name="userId">登录的时候返回用户模型的UserId</param>
        /// <returns></returns>
        [HttpGet, Route("GetMenus")]
        public IHttpActionResult GetMenus(int userId)
        {
            using (var db = new PapsEntities())
            {
                var menus = db.Menus.ToList();
                var menusMode = new List<MenusMode>();
                var roleUser = db.RoleUser.Where(i => i.UserID == userId).Select(i => i.RoleID).ToList();
                var arrRole = db.ArrRoles.Where(i => roleUser.Contains(i.ID)).Select(i => i.RoleID).ToList();
                var roleMenu = db.RoleMenu.Where(i => arrRole.Contains(i.RoleID)).Select(i => i.MenuID).ToList();
                menus.Where(m => m.ParentID == 0 && !m.IsDel && roleMenu.Contains(m.ID)).ToList().ForEach(i =>
                {
                    if (roleMenu.Contains(i.ID))
                    {
                        menusMode.Add(new MenusMode
                        {
                            AccLoginName = i.AccLoginName,
                            AccLoginPwd = i.AccLoginPwd,
                            DepartmentID = i.DepartmentID,
                            ID = i.ID,
                            ItemIp = i.ItemIp,
                            Name = i.Name,
                            IsAsync = i.IsAsync,
                            IsDel = i.IsDel,
                            IsType = i.IsType,
                            ParentID = i.ParentID,
                            ServicePath = i.ServicePath
                        });
                    }
                });
                Recursion(menus, menusMode, roleMenu);
                return Json(menusMode);
            }

        }

        [HttpGet, Route("GetMenusJson")]
        public IHttpActionResult GetMenusJson()
        {
            var list = DatabaseServices.GetMenus();
            return Json(list);
        }
        private void Recursion(List<Menus> allMenus, List<MenusMode> list, List<int> roles)
        {
            list.ForEach(l =>
            {
                l.ChildMenus = new List<MenusMode>();
                if (roles.Contains(l.ID))
                {
                    var childItems = allMenus.Where(j => j.ParentID == l.ID && roles.Contains(j.ID)).ToList();
                    if (childItems.Any())
                    {
                        childItems.ForEach(i =>
                        {
                            if (roles.Contains(i.ID))
                            {
                                l.ChildMenus.Add(new MenusMode()
                                {
                                    AccLoginName = i.AccLoginName,
                                    AccLoginPwd = i.AccLoginPwd,
                                    DepartmentID = i.DepartmentID,
                                    ID = i.ID,
                                    ItemIp = i.ItemIp,
                                    Name = i.Name,
                                    IsAsync = i.IsAsync,
                                    IsDel = i.IsDel,
                                    IsType = i.IsType,
                                    ParentID = i.ParentID,
                                    ServicePath = i.ServicePath
                                });
                                Recursion(allMenus, l.ChildMenus, roles);
                            }
                        });
                    }
                }
            });
        }
        public class MenusMode : Menus
        {
            public List<MenusMode> ChildMenus { get; set; }
        }

        public const string Key = "shinetec";//加密密钥必须为8位

        private ClientLoginResult GetLoginResult(string userName, string pwd)
        {
            using (var db = new PapsEntities())
            {
                var self = db.Menus.FirstOrDefault(i => (i.AccLoginName == userName && i.AccLoginPwd == pwd) || i.GuestUserName == userName && i.GuestPwd == pwd && i.IsDel != true);
                if (self == null) return null;
                var parent = db.Menus.FirstOrDefault(i => i.ID == self.ParentID);
                var result = new ClientLoginResult
                {
                    SelfId = self.ID.ToString(),
                    ParentId = self.ParentID.ToString(),
                    GrandParId = parent.ParentID.ToString()
                };
                return result;
            }
        }

        public static string Encrypt(string str)
        {
            string m_Need_Encode_String = str;
            if (m_Need_Encode_String == null)
            {
                throw new Exception("Error: \n String is empty！！");
            }
            var arrDESKey = UTF8Encoding.UTF8.GetBytes(Key);
            var arrDESIV = UTF8Encoding.UTF8.GetBytes(Key);
            DESCryptoServiceProvider objDES = new DESCryptoServiceProvider();
            MemoryStream objMemoryStream = new MemoryStream();
            CryptoStream objCryptoStream = new CryptoStream(objMemoryStream, objDES.CreateEncryptor(arrDESKey, arrDESIV), CryptoStreamMode.Write);
            StreamWriter objStreamWriter = new StreamWriter(objCryptoStream);
            objStreamWriter.Write(m_Need_Encode_String);
            objStreamWriter.Flush();
            objCryptoStream.FlushFinalBlock();
            objMemoryStream.Flush();

            return Convert.ToBase64String(objMemoryStream.GetBuffer(), 0, (int)objMemoryStream.Length);
        }
        public class Paras
        {
            public int code { get; set; }
            public bool status { get; set; }
            public string result { get; set; }
            public ClientLoginResult data { get; set; }
        }
        /// <summary>
        /// 获取客户端IP地址（无视代理）
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        public static string GetHostAddress()
        {
            string userHostAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(userHostAddress))
            {
                if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
                    userHostAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
            }

            if (string.IsNullOrEmpty(userHostAddress))
            {
                userHostAddress = HttpContext.Current.Request.UserHostAddress;
            }

            //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
            if (!string.IsNullOrEmpty(userHostAddress) && IsIP(userHostAddress))
            {
                return userHostAddress;
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
        [HttpGet, Route("DemoUser")]
        public IHttpActionResult DemoUser(int page, int limit, string startTime, string endTime, string region, string type)
        {
            var list = new List<user>()
            {
                new user(){city = "",classify = "",experience = "", id = 1, score = "12", sex = "1", sign = "", username = "张三", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 2, score = "20", sex = "1", sign = "", username = "李四", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 3, score = "30", sex = "2", sign = "", username = "王五", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 4, score = "22", sex = "1", sign = "", username = "赵六", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 5, score = "33", sex = "2", sign = "", username = "郑七", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 6, score = "44", sex = "1", sign = "", username = "包八", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 7, score = "55", sex = "2", sign = "", username = "孙子", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 8, score = "11", sex = "1", sign = "", username = "孔子", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 8, score = "66", sex = "2", sign = "", username = "老子", wealth = ""},
                new user(){city = "",classify = "",experience = "", id = 10, score = "1", sex = "1", sign = "", username = "梁尚", wealth = ""}
            };
            return Json(new { code = 0, msg = "1000", data = list });
        }
        public class user
        {
            public int id { get; set; }
            public string username { get; set; }
            public string sex { get; set; }
            public string city { get; set; }
            public string sign { get; set; }
            public string experience { get; set; }
            public string score { get; set; }
            public string classify { get; set; }
            public string wealth { get; set; }
        }
        #region 第二版接口，平台和app公用

        /// <summary>
        /// 获取在岗时长
        /// </summary>
        /// <param name="workProcedure">工序</param>
        /// <param name="position">位置</param>
        /// <param name="year">年份</param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOnDutyData")]
        public IHttpActionResult GetOnDutyData(string workProcedure, int year, int menuId)
        {
            var list = new List<OnlineSummary>();
            var onDutyList = DatabaseServices.GetOnDutySummary(workProcedure, year, menuId);

            if (!onDutyList.Any())
            {
                var start = int.Parse(year.ToString().Substring(4));
                for (int i = 1; i <= 6; i++)
                {
                    list.Add(new OnlineSummary
                    {
                        Month = i,
                        LeaderCount = 0,
                        StandardCount = 0,
                        SupervisorCount = 0
                    });
                }
                return Json(list);
            }
            for (int i = 0; i < onDutyList.Count; i++)
            {
                if (i == 0)
                {
                    list = JsonConvert.DeserializeObject<List<OnlineSummary>>(onDutyList[i]);
                }
                else
                {
                    var tempList = JsonConvert.DeserializeObject<List<OnlineSummary>>(onDutyList[i]);
                    tempList.ForEach(t =>
                    {
                        var first = list.FirstOrDefault(l => l.Month == t.Month);
                        if (first == null) return;
                        first.LeaderCount += t.LeaderCount;
                        first.StandardCount += t.StandardCount;
                        first.SupervisorCount += t.SupervisorCount;
                    });
                }

            }
            return Json(list);
        }

        /// <summary>
        /// 获取隧道内时长接口
        /// </summary>
        /// <param name="position"></param>
        /// <param name="year"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpGet, Route("GetOnlineData")]
        public IHttpActionResult GetOnlineData(int year, int menuId)
        {
            var list = new List<OnlineSummary>();
            var inTunnelList = DatabaseServices.GetInTunnelSummary(year, menuId);
            if (!inTunnelList.Any()) {
                var start = int.Parse(year.ToString().Substring(4));
                for (int i = 1; i <= 6; i++)
                {
                    list.Add(new OnlineSummary
                    {
                        Month = i,
                        LeaderCount = 0,
                        StandardCount = 0,
                        SupervisorCount = 0
                    });
                }
                return Json(list);
            }
            for (int i = 0; i < inTunnelList.Count; i++)
            {
                if (i == 0)
                {
                    list = JsonConvert.DeserializeObject<List<OnlineSummary>>(inTunnelList[i]);
                }
                else
                {
                    var tempList = JsonConvert.DeserializeObject<List<OnlineSummary>>(inTunnelList[i]);
                    tempList.ForEach(t =>
                    {
                        var first = list.FirstOrDefault(l => l.Month == t.Month);
                        if (first == null) return;
                        first.LeaderCount += t.LeaderCount;
                        first.StandardCount += t.StandardCount;
                        first.SupervisorCount += t.SupervisorCount;
                    });
                }

            }
            return Json(list);
        }

        /// <summary>
        /// 获取图片，人员列表，隧道内人数，掌子面人数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="workProcedure"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [HttpGet, Route("RealtimeWatch")]
        public IHttpActionResult RealtimeWatch(string type, string workProcedure, string position)
        {
            var model = DatabaseServices.GetRealtimeWatch(type, workProcedure, position);
            return Json(model);
        }

        [HttpGet, Route("GetTypes")]
        public IHttpActionResult GetTypes()
        {
            var list = InterfaceServices.GetTypes();
            return Json(list);
        }

        [HttpGet, Route("GetWorkStep")]
        public IHttpActionResult GetWorkStep()
        {
            var list = InterfaceServices.GetWorkStep();
            return Json(list);
        }
        [HttpGet, Route("GetLocalWorkStep")]
        public IHttpActionResult GetLocalWorkStep(int menuId)
        {
            var list = DatabaseServices.GetWorkStep(menuId);
            return Json(list);
        }
        /// <summary>
        /// 用于客户端向平台同步工序数据
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [HttpPost, Route("SyncWorkStep")]
        public bool SyncWorkStep(List<WorkStep> list)
        {
            var res = DatabaseServices.SyncWorkStep(list);
            return res;
        }
        [HttpPost, Route("SyncWorkStepLog")]
        public bool SyncWorkStepLog(List<WorkStepLog> list)
        {
            var res = DatabaseServices.SyncWorkStepLog(list);
            return res;
        }

        /// <summary>
        /// 同步客户端报表数据
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("SyncChartData")]
        public bool SyncChartData(QueryMode obj)
        {
            var db = new PapsEntities();
            var mode = db.ChartData.FirstOrDefault(c => c.DeviceCode == obj.DeviceCode && c.SearchData == obj.SearchData);
            if (mode == null)
            {
                db.ChartData.Add(new ChartData()
                {
                    ClientId = obj.ClientId,
                    Created = DateTime.Now,
                    ResultData = obj.ResultData,
                    SearchData = obj.SearchData,
                    Updated = DateTime.Now,
                    DeviceCode = obj.DeviceCode
                });
            }
            else
            {
                mode.ResultData = obj.ResultData;
                mode.Updated = DateTime.Now;
                mode.DeviceCode = obj.DeviceCode;
                db.ChartData.AddOrUpdate(mode);
            }

            return db.SaveChanges() > 0;
        }

        [HttpPost, Route("SyncOnDutyData")]
        public bool SyncOnDutyData(QueryMode obj)
        {
            var db = new PapsEntities();
            var mode = db.OnDutyData.FirstOrDefault(c => c.DeviceCode == obj.DeviceCode && c.SearchData == obj.SearchData && c.WorkStepName == obj.WorkStepName);
            if (mode == null)
            {
                db.OnDutyData.Add(new OnDutyData()
                {
                    ClientId = obj.ClientId,
                    Created = DateTime.Now,
                    ResultData = obj.ResultData,
                    WorkStepName = obj.WorkStepName,
                    SearchData = obj.SearchData,
                    Updated = DateTime.Now,
                    DeviceCode = obj.DeviceCode
                });
            }
            else
            {
                mode.ResultData = obj.ResultData;
                mode.Updated = DateTime.Now;
                db.OnDutyData.AddOrUpdate(mode);
            }

            return db.SaveChanges() > 0;
        }
        /// <summary>
        /// 
        /// </summary>
        public class QueryMode
        {
            public int ClientId { get; set; } //客户端id
            public int SearchData { get; set; } //20187这种格式
            public string ResultData { get; set; } //计算结果
            public string WorkStepName { get; set; } //工序名称
            public string DeviceCode { get; set; }
        }
        #endregion

        /// <summary>
        /// 删除工序
        /// </summary>
        /// <param name="clientWorkStepId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        [HttpGet, Route("DeleteWorkStep")]
        public bool DeleteWorkStep(int clientWorkStepId, string deviceCode)
        {
            var db = new PapsEntities();
            var mode = db.WorkStep.FirstOrDefault(w => w.DeviceCode == deviceCode && w.ClientWorkStepId == clientWorkStepId);
            if (mode == null) return true;
            var list = db.WorkStepLog.Where(l => l.DeviceCode == deviceCode && l.WorkStepId == clientWorkStepId).ToList();
            //删除工序对应的工序日志先
            if (list.Any())
            {
                list.ForEach(l => { db.WorkStepLog.Remove(l); });
            }
            //再删除工序
            db.WorkStep.Remove(mode);
            return db.SaveChanges() > 0;
        }

        [HttpGet, Route("GetDayRecord")]
        public IHttpActionResult GetDayRecord(string day, string cardNo)
        {
            return Json(InterfaceServices.GetDayRecord(day, cardNo));
        }

        /// <summary>
        /// 获取系统所有用户名
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("GetUserNames")]
        public List<string> GetUserNames()
        {
            var entity = new PapsEntities();
            var list = entity.Menus.Where(m => m.StructureType == 4).ToList();
            var result = list.Select(l => l.AccLoginName).ToList().Union(list.Select(l => l.GuestUserName)).ToList();
            return result.Where(r => !string.IsNullOrEmpty(r)).ToList();
        }

    }
}