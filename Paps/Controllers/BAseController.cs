﻿using DAL;
using Model;
using System.Web.Mvc;

namespace Paps.Controllers
{

    public class BaseController : ActionFilterAttribute
    {
        public bool IsCheck { get; set; }
        //Action方法执行之前执行此方法
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var returnUrl = filterContext.ActionDescriptor.ActionName;
            if (!IsCheck) return;
            //校验用户是否已经登录
            if (filterContext.HttpContext.Session["UserInfo"] == null)
            {
                //跳转到登陆页
                filterContext.HttpContext.Response.Redirect($"/Home/RedirectLogin?returnUrl={returnUrl}");
            }
            else
            {
                var user = (Users)filterContext.HttpContext.Session["UserInfo"];
                var main = DAL.DatabaseServices.GetUsers(user.UserId);
                if (main == null || main.IsActive == true)
                {
                    filterContext.HttpContext.Response.Redirect($"/Home/Login?IsDelete=1&returnUrl={returnUrl}");
                }
                var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                if (controller == "Admin" || controller == "Account")
                {
                    if (user.RoleId != 1 && !DatabaseServices.IsAdmin(user.UserId))//add by Ted on 20180613
                    {
                        filterContext.HttpContext.Response.Redirect($"/Home/Login?IsAdmin=1&returnUrl={returnUrl}");
                    }
                    else
                    {
                        if (main.RoleId != 1 && !DatabaseServices.IsAdmin(user.UserId)) //add by Ted on 20180613
                        {
                            filterContext.HttpContext.Response.Redirect($"/Home/Login?IsUser=1&returnUrl={returnUrl}");
                        }
                    }
                }
                else
                {
                    if ((user.RoleId == 1 || DatabaseServices.IsAdmin(user.UserId)) && returnUrl != "ChangePassword") //add by Ted on 20180613
                    {
                        filterContext.HttpContext.Response.Redirect($"/Systems/Admin/Index");
                    }
                    else
                    {
                        if ((main.RoleId == 1 || DatabaseServices.IsAdmin(user.UserId)) && returnUrl != "ChangePassword") //add by Ted on 20180613
                        {
                            filterContext.HttpContext.Response.Redirect($"/Home/Login?IsUser=1&returnUrl={returnUrl}");
                        }
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (!IsCheck) return;
            //            if (context.Exception != null)
            //            {
            //                if (context.Exception.Message.ToString() == "远程服务器返回错误: (500) 内部服务器错误。")
            //                {
            //                    context.HttpContext.Response.Write("<script>alert('无法连接到服务器')</script>");
            //                    context.HttpContext.Response.Clear();
            //                }
            //
            //            }
            var actionName = context.ActionDescriptor.ActionName;
            var controller = context.ActionDescriptor.ControllerDescriptor.ControllerName;
            if (controller == "Admin" || controller == "Account")
            {
                return;
            }
            //else if (actionName == "Login" || actionName == "DoLogin")
            //{
            //    return;
            //}

            //if (context.HttpContext.Session["Ip"] == null && context.HttpContext.Request.QueryString["IpAddress"] == null && !new []{"Index", "GetOnlineRate","OnlineRate","test", "GetTree" }.Contains(actionName))
            //{
            //    context.HttpContext.Response.Write("<script>alert('请先选择客户机')</script>");
            //}
            base.OnActionExecuted(context);
        }
    }
}