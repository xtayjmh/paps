﻿using DAL;
using Model;
using Paps.Common;
using Paps.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace Paps.Controllers
{
    public class UserController : Controller
    {
        Cache data = new Cache();

        // GET: Login
        public ActionResult Index()
        {
            var session = Request.QueryString["sessionid"] ?? "";
            var fromUrl = Request.UrlReferrer == null ? "" : Request.UrlReferrer.ToString();
            
            using (var db = new PapsEntities())
            {
                var tokenUrls = "";
                var loginUrls = "";
                var appID = "";
                var appSecret = "";
                if (fromUrl.ToLower().Contains("rails"))    // 这里加上大铁或者地方铁路来源url   //地方铁路
                {
                    var sysAccess = db.SysAccess.FirstOrDefault(s => s.ID == 2);
                    appID = sysAccess.AppID;
                    appSecret = sysAccess.AppSecret;
                    tokenUrls = ConfigurationManager.AppSettings["railTokenUrls"];
                    loginUrls = ConfigurationManager.AppSettings["railLoginUserUrls"];
                }
                else  //默认大铁
                {
                    var sysAccess = db.SysAccess.FirstOrDefault();
                    appID = sysAccess.AppID;
                    appSecret = sysAccess.AppSecret;
                    tokenUrls = ConfigurationManager.AppSettings["tokenUrls"];
                    loginUrls = ConfigurationManager.AppSettings["loginUserUrls"];
                }
                tokenUrls = tokenUrls + "?appid=" + appID + "&appsecret=" + appSecret;
                var token = LoginServices.GetAccessToken(tokenUrls);
                if (token != "")
                {
                    loginUrls = loginUrls + "?sessionid=" + session + "&token=" + token;

                    var response = LoginServices.GetServiceUser2(loginUrls);
                    //                    var response = LoginServices.TestAccount();
                    if (!string.IsNullOrEmpty(response?.account))
                    {
                        var res = ValidateUser(db, response.account);
                        if (res.status)
                        {

                            Session["UserInfo"] = res.user;
                            Session["UserName"] = res.user.UserName;
                            if (res.user.IsActive == true) TempData["result"] = "IsActive";
                        }
                        return Redirect(res.status ? "/Home/Index" : fromUrl);
                    }
                }
            }
            return View();
        }
        /// <summary>
        /// 登陆按钮
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// 登陆方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(Users model)
        {
            using (var db = new PapsEntities())
            {
                var password = EncryptHelper.CreateMd5Hash(model.Password); // updated by Ted on 20180522
                var data = ValidateUser(db, model.UserName, password);
                if (data.status)
                {
                    Session["user"] = data.user;
                    if (data.user.RoleId == 1)
                    {
                        return Redirect("/Admin/Home");
                    }
                    else
                    {
                        return Redirect("/Home");
                    }
                }
                else
                {
                    TempData["login"] = "false";
                    return View();
                }
            }
        }
        #region 私有方法
        /// <summary>
        /// 验证用户是否存在
        /// </summary>
        /// <param name="db"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        private ValidateUserInfo ValidateUser(PapsEntities db, string userName)
        {
            ValidateUserInfo result = new ValidateUserInfo();
            try
            {
                var user = db.Users.FirstOrDefault(p => p.UserName == userName);
                result.status = user != null;
                result.user = user;
            }
            catch (Exception exception)
            {
                result.status = false;
                result.user = null;
            }
            return result;
        }
        /// <summary>
        /// 验证用户是否存在
        /// </summary>
        /// <param name="db"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private ValidateUserInfo ValidateUser(PapsEntities db, string userName, string password)
        {
            ValidateUserInfo result = new ValidateUserInfo();
            try
            {
                var user = db.Users.Where(p => p.UserName == userName && p.Password == password).FirstOrDefault();
                result.status = user == null ? false : true;
                result.user = user;
            }
            catch (Exception ex)
            {
                result.status = false;
                result.user = null;
            }
            return result;
        }
        private ValidateUserInfo AddUser(PapsEntities db, string userName)
        {
            var password = EncryptHelper.CreateMd5Hash("Ht$t1qaz"); // updated by Ted on 20180522
            ValidateUserInfo result = new ValidateUserInfo();
            try
            {
                var entity = new Users();
                entity.UserName = userName;
                entity.Password = password;
                entity.RoleId = 2;
                entity.IsActive = false;
                entity.CreateDate = DateTime.Now;
                db.Users.Add(entity);
                db.SaveChanges();
                result.status = entity.UserId > 0 ? false : true;
                result.user = entity;
            }
            catch (Exception ex)
            {
                result.status = false;
                result.user = null;
            }
            return result;
        }
        #endregion
    }
}