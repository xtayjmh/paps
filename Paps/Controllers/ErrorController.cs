﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Paps.Controllers
{
    public class ErrorController : Controller
    {
        [BaseController(IsCheck = false)]
        // GET: Error
        public ActionResult Index()
        {
            if (Request.IsAjaxRequest()) Response.StatusCode = 500;
            return View();
        }
        [BaseController(IsCheck = false)]
        public ActionResult NotFound()
        {
            if (Request.IsAjaxRequest())
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
            return View();
        }
    }
}
