﻿$(document).ready(function () {
    $('.btnGetBsPerInfo').fancybox({
        width: 920,
        height: 660,
        padding: 10,
        closeClick: true,
        type: "iframe",
        mouseWheel: false,
        helpers: {
            overlay: {
                locked: false // if true, the content will be locked into overlay
            }
        }
    });
});
function alertModal(title, content) {
    $.confirm({
        title: title,
        content: content,
        buttons: {
            ok: {
                text: "确定",
                btnClass: 'btn-primary',
                keys: ['enter']
            }
        }
    });
}