﻿using Model;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Paps.Common
{
    public static class ExcelHelper
    {
        /// <summary>
        /// 生成汇总报表文件
        /// </summary>
        /// <param name="list"></param>
        /// <param name="path"></param>
        /// <param name="st"></param>
        /// <param name="et"></param>
        public static void GenerateSummaryReport(List<OnlineRateModel> list, string path, string st, string et)
        {
            const string name = "在线率汇总报表";
            var book = new XSSFWorkbook();
            var style = book.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            var font = book.CreateFont();
            font.FontHeightInPoints = 18;
            font.Boldweight = (short)FontBoldWeight.Bold;
            font.FontName = "宋体";
            style.SetFont(font);
            var sheet1 = book.CreateSheet(name);
            var row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue(name);
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, 5));
            sheet1.AddMergedRegion(new CellRangeAddress(1, 1, 0, 5));
            row1.Cells[0].CellStyle = style;
            var row2 = sheet1.CreateRow(1);
            row2.CreateCell(0).SetCellValue($"统计时间：{st}~{et}  制表时间：{DateTime.Now:yyyy-MM-dd}");
            var style2 = book.CreateCellStyle();
            var font2 = book.CreateFont();
            style2.Alignment = HorizontalAlignment.Center;
            font2.FontHeightInPoints = 12;
            font2.FontName = "宋体";
            font2.Boldweight = (short)FontBoldWeight.Bold;
            style2.SetFont(font2);
            row2.Cells[0].CellStyle = style2;

            var row3 = sheet1.CreateRow(2);
            row3.CreateCell(0).SetCellValue("序号");
            row3.CreateCell(1).SetCellValue("所属区域");
            row3.CreateCell(2).SetCellValue("工点数");
            row3.CreateCell(3).SetCellValue("在线（小时）");
            row3.CreateCell(4).SetCellValue("应在线（小时）");
            row3.CreateCell(5).SetCellValue("在线率");
            row3.Cells.ForEach(r => { r.CellStyle = style2; });

            var font3 = book.CreateFont();
            var style3 = book.CreateCellStyle();
            style3.Alignment = HorizontalAlignment.Left;
            font3.FontHeightInPoints = 11;
            font3.FontName = "宋体";
            style3.SetFont(font3);
            for (var i = 0; i < list.Count; i++)
            {
                var rowTemp = sheet1.CreateRow(i + 3);
                rowTemp.CreateCell(0).SetCellValue(i + 1);
                rowTemp.CreateCell(1).SetCellValue(list[i].RegionName);
                rowTemp.CreateCell(2).SetCellValue(list[i].PointName);
                rowTemp.CreateCell(3).SetCellValue(list[i].OnlineHours.ToString());
                rowTemp.CreateCell(4).SetCellValue(list[i].TotalHours.ToString());
                rowTemp.CreateCell(5).SetCellValue(list[i].OnlineRate + "%");
                rowTemp.Cells.ForEach(r => { r.CellStyle = style3;});
            }

            for (int columnNum = 0; columnNum <= 6; columnNum++)
            {
                int columnWidth = sheet1.GetColumnWidth(columnNum) / 256;//获取当前列宽度
                for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)//在这一列上循环行
                {
                    IRow currentRow = sheet1.GetRow(rowNum);
                    ICell currentCell = currentRow.GetCell(columnNum);
                    if (currentCell == null) continue;
                    int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;//获取当前单元格的内容宽度
                    if (columnWidth < length + 1)
                    {
                        columnWidth = length + 1;
                    }//若当前单元格内容宽度大于列宽，则调整列宽为当前单元格宽度，后面的+1是我人为的将宽度增加一个字符
                }
                sheet1.SetColumnWidth(columnNum, columnWidth * 256);
            }
            for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)
            {
                IRow currentRow = sheet1.GetRow(rowNum);
                ICell currentCell = currentRow.GetCell(27);
                if (currentCell == null) continue;
                int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;
                currentRow.HeightInPoints = 20 * (length / 60 + 1);
            }

            FileStream ms = new FileStream(path, FileMode.Create);
            book.Write(ms);
        }
        /// <summary>
        /// 生成详情报表的文件
        /// </summary>
        /// <param name="list"></param>
        /// <param name="path"></param>
        /// <param name="st"></param>
        /// <param name="et"></param>
        public static void GenerateDetailReport(List<OnlineRateModel> list, string path, string st, string et)
        {
            const string name = "在线率详情报表";
            var book = new XSSFWorkbook();
            var style = book.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;

            var font = book.CreateFont();
            font.FontHeightInPoints = 18;
            font.FontName = "宋体";
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.SetFont(font);
            var sheet1 = book.CreateSheet(name);

            var row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue(name);
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, 5));
            sheet1.AddMergedRegion(new CellRangeAddress(1, 1, 0, 5));
            row1.Cells[0].CellStyle = style;
            var row2 = sheet1.CreateRow(1);
            row2.CreateCell(0).SetCellValue($"统计时间：{st}~{et}  制表时间：{DateTime.Now:yyyy-MM-dd}");

            var style2 = book.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.Center;
            var font2 = book.CreateFont();
            font2.FontHeightInPoints = 12;
            font2.FontName = "宋体";
            font2.Boldweight = (short) FontBoldWeight.Bold;
            style2.SetFont(font2);
            var row3 = sheet1.CreateRow(2);
            row3.CreateCell(0).SetCellValue("序号");
            row3.CreateCell(1).SetCellValue("所属区域");
            row3.CreateCell(2).SetCellValue("工点名称");
            row3.CreateCell(3).SetCellValue("在线（小时）");
            row3.CreateCell(4).SetCellValue("应在线（小时）");
            row3.CreateCell(5).SetCellValue("在线率");
            row2.Cells[0].CellStyle = style2;
            foreach (var t in row3.Cells)
            {
                t.CellStyle = style2;
            }
            var style3 = book.CreateCellStyle();
            style3.Alignment = HorizontalAlignment.Left; //居中显示
            var font3 = book.CreateFont();
            font3.FontHeightInPoints = 11;
            font3.FontName = "宋体";
            style3.SetFont(font3);
            for (var i = 0; i < list.Count; i++)
            {
                var rowTemp = sheet1.CreateRow(i + 3);
                rowTemp.CreateCell(0).SetCellValue(i + 1);
                rowTemp.CreateCell(1).SetCellValue(list[i].RegionName);
                rowTemp.CreateCell(2).SetCellValue(list[i].PointName);
                rowTemp.CreateCell(3).SetCellValue(list[i].OnlineHours.ToString());
                rowTemp.CreateCell(4).SetCellValue(list[i].TotalHours.ToString());
                rowTemp.CreateCell(5).SetCellValue(list[i].OnlineRate + "%");
                foreach (var rowTempCell in rowTemp.Cells)
                {
                    rowTempCell.CellStyle = style3;
                }
            }

            for (int columnNum = 0; columnNum <= 6; columnNum++)
            {
                int columnWidth = sheet1.GetColumnWidth(columnNum) / 256;//获取当前列宽度
                for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)//在这一列上循环行
                {
                    IRow currentRow = sheet1.GetRow(rowNum);
                    ICell currentCell = currentRow.GetCell(columnNum);
                    if (currentCell == null) continue;
                    int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;//获取当前单元格的内容宽度
                    if (columnWidth < length + 1)
                    {
                        columnWidth = length + 1;
                    }//若当前单元格内容宽度大于列宽，则调整列宽为当前单元格宽度，后面的+1是我人为的将宽度增加一个字符
                }
                sheet1.SetColumnWidth(columnNum, columnWidth * 256);
            }
            for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)
            {
                IRow currentRow = sheet1.GetRow(rowNum);
                ICell currentCell = currentRow.GetCell(27);
                if (currentCell == null) continue;
                int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;
                currentRow.HeightInPoints = 20 * (length / 60 + 1);
            }

            FileStream ms = new FileStream(path, FileMode.Create);
            book.Write(ms);
        }

        /// <summary>
        /// 生成日考勤报表的文件
        /// </summary>
        /// <param name="list"></param>
        /// <param name="path"></param>
        /// <param name="st"></param>
        /// <param name="name"></param>
        public static void GenerateDailyReport(List<DayReport> list, string path, string st, string name)
        {
            var title = $"{name}工点{st}日考勤报表";
            var book = new XSSFWorkbook();
            var style = book.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;

            var font = book.CreateFont();
            font.FontHeightInPoints = 18;
            font.Boldweight = (short)FontBoldWeight.Bold;
            font.FontName = "宋体";
            style.SetFont(font);
            var sheet1 = book.CreateSheet(title);

            var row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue(title);
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, 6));
            row1.Cells[0].CellStyle = style;

            var style2 = book.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.Center;
            var font2 = book.CreateFont();
            font2.FontHeightInPoints = 12;
            font2.FontName = "宋体";
            font2.Boldweight = (short)FontBoldWeight.Bold;
            style2.SetFont(font2);

            var row2 = sheet1.CreateRow(1);
            row2.CreateCell(0).SetCellValue("序号");
            row2.CreateCell(1).SetCellValue("标志卡号");
            row2.CreateCell(2).SetCellValue("姓名");
            row2.CreateCell(3).SetCellValue("部门");
            row2.CreateCell(4).SetCellValue("职务");
            row2.CreateCell(5).SetCellValue("进洞时间");
            row2.CreateCell(6).SetCellValue("工作时间");
            foreach (var t in row2.Cells)
            {
                t.CellStyle = style2;
            }
            var style3 = book.CreateCellStyle();
            style3.Alignment = HorizontalAlignment.Left; //居中显示
            var font3 = book.CreateFont();
            font3.FontHeightInPoints = 11;
            font3.FontName = "宋体";
            style3.SetFont(font3);
            for (var i = 0; i < list.Count; i++)
            {
                var rowTemp = sheet1.CreateRow(i + 2);
                rowTemp.CreateCell(0).SetCellValue(i + 1);
                rowTemp.CreateCell(1).SetCellValue(list[i].BsPerInfo.IdCardNo);
                rowTemp.CreateCell(2).SetCellValue(list[i].BsPerInfo.Name);
                rowTemp.CreateCell(3).SetCellValue(list[i].BsPerInfo.Dept);
                rowTemp.CreateCell(4).SetCellValue(list[i].BsPerInfo.Title);
                rowTemp.CreateCell(5).SetCellValue(list[i].BsPerInfo.EnterTime);
                rowTemp.CreateCell(6).SetCellValue(list[i].WorkingTime.Replace(":", "小时") + "分钟");
                foreach (var c in rowTemp.Cells)
                {
                    c.CellStyle = style3;
                }
            }

            for (int columnNum = 0; columnNum <= 6; columnNum++)
            {
                int columnWidth = sheet1.GetColumnWidth(columnNum) / 256;//获取当前列宽度
                for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)//在这一列上循环行
                {
                    IRow currentRow = sheet1.GetRow(rowNum);
                    ICell currentCell = currentRow.GetCell(columnNum);
                    if (currentCell == null) continue;
                    int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;//获取当前单元格的内容宽度
                    if (columnWidth < length + 1)
                    {
                        columnWidth = length + 2;
                    }//若当前单元格内容宽度大于列宽，则调整列宽为当前单元格宽度，后面的+1是我人为的将宽度增加一个字符
                }
                sheet1.SetColumnWidth(columnNum, columnWidth * 256);
            }
            for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)
            {
                IRow currentRow = sheet1.GetRow(rowNum);
                ICell currentCell = currentRow.GetCell(27);
                if (currentCell == null) continue;
                int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;
                currentRow.HeightInPoints = 20 * (length / 60 + 2);
            }

            FileStream ms = new FileStream(path, FileMode.Create);
            book.Write(ms);
        }

        /// <summary>
        /// 生成月考勤报表的文件
        /// </summary>
        /// <param name="list"></param>
        /// <param name="path"></param>
        /// <param name="month"></param>
        /// <param name="name"></param>
        public static void GenerateMonthlyReport(List<MonthReport> list, string path, string month, string name)
        {
            var title = $"{name}工点{month}月考勤报表";
            var book = new XSSFWorkbook();
            var style = book.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;

            //第一行大标题的样式
            var font = book.CreateFont();
            font.FontHeightInPoints = 18;
            font.FontName = "宋体";
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.SetFont(font);
            var sheet1 = book.CreateSheet(title);

            ICellStyle styleMiddle = book.CreateCellStyle();
            styleMiddle.Alignment = HorizontalAlignment.Center;
            styleMiddle.VerticalAlignment = VerticalAlignment.Center;
            styleMiddle.WrapText = true; //wrap the text in the cell

            var row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue(title);
            row1.Cells[0].CellStyle = style;
            var row2 = sheet1.CreateRow(1);
            row2.CreateCell(0).SetCellValue("姓名");
            row2.Cells[0].CellStyle = styleMiddle;
            var days = list[0].Detail.Count;
            for (var i = 1; i <= days; i++)
            {
                row2.CreateCell(i).SetCellValue(i);
                row2.Cells[i].CellStyle = styleMiddle;
            }
            row2.CreateCell(days + 1).SetCellValue("进隧道天数");
            row2.CreateCell(days + 2).SetCellValue("未进隧道天数");

            //第二行（标题）的样式
            var style2 = book.CreateCellStyle();
            var font2 = book.CreateFont();
            font2.FontHeightInPoints = 12;
            font.FontName = "宋体";
            font2.Boldweight = (short)FontBoldWeight.Bold;
            style2.SetFont(font2);
            style2.Alignment = HorizontalAlignment.Center;
            style2.VerticalAlignment = VerticalAlignment.Center;

            //出勤的样式
            var styleGreen = book.CreateCellStyle();
            var fontGreen = book.CreateFont();
            fontGreen.FontHeightInPoints = 12;
            fontGreen.Boldweight = (short)FontBoldWeight.Bold;
            fontGreen.Color = HSSFColor.Green.Index;
            styleGreen.SetFont(fontGreen);
            styleGreen.Alignment = HorizontalAlignment.Center;
            styleGreen.VerticalAlignment = VerticalAlignment.Center;

            //未出勤的样式
            var styleRed = book.CreateCellStyle();
            var fontRed = book.CreateFont();
            fontRed.FontHeightInPoints = 12;
            fontRed.Boldweight = (short)FontBoldWeight.Bold;
            fontRed.Color = HSSFColor.Red.Index;
            styleRed.SetFont(fontRed);
            styleRed.Alignment = HorizontalAlignment.Center;
            styleRed.VerticalAlignment = VerticalAlignment.Center;

            for (var i = 0; i < list.Count; i++)
            {
                var rowTemp = sheet1.CreateRow(i + 2);
                rowTemp.CreateCell(0).SetCellValue(list[i].Name);
                var listCount = list[i].Detail.Count;
                for (int k = 0; k < listCount; k++)
                {
                    rowTemp.CreateCell(k + 1);
                }
                list[i].Detail.Reverse(); //接口返回的数据是30,29,28...3,2,1这样的，要反转一下
                var inTimes = 0;
                for (int j = 0; j < listCount; j++)
                {
                    var item = list[i].Detail[j];
                    var value = "×";
                    ICellStyle tempStyle = styleRed;
                    if (!string.IsNullOrEmpty(item) && item != "0:0") //如果有数据并且不是0
                    {
                        value = "√";
                        tempStyle = styleGreen;
                        inTimes++;
                    }
                    rowTemp.Cells[j + 1].SetCellValue(value);
                    rowTemp.Cells[j + 1].CellStyle = tempStyle;
                }
                var outTimes = list[i].Detail.Count - inTimes;
                rowTemp.CreateCell(listCount + 1).SetCellValue(inTimes); //进洞天数
                rowTemp.Cells[listCount + 1].CellStyle = styleGreen;
                rowTemp.CreateCell(listCount + 2).SetCellValue(listCount - inTimes); //未进洞天数
                rowTemp.Cells[listCount + 2].CellStyle = styleRed;
            }

            foreach (var t in row2.Cells)
            {
                t.CellStyle = style2;
            }

            for (int i = 1; i < row2.Cells.Count - 2; i++)
            {
                sheet1.SetColumnWidth(days + 1, 2 * 256);
            }

            //最后两列设置一下宽度
            sheet1.SetColumnWidth(days + 1, 15 * 256);
            sheet1.SetColumnWidth(days + 2, 15 * 256);

            for (int columnNum = 1; columnNum <= row2.Cells.Count - 3; columnNum++)
            {
                int columnWidth = sheet1.GetColumnWidth(columnNum) / 256;//获取当前列宽度
                for (int rowNum = 1; rowNum <= sheet1.LastRowNum; rowNum++)//在这一列上循环行
                {
                    IRow currentRow = sheet1.GetRow(rowNum);
                    ICell currentCell = currentRow.GetCell(columnNum);
                    if (currentCell == null) continue;
                    int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;//获取当前单元格的内容宽度
                    if (columnWidth < length + 1)
                    {
                        columnWidth = length + 1;
                    }//若当前单元格内容宽度大于列宽，则调整列宽为当前单元格宽度，后面的+1是我人为的将宽度增加一个字符
                }
                sheet1.SetColumnWidth(columnNum, 4 * 256);
            }
            for (int rowNum = 2; rowNum <= sheet1.LastRowNum; rowNum++)
            {
                IRow currentRow = sheet1.GetRow(rowNum);
                ICell currentCell = currentRow.GetCell(27);
                if (currentCell == null) continue;
                int length = Encoding.UTF8.GetBytes(currentCell.ToString()).Length;
                currentRow.HeightInPoints = 20 * (length / 60 + 1);
            }

            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, row2.Cells.Count - 1));
            FileStream ms = new FileStream(path, FileMode.Create);
            book.Write(ms);
        }
        public static void GenerateOneKeyReport(List<DailyDetail> dailyDetails, List<MonthDetail> monthlyDetails, string pointName, string year, string path)
        {
            var name = $"{year}年{pointName}年度概况报表"; //sheet1名字
            var name1 = $"{year}年{pointName}进出记录详情"; //sheet2名字

            //创建excel以及两个sheet
            var book = new XSSFWorkbook();
            var sheet1 = book.CreateSheet(name);
            var sheet2 = book.CreateSheet(name1);


            #region 年度概况

            //标题的样式
            var style = book.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center; //居中显示
            var font = book.CreateFont();
            font.FontHeightInPoints = 18;
            font.FontName = "宋体";
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.SetFont(font);

            //创建标题行
            var row1 = sheet1.CreateRow(0);
            row1.CreateCell(0).SetCellValue(name);
            sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, 16));
            row1.Cells[0].CellStyle = style;

            var row2 = sheet1.CreateRow(1); //第二行，表头

            row2.CreateCell(0).SetCellValue("序号");
            row2.CreateCell(1).SetCellValue("姓名");
            row2.CreateCell(2).SetCellValue("卡号");
            row2.CreateCell(3).SetCellValue("一月");
            row2.CreateCell(4).SetCellValue("二月");
            row2.CreateCell(5).SetCellValue("三月");
            row2.CreateCell(6).SetCellValue("四月");
            row2.CreateCell(7).SetCellValue("五月");
            row2.CreateCell(8).SetCellValue("六月");
            row2.CreateCell(9).SetCellValue("七月");
            row2.CreateCell(10).SetCellValue("八月");
            row2.CreateCell(11).SetCellValue("九月");
            row2.CreateCell(12).SetCellValue("十月");
            row2.CreateCell(13).SetCellValue("十一月");
            row2.CreateCell(14).SetCellValue("十二月");
            row2.CreateCell(15).SetCellValue("合计");

            //表头的样式
            var style2 = book.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.Center; //居中显示
            var font2 = book.CreateFont();
            font2.FontHeightInPoints = 12;
            font2.FontName = "宋体";
            font2.Boldweight = (short)FontBoldWeight.Bold;
            style2.SetFont(font2);

            foreach (var t in row2.Cells)
            {
                t.CellStyle = style2;
            }

            var style3 = book.CreateCellStyle();
            style3.Alignment = HorizontalAlignment.Left; //居中显示
            var font3 = book.CreateFont();
            font3.FontHeightInPoints = 11;
            font3.FontName = "宋体";
            style3.SetFont(font3);

            for (int i = 0; i < monthlyDetails.Count; i++)
            {
                var rowTemp = sheet1.CreateRow(i + 2);
                rowTemp.CreateCell(0).SetCellValue(i + 1);
                rowTemp.CreateCell(1).SetCellValue(monthlyDetails[i].Name);
                rowTemp.CreateCell(2).SetCellValue(monthlyDetails[i].CardNo);
                for (int j = 0; j < 12; j++)
                {
                    var times = 0;
                    var hours = 0;
                    var details = monthlyDetails[i].DailyDetails.Where(m => m.Month == j + 1).ToList();
                    var content = "--";
                    if (details.Any())
                    {
                        content = $"{details.Count}/{details.Sum(d => d.Duration)}";
                    }
                    rowTemp.CreateCell(j + 3).SetCellValue(content);
                }
                rowTemp.CreateCell(15).SetCellValue(monthlyDetails[i].DailyTimes + "/" + monthlyDetails[i].DailyHours);
                foreach (var c in rowTemp.Cells)
                {
                    c.CellStyle = style3;
                }
            }

            #endregion

            #region 进出记录详细信息

            var row0 = sheet2.CreateRow(0);
            row0.CreateCell(0).SetCellValue(name1);
            sheet2.AddMergedRegion(new CellRangeAddress(0, 0, 0, 16));
            row0.Cells[0].CellStyle = style;


            var months = new[] { 1, 3, 5, 7, 8, 10, 12 };
            var groupList = dailyDetails.GroupBy(d => d.CardNo).ToList();
            var maxRecords = groupList.Max(g => g.GroupBy(gg => gg.DayIndex).Max(gg => gg.Count())); //这段儿屌爆了，取到日进出记录最大的数量
            var rowIndex = 2;

            var row22 = sheet2.CreateRow(1);
            row22.CreateCell(0).SetCellValue("姓名");
            row22.CreateCell(1).SetCellValue("卡号");
            row22.CreateCell(2).SetCellValue("班组");
            row22.CreateCell(3).SetCellValue("月份");
            row22.CreateCell(4).SetCellValue("日期");
            row22.CreateCell(5).SetCellValue("日累计时长");
            row22.CreateCell(6).SetCellValue("进出记录");

            sheet2.AddMergedRegion(new CellRangeAddress(1, 1, 6, maxRecords + 5)); //合并进出记录

            for (int k = 0; k < groupList.Count(); k++)
            {
                for (int i = 1; i <= 12; i++)
                {
                    var a = 30;
                    if (i == 2) //2月28天
                    {
                        a = 28;
                    }
                    else if (months.Contains(i)) //31天
                    {
                        a = 31;
                    }
                    for (int j = 1; j <= a; j++)
                    {
                        var totalHours = 0.00d;
                        var st = new DateTime(int.Parse(year), i, j, 0, 0, 0);
                        var et = new DateTime(int.Parse(year), i, j, 23, 59, 59);
                        var records = groupList[k].Where(g => g.Month == i && g.StartTime >= st && g.StartTime <= et).ToList(); //取出来当天的进出记录
                        var tr = sheet2.CreateRow(rowIndex);
                        tr.CreateCell(0).SetCellValue(groupList[k].FirstOrDefault()?.Name); //姓名
                        tr.CreateCell(1).SetCellValue(groupList[k].FirstOrDefault()?.CardNo); //卡号
                        tr.CreateCell(2).SetCellValue(groupList[k].FirstOrDefault()?.DeptName); //班组
                        tr.CreateCell(3).SetCellValue(i + "月"); //月
                        tr.CreateCell(4).SetCellValue(j); //天

                        if (records.Any())
                        {
                            for (var index = 0; index < records.Count; index++)
                            {
                                var r = records[index];
                                tr.CreateCell(index + 6).SetCellValue($" {r.StartTime:HH:mm:ss}-{r.EndTime:HH:mm:ss} "); //进出记录
                            }

                            totalHours = Math.Round(records.Sum(r => r.EndTime.Subtract(r.StartTime).TotalHours), 2);
                        }
                        tr.CreateCell(5).SetCellValue(totalHours); //进出记录
                        foreach (var c in tr.Cells)
                        {
                            c.CellStyle = style3;
                        }
                        rowIndex++;
                    }

                }

            }

            for (int i = 0; i < row22.Cells.Count; i++)
            {
                row22.Cells[i].CellStyle = style2;
            }
            
            for (int i = 5; i < maxRecords; i++)
            {
                //row22.Cells[i].CellStyle = style2;
            }

            for (int i = 5; i < maxRecords + 5; i++)
            {
                sheet2.SetColumnWidth(i, 20 * 256);
            }
            sheet2.SetColumnWidth(maxRecords + 5, 20 * 256);

            #endregion
            FileStream ms = new FileStream(path, FileMode.Create);
            book.Write(ms);
        }
    }
}