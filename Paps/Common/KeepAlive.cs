﻿using System.Net;
using System.Web;
using FluentScheduler;

namespace Paps.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class KeepAlive : Registry
    {
        /// <summary>
        /// 
        /// </summary>
        public KeepAlive()
        {
            Schedule<DoKeep>().ToRunNow().AndEvery(10).Minutes();
        }
        /// <summary>
        /// 检查是否在线，根据最后一次心跳的时间来判断
        /// </summary>
        public class DoKeep : IJob
        {
            void IJob.Execute()
            {
                
                string url = "http://localhost";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                string tmp = res.StatusDescription;
            }
        }
    }
}