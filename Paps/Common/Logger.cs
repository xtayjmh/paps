﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Paps.Common
{
    #region Class : Task Logger

    public class TaskLogger
    {
        public string Path { get; set; }

        public TaskLogger(string fileName)
        {
            Path = Logger.GetPath(fileName);
        }

        public void Begin()
        {
            Logger.Append(Path, string.Concat("Begin: ", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt")));
        }

        public void End()
        {
            Logger.Append(Path, string.Concat("End: ", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt"), "\r\n\r\n"));
        }

        public void Total(int count)
        {
            Logger.Append(Path, string.Concat("Total: ", count));
        }

        public void OnFaild(string loanNumber)
        {
            Logger.Append(Path, string.Concat("Run fail at: ", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt"), ", for #", loanNumber));
        }

        public void OnFaild(string loanNumber, string errorMessage)
        {
            Logger.Append(Path, string.Concat("Run fail at: ", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt"), ", for #", loanNumber, ", Error Message:", errorMessage));
        }

        public void OnSuccess(string loanNumber, string successMessage)
        {
            Logger.Append(Path, string.Concat("Run success at: ", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt"), ", for #", loanNumber, ", Success Message:", successMessage));
        }
    }

    #endregion

    #region Class : Logger

    public static class Logger
    {
        /// <summary>
        /// yyyy.MM.dd hh:mm:ss tt
        /// </summary>
        /// <returns></returns>
        public static string Now
        {
            get { return DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss tt"); }
        }

        /// <summary>
        /// yyyy_MM_dd_HH_mm_ss
        /// </summary>
        /// <returns></returns>
        public static string Timestamp
        {
            get { return DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"); }
        }

        public static void SystemError(string info)
        {
            var fileName = string.Concat("sys_error_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }
        public static void SystemError0(string info, string baseDir)
        {
            var fileName = string.Concat("sys_error_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetStaticPath(fileName, baseDir), info);
        }

        public static void SendError(string info)
        {
            var fileName = string.Concat("send_error_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }

        public static void MailSent(string info)
        {
            var fileName = string.Concat("sent_mail_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }

        public static void MailSentDetails(string info)
        {
            var fileName = string.Concat("sent_mail_details_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }

        public static void LockMailSent(string info)
        {
            var fileName = string.Concat("locked_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }

        public static void LockExtendMailSent(string info)
        {
            var fileName = string.Concat("extended_", DateTime.Now.ToString("dd"), ".txt");

            Append(GetPath(fileName), info);
        }

        public static void PriceResult(string loanNumber, string xml)
        {
            var fileName = string.Format("{0}_{1}.xml", loanNumber, DateTime.Now.ToString("dd_HHmmssfff"));

            New(GetPath(fileName, "price_result"), xml);
        }

        public static void CensusResult(string loanNumber, string xml)
        {
            var fileName = string.Format("{0}_{1}.xml", loanNumber, DateTime.Now.ToString("dd_HHmmssfff"));

            New(GetPath(fileName, "census"), xml);
        }

        public static void Debug(string info)
        {
            const string fileName = "debug.txt";

            AddTo(GetPath(fileName), string.Concat("[", Now, "]\t", info));
        }

        #region Methods : Common

        public static string GetRoot()
        {
            var root = "/Weblogs";
            root = Path.Combine(root, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString("0#"));

            return root;
        }

        public static string GetPath(string fileName, string subDir = "")
        {
            var root = GetRoot();
            //var baseDir = HttpContext.Current.Server.MapPath("~");
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            
            var dir = string.Concat(baseDir, root.TrimEnd('\\'), subDir.Trim('\\'));
            var path = string.Concat(dir,"\\", fileName.TrimStart('\\'));
            path = path.Replace("/", "");
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            return path;
        }
        public static string GetStaticPath(string fileName, string baseDir, string subDir = "")
        {
            var root = GetRoot();
            var dir = string.Concat(baseDir, root.TrimEnd('\\'), subDir.Trim('\\'));
            var path = string.Concat(dir, "\\", fileName.TrimStart('\\'));
            path = path.Replace("/", "");
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            return path;
        }

        private static readonly object locker = new object();

        public static void New(string path, string info)
        {
            try
            {
                lock (locker)
                {
                    var dir = Path.GetDirectoryName(path);
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

                    using (var sw = new StreamWriter(path, false))
                    {
                        sw.Write(info);
                        sw.Flush();
                    }
                }
            }
            catch
            {
            }
        }

        public static void New(string path, byte[] info)
        {
            try
            {
                lock (locker)
                {
                    var dir = Path.GetDirectoryName(path);
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

                    using (var sw = new StreamWriter(path, false))
                    {
                        sw.BaseStream.Write(info, 0, info.Length);
                        sw.Flush();
                    }
                }
            }
            catch
            {
            }
        }

        public static void Append(string path, string info)
        {
            try
            {
                lock (locker)
                {
                    using (var sw = new StreamWriter(path, true)) //when the file is very larger, the read operation will take a lot of memory
                    {
                        sw.WriteLine(info);
                        sw.Flush();
                    }
                }
            }
            catch
            {
            }
        }

        public static void AddTo(string path, string info)
        {
            try
            {
                using (var sw = new StreamWriter(path, true))
                {
                    sw.WriteLine(info);
                    sw.Flush();
                }
            }
            catch
            {
            }
        }

        #endregion
    }

    #endregion
}