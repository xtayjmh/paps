﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Paps.Common
{
    public class DbHelper
    {
        private static readonly PapsEntities Db = new PapsEntities();
        //示例 var model = Common.GetSingleModel<t_User>(i => i.LoginName == "xtayjmh");
        public static T GetSingleModel<T>(Expression<Func<T, bool>> whereLambda) where T : class
        {
            return Db.Set<T>().Where(whereLambda).AsNoTracking().FirstOrDefault();
        }
    }
}