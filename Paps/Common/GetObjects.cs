﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using Model;

namespace Paps.Common
{
    public class GetObjects
    {
        public static Uri Uri { get; set; } = new Uri("http://106.112.136.99:81/SDService.asmx");

        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        public static List<DeptModel> GetDeptList()
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<GetDeptList xmlns=\"http://tempuri.org/\" />");
            List<DeptModel> list = new List<DeptModel>();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(GetResponse(soap));
            var obj = doc.GetElementsByTagName("DeptList");
            for (int i = 0; i < obj.Count; i++)
            {
                list.Add(new DeptModel { DeptName = obj[i].ChildNodes[1].InnerText, DeptNo = obj[i].ChildNodes[0].InnerText });
            }
            return list;
        }

        public static List<DayReport> GetDayReport(string date, string deptNo)
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            soap.Append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">");
            soap.Append("<soap:Header/>");
            soap.Append("<soap:Body>");
            soap.Append($"<tem:GetDayReport><tem:Date>{date}</tem:Date><tem:DeptNo>{deptNo}</tem:DeptNo ></tem:GetDayReport>");
            soap.Append("</soap:Body>");
            soap.Append("</soap:Envelope>");
            WebRequest webRequest = WebRequest.Create(Uri);
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            WebResponse webResponse = webRequest.GetResponse();
            using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                var doc = new XmlDocument();
                doc.LoadXml(myStreamReader.ReadToEnd());
                var obj = doc.GetElementsByTagName("DayReport");
                var list = new List<DayReport>();
                for (var i = 0; i < obj.Count; i++)
                {
                    list.Add(new DayReport
                    {
                        BsPerInfo = new BSPerInfo
                        {
                            Dept = obj[i].ChildNodes[3].InnerText,
                            EnterTime = obj[i].ChildNodes[5].InnerText,
                            IdCardNo = obj[i].ChildNodes[1].InnerText,
                            LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                            Name = obj[i].ChildNodes[2].InnerText,
                            Title = obj[i].ChildNodes[4].InnerText
                        },
                        EnterTime = obj[i].ChildNodes[5].InnerText
                    });
                }
                return list.OrderBy(i=>i.BsPerInfo.IdCardNo).ToList();
            }
        }
        private static string GetResponse(StringBuilder sb)
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            soap.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
            soap.Append("<soap:Body>");

            soap.Append(sb);

            soap.Append("</soap:Body>");
            soap.Append("</soap:Envelope>");
            WebRequest webRequest = WebRequest.Create(Uri);
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            WebResponse webResponse = webRequest.GetResponse();
            using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
            {
                return myStreamReader.ReadToEnd();
            }
        }
    }
}