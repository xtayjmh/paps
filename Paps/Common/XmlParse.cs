﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Paps.Common
{
    public class XmlParse
    {
        /// <summary>
        /// XML字符串转换成Model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T XmlStr2Class<T>(string str)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader sr = new StringReader(str);
            return (T)serializer.Deserialize(sr);
        }

        public static XmlDocument Class2XmlDocument<T>(T source)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, source);
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(sw.ToString());
            return xd;
        }
    }
}