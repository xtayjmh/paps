﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Paps.Common
{
    public class ExceptionHandler
    {
        public static void Handle(HttpRequest request, Exception error, string source = "Global.Application_Error")
        {
            var is404 = error is HttpException && (error as HttpException).GetHttpCode() == 404;
            Logger.SystemError(error.Source + error.Message);
            var response = HttpContext.Current.Response;
            if (error.Message== "远程服务器返回错误: (500) 内部服务器错误。")
            {
                response.Clear();
                response.Redirect("/Home/Index");
            }
        }
    }
}