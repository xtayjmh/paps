﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using DAL;
using FluentScheduler;
using Model;

namespace Paps
{
    public class SyncStructure : Registry
    {
        /// <summary>
        /// 每天凌晨2点执行
        /// </summary>
        public SyncStructure()
        {
            Schedule<DoSync>().ToRunEvery(1).Days().At(02, 00);
        }
        /// <summary>
        /// 立即执行
        /// </summary>
        /// <param name="runNow"></param>
        public SyncStructure(bool runNow)
        {
            Schedule<DoSync>().ToRunNow();
        }
    }
    public class DoSync: IJob
    {
        private static List<SysAccess> GetAccess()
        {
            using (var db = new PapsEntities())
            {
                return db.SysAccess.ToList();
            }
        }

        /// <summary>
        /// 获取所有需要同步的公司列表
        /// </summary>
        /// <returns></returns>
        private static IList<Departments> GetAsyncDepartments()
        {
            using (var db = new PapsEntities())
            {
                return db.Departments.Where(p => p.IsAsync == true).ToList();
            }
        }

        /// <summary>
        /// 根据公司ID，获取数据库ID
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        private static Departments GetMenuByDepartmentId(string departmentId)
        {
            using (var db = new PapsEntities())
            {
                var entity = db.Departments.FirstOrDefault(p => p.DepartmentID == departmentId);
                return entity;
            }
        }

        public static string CreateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hashBytes = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }

        // added by Ted on 20180517

        public static string Html2Text(string htmlStr)
        {
            if (String.IsNullOrEmpty(htmlStr) || htmlStr.IndexOf("京津冀") > -1 || htmlStr.IndexOf("<") > -1 || htmlStr.IndexOf(">") > -1 || htmlStr.ToLower().IndexOf("img") > -1)
            {
                return "";
            }
            string regEx_style = "<style>[^>]*?>[\\s\\S]*?<\\/style>";
            string regEx_script = "<script>[^>]*?>[\\s\\S]*?<\\/script>";
            string regEx_html = "<[^>]+>";
            htmlStr = Regex.Replace(htmlStr, regEx_style, "");
            htmlStr = Regex.Replace(htmlStr, regEx_script, "");
            htmlStr = Regex.Replace(htmlStr, regEx_html, "");
            return htmlStr.Trim();
        }

        public static void GetAllRoles(PapsEntities db, Departments currentMenu, ref List<int> RoleMenuList)
        {
            if (currentMenu != null)
            {
                var roles = db.DeptRoleRelation.FirstOrDefault(m => m.DeptId == currentMenu.ID);
                if (roles != null)
                {
                    var roleIdString = roles.RoleIds;
                    if (roleIdString.IndexOf(",") > -1)
                    {
                        var roleItems = roleIdString.Split(',');
                        foreach (var roleitem in roleItems)
                        {
                            int role = 0;
                            Int32.TryParse(roleitem, out role);
                            if (!RoleMenuList.Contains(role))
                            {
                                RoleMenuList.Add(role);
                            }
                        }
                    }
                    else
                    {
                        int role = 0;
                        Int32.TryParse(roleIdString, out role);
                        if (!RoleMenuList.Contains(role))
                        {
                            RoleMenuList.Add(role);
                        }
                    }
                }
            }
        }

        public static Departments GetTopParentMenu(PapsEntities db, Departments currentMenu)
        {
            if (currentMenu.ParentID == 0)
                return currentMenu;
            var parent = db.Departments.FirstOrDefault(m => m.ID == currentMenu.ParentID);
            if (parent != null)
            {
                if (parent.ParentID != 0)
                    return GetTopParentMenu(db, parent);
                else
                    return parent;
            }
            return currentMenu;
        }
        //TODO 这儿需要优化，频繁的访问数据库，效率太低了
        void IJob.Execute()
        {
            using (var db = new PapsEntities())
            {
                var departmentList = db.Departments.ToList();
                //获取基础信息
                var access = GetAccess();
                //                var appid = access.AppID;
                //                var appSecret = access.AppSecret;
                //需要同步的公司

                var async = GetAsyncDepartments();
                //菜单列表

                //循环同步大铁的数据
                foreach (var department in async)
                {
                    var getAllDepartmentsUrl = ConfigurationManager.AppSettings["getAllDepartments"].ToString();
                    var getAllDepartments = getAllDepartmentsUrl + department.DepartmentID;
                    //                    var sr = new StreamReader("D:\\departments.json");
                    //                    var content = "";
                    //                    content = sr.ReadToEnd();
                    //                    var dl = CommonJson.JsonDeserialize<List<DepartmentInfo>>(content);
                    var departList = DepartmentsServices.GetAllDepartments(getAllDepartments);

                    if (departList == null) continue; //added by Ted
                    //                    var departList = dl;
                    if (!departList.Any()) continue;
                    foreach (var depart in departList.Where(i => !string.IsNullOrEmpty(i.name)))
                    {
                        //同步组织架构 start
                        var m = departmentList.FirstOrDefault(i => i.DepartmentID == depart.parentid);
                        int parentId = 0;
                        if (m != null)  //如果找不到父级节点直接跳过
                        {
                            parentId = m.ID;
                        }
                        if (parentId != -1 && parentId != 0)
                        {
                            var departName = Html2Text(depart.name); // added by Ted on 20180517
                            if (string.IsNullOrEmpty(departName))  // 临时测试
                                continue;
                            var newDepartment = new Departments()
                            {
                                ParentID = parentId,
                                DepartmentID = depart.id,
                                Name = departName.Length > 50 ? departName.Substring(0, 49) : departName,
                                IsDel = false,
                                StructureType = m.StructureType + 1
                            };
                            if (!db.Departments.Any(p => p.DepartmentID == depart.id))
                            {
                                db.Departments.Add(newDepartment);
                                departmentList.Add(newDepartment);
                                db.SaveChanges();
                                var roles = db.DeptRoleRelation.FirstOrDefault(d => d.DeptId == parentId);
                                if (roles != null)
                                {
                                    db.DeptRoleRelation.Add(new DeptRoleRelation() { DeptId = newDepartment.ID, RoleIds = roles.RoleIds });
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                var existDepart = db.Departments.First(p => p.DepartmentID == depart.id);
                                if (existDepart.ParentID != parentId)
                                    existDepart.ParentID = parentId;
                                db.SaveChanges();
                            }
                        }
                    }

                    var sync = new DepartSync
                    {
                        DepartID = department.DepartmentID,
                        AsyncDate = DateTime.Now
                    };
                    db.DepartSync.Add(sync);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    var Message = String.Empty;
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Message += ve.ErrorMessage;
                        }
                    }
                }

                foreach (var depart in db.Departments.ToList())
                {
                    var topParent = GetTopParentMenu(db, depart);
                    if (!(bool)(topParent.IsAsync == null ? false : topParent.IsAsync))
                        continue;
                    //同步组织架构 end
                    //同步用户 star
                    var userList = new List<Users>();
                    var getUsersByDeptIdUrl = ConfigurationManager.AppSettings["getUsersByDeptId"];
                    var getUsersByDeptId = String.Format(getUsersByDeptIdUrl + depart.DepartmentID + "/{0}/{1}", access[0].AppID, access[0].AppSecret);
                    var serviceUserList = DepartmentsServices.GetUsersByDeptId(getUsersByDeptId);
                    if (serviceUserList == null) continue;
                    var RoleMenuList = new List<int>();
                    var password = CreateMd5Hash("Ht$t1qaz"); // updated by Ted on 20180522
                    foreach (var user in serviceUserList)
                    {
                        var users = new Users
                        {
                            UserName = user.account,
                            Password = password,
                            RoleId = 2,
                            IsActive = false,
                            CreateDate = DateTime.Now,
                            UserAccount = user.loginaccount,
                            FullName = user.name
                        };
                        if (db.Users.Any(p => p.UserName == user.account))
                        {
                            var currentUser = db.Users.First(u => u.UserName == user.account);
                            currentUser.FullName = user.name;
                            continue;
                        }
                        userList.Add(users);
                    }
                    if (userList.Count > 0) // added by Ted 
                    {
                        db.Users.AddRange(userList);
                        db.SaveChanges();//必须每添加一个就要保存一下数据库才能获取到对应的id，因为子节点需要这个id
                    }
                    if (userList.Count > 0) // added by Ted 
                    {
                        GetAllRoles(db, depart, ref RoleMenuList);
                        //GetAllParentMenu(db, depart, ref RoleMenuList);
                        RoleMenuList = RoleMenuList.Distinct().ToList();
                        foreach (var user in userList)
                        {
                            if (!db.DepartmentUsers.Any(r => r.DepartID == depart.ID && r.UserID == user.UserId))
                            {
                                db.DepartmentUsers.Add(new DepartmentUsers { UserID = user.UserId, DepartID = depart.ID, IsDel = false });
                            }
                            foreach (var roleid in RoleMenuList)
                            {
                                if (!db.RoleUser.Any(r => r.RoleID == roleid && r.UserID == user.UserId))
                                {
                                    db.RoleUser.Add(new RoleUser { UserID = user.UserId, RoleID = roleid, IsDel = false });
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                }

                //循环同步地方铁路的数据
                foreach (var department in async)
                {
                    var getAllDepartmentsUrl = ConfigurationManager.AppSettings["getDepts"].ToString();
                    var getAllDepartments = getAllDepartmentsUrl + department.DepartmentID;
                    //                    var sr = new StreamReader("D:\\departments.json");
                    //                    var content = "";
                    //                    content = sr.ReadToEnd();
                    //                    var dl = CommonJson.JsonDeserialize<List<DepartmentInfo>>(content);
                    var departList = DepartmentsServices.GetAllDepartments(getAllDepartments);
                    if (departList == null) continue; //added by Ted
                    //                    var departList = dl;
                    if (!departList.Any()) continue;  //updated by Ted
                    foreach (var depart in departList.Where(i => !String.IsNullOrEmpty(i.name)))
                    {
                        //同步组织架构 start
                        var m = departmentList.FirstOrDefault(i => i.DepartmentID == depart.parentid);
                        if (m == null) continue;//如果找不到父级节点直接跳过
                        int parentId = 0;
                        if (m != null)  //如果找不到父级节点直接跳过
                        {
                            parentId = m.ID;
                        }
                        if (parentId != -1 && parentId != 0)
                        {
                            var departName = Html2Text(depart.name); // added by Ted on 20180517
                            if (String.IsNullOrEmpty(departName))
                                continue;
                            var newDepartment = new Departments
                            {
                                ParentID = parentId,
                                DepartmentID = depart.id,
                                Name = departName.Length > 50 ? departName.Substring(0, 49) : departName,
                                IsDel = false,
                                StructureType = m.StructureType + 1
                            };

                            if (!db.Departments.Any(p => p.DepartmentID == depart.id))
                            {
                                db.Departments.Add(newDepartment);
                                departmentList.Add(newDepartment);
                                db.SaveChanges();
                                var roles = db.DeptRoleRelation.FirstOrDefault(d => d.DeptId == parentId);
                                if (roles != null)
                                {
                                    db.DeptRoleRelation.Add(new DeptRoleRelation() { DeptId = newDepartment.ID, RoleIds = roles.RoleIds });
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                var existDepart = db.Departments.First(p => p.DepartmentID == depart.id);
                                if (existDepart.ParentID != parentId)
                                    existDepart.ParentID = parentId;
                                db.SaveChanges();
                            }
                        }
                        //if (db.Menus.Any(p => p.DepartmentID == depart.id))
                        //{
                        //    continue;
                        //}

                        //db.Menus.Add(menu);
                        //menuList.Add(menu);
                    }

                    var sync = new DepartSync
                    {
                        DepartID = department.DepartmentID,
                        AsyncDate = DateTime.Now
                    };
                    db.DepartSync.Add(sync);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    var Message = String.Empty;
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Message += ve.ErrorMessage;
                        }
                    }
                    Console.WriteLine(Message);
                }

                foreach (var depart in db.Departments.ToList())
                {
                    var topParent = GetTopParentMenu(db, depart);
                    if (!(bool)(topParent.IsAsync == null ? false : topParent.IsAsync))
                        continue;
                    //同步组织架构 end
                    //同步用户 star
                    var userList = new List<Users>();  // moved from global by Ted
                    var getUsersByDeptIdUrl = ConfigurationManager.AppSettings["getUsers"];
                    var getUsersByDeptId = String.Format(getUsersByDeptIdUrl + depart.DepartmentID + "/{0}/{1}", access[1].AppID, access[1].AppSecret);
                    var serviceUserList = DepartmentsServices.GetUsersByDeptId(getUsersByDeptId);
                    if (serviceUserList == null) continue;
                    var RoleMenuList = new List<int>();
                    var password = CreateMd5Hash("Ht$t1qaz"); // updated by Ted on 20180522
                    foreach (var user in serviceUserList)
                    {
                        var users = new Users
                        {
                            UserName = user.account,
                            Password = password,
                            RoleId = 2,
                            IsActive = false,
                            CreateDate = DateTime.Now,
                            UserAccount = user.loginaccount,
                            FullName = user.name
                        };
                        if (db.Users.Any(p => p.UserName == user.account))
                        {
                            var currentUser = db.Users.First(u => u.UserName == user.account);
                            currentUser.FullName = user.name;
                            continue;
                        }
                        userList.Add(users);
                    }
                    if (userList.Count > 0) // added by Ted 
                    {
                        db.Users.AddRange(userList);
                        db.SaveChanges();//必须每添加一个就要保存一下数据库才能获取到对应的id，因为子节点需要这个id
                    }
                    if (userList.Count > 0) // added by Ted 
                    {
                        //GetAllChildMenu(db, depart, ref RoleMenuList);
                        GetAllRoles(db, depart, ref RoleMenuList);
                        //GetAllParentMenu(db, depart, ref RoleMenuList);
                        RoleMenuList = RoleMenuList.Distinct().ToList();
                        foreach (var user in userList)
                        {
                            if (!db.DepartmentUsers.Any(r => r.DepartID == depart.ID && r.UserID == user.UserId))
                            {
                                db.DepartmentUsers.Add(new DepartmentUsers { UserID = user.UserId, DepartID = depart.ID, IsDel = false });
                            }
                            foreach (var roleid in RoleMenuList)
                            {
                                if (!db.RoleUser.Any(r => r.RoleID == roleid && r.UserID == user.UserId))
                                {
                                    db.RoleUser.Add(new RoleUser { UserID = user.UserId, RoleID = roleid, IsDel = false });
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                }
            }
        }
    }
    
}