﻿using FluentScheduler;
using Model;
using Paps.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Paps
{
    /// <summary>
    /// 
    /// </summary>
    public class CheckOnLine : Registry
    {
        /// <summary>
        /// 每5分钟执行一次
        /// </summary>
        public CheckOnLine()
        {
            Schedule<DoCheck>().ToRunNow().AndEvery(5).Minutes();
            Schedule<KeepSocketConnect>().ToRunNow().AndEvery(30).Seconds();
        }
    }
    /// <summary>
    /// 检查是否在线，根据最后一次心跳的时间来判断
    /// </summary>
    public class DoCheck : IJob
    {
        void IJob.Execute()
        {
            PapsEntities entity = new PapsEntities();
            var onlineClients = entity.Menus.Where(m => m.IsActive).ToList();
            var devices = entity.Device.ToList();

            onlineClients.ForEach(o =>
            {
                var device = devices.FirstOrDefault(d => d.MenuId == o.ID);
                if (device == null)
                {
                    o.IsActive = false;
                }
                else
                {

                    var onlineRecord = entity.OnlineRate.OrderByDescending(r => r.TableId).FirstOrDefault(r => r.DeviceCode == device.DeviceCode);
                    if (onlineRecord == null || onlineRecord.OfflineTime != null || onlineRecord.OfflineTime < DateTime.Now.AddMinutes(-10))
                    {
                        o.IsActive = false;
                    }
                }
            });

            List<string> onlineDevices = new List<string>();
            var groupList = entity.OnlineRate.GroupBy(o => o.DeviceCode).ToList();
            groupList.ForEach(g =>
            {
                var lastRecord = g.ToList().OrderByDescending(i => i.TableId).FirstOrDefault();
                if (lastRecord.OfflineTime == null) onlineDevices.Add(g.Key);
            });
            var menuIds = entity.Device.Where(d => onlineDevices.Contains(d.DeviceCode)).Select(d => d.MenuId).Distinct().ToList();
            menuIds.ForEach(i =>
            {
                var menu = entity.Menus.FirstOrDefault(m => m.ID == i);
                if (menu != null) menu.IsActive = true;
            });
            entity.SaveChanges();
        }
    }
    /// <summary>
    /// 每隔半分钟发送一个消息，保持Socket是连接状态
    /// </summary>
    public class KeepSocketConnect : IJob
    {
        void IJob.Execute()
        {
            var obj = new SocketMsg() { SignId = "PlateForm", QueryModel = new QueryModel() };
            SocketInstance.SendMsgToClient(obj, false);
        }
    }
}