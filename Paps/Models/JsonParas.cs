﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Paps.Models
{
    public class JsonParas
    {
        public int code { get; set; }
        public bool status { get; set; }
        public string msg { get; set; }
    }
    public class JsonParas<T>
    {
        public int code { get; set; }
        public bool status { get; set; }
        public string msg { get; set; }
        public List<T> data { get; set; }
    }
}