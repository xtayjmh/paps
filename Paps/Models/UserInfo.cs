﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Paps.Models
{
    public class UserInfo
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Date { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
    }
}