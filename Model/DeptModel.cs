﻿using System;

namespace Model
{
    /// <summary>
    /// 部门信息
    /// </summary>
    [Serializable]
    public class DeptModel
    {
        /// <summary>
        /// 部门编号
        /// </summary>
        public string DeptNo { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DeptName { get; set; }
    }
}
