﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    [Serializable]
    public class MonthReport
    {
        public string Name { get; set; }//姓名
        public int Absent{ get; set; }//旷工天数
        public int LateOrEarly { get; set; }//迟到早退天数
        public int Normal { get; set; }//正常天数
        public int OverTime { get; set; }//加班天数
        public List<string> Detail{ get; set; }

        public int InCount
        {
            get { return Detail.Count(d => !string.IsNullOrEmpty(d) && d != "0:0"); }
        }

        public int OutCount => Detail.Count() - InCount;
    }
}
