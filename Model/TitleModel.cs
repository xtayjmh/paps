﻿namespace Model
{
    /// <summary>
    /// 职务信息
    /// </summary>
    public class TitleModel
    {
        public string TitleNo { get; set; }//职务编号
        public string TitleName { get; set; }//职务名称
    }
}
