﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 监理和领导班子二衬/仰拱工序在岗时长统计
    /// </summary>
    [Serializable]
    public class OnlineSummary
    {
        public double SupervisorCount { get; set; } //监理人员
        public double LeaderCount { get; set; } //领导班子
        public int Month { get; set; } //月份
        public double StandardCount { get; set; } //标准时长
    }
}
