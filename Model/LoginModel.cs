﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class LoginModel
    {
        public class AccessToken
        {
            public string token { get; set; }
            public double expires_in { get; set; }
        }
        public class ClientLoginResult
        {
            public string SelfId { get; set; }
            public string ParentId { get; set; }
            public string GrandParId { get; set; }

        }
    }
}
