﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Model
{
    public class PersonModel
    {
        public string Name { get; set; } //定位对象
        public string CardNo { get; set; } //卡号
        public string Type { get; set; } //类别
        public string Department { get; set; } //部门
        public string Title { get; set; } //职务
        public string Position { get; set; } //所在区域
    }

    public class RealtimeWatchModel
    {
        public string Image { get; set; } //图片二进制
        public int TunnelNumber { get; set; } //隧道内人数
        public int TunnelFaceNumber { get; set; } //掌子面人数
        public List<PersonModel> Persons { get; set; } //人员列表
    }
}
