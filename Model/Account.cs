﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Account
    {
        public string userid { get; set; }
        public string account { get; set; }
        public string errcode { get; set; }
        public string errmsg { get; set; }
    }
}
