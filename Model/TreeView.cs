﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TreeView
    {
        public string text { get; set; }
        public int id { get; set; }
        public int ParentId { get; set; }
        public List<TreeView> nodes { get; set; }
    }

    public class TreeData
    {
        public string text { get; set; }
        public string href { get; set; }
        public string tags { get; set; }
        public int id { get; set; }
        public List<TreeData> nodes { get; set; }
    }
}
