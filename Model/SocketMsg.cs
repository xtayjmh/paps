﻿namespace Model
{
    public class PlateSocketMsg : SocketMsg
    {
        public bool CanConnect { get; set; } = false; //默认false表示客户端连接找不到，无法访问，页面就不再继续请求了
    }
    /// <summary>
    /// Socket通信的对象
    /// </summary>
    public class SocketMsg
    {
        public bool IsWebFrom { get; set; } = false;
        /// <summary>
        /// 平台页面连接Id，这个平台发的消息里会带，直接返回就可以了
        /// </summary>
        public string SignId { get; set; }
        /// <summary>
        /// 客户端和平台当前Socket连接的Id
        /// </summary>
        public string SocketId { get; set; }
        /// <summary>
        /// 客户端Id，对应客户端在平台Menus表中的主键Id
        /// </summary>
        public string SelfId { get; set; }
        /// <summary>
        /// 消息体
        /// </summary>
        public QueryModel QueryModel { get; set; }
        /// <summary>
        /// 返回结果
        /// </summary>
        public string Result { get; set; }
        public byte[] ByteResult { get; set; }

        public bool IsSocketResponse { get; set; } = false;
        public string DeviceCode { get; set; }
    }
    public enum QueryModelMethodEnum
    {
        Welcome = 1,
        //部门列表
        GetDeptList = 2,
        GetTitleList = 3,
        GetBaseStationList = 4,
        GetMonthReport = 5,
        GetDayReport = 6,
        GetSysAlarmCount = 7,
        GetSysAlarmData = 8,
        GetOverPerAlarmCount = 9,
        GetOverPerAlarmData = 10,
        GetYearData = 11,
        GetMonthData = 12,
        GetDayReportByCode = 13,
        GetExcelPath = 14,
        GetConfigData = 15,
        GetLocationData = 16,
        GetTypes = 17,
        GetBackGroundImage = 18
    }
    public class QueryModel
    {
        public QueryModelMethodEnum MethodName { get; set; }
        public string DeptNo { get; set; }
        public string TitleNo { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Date { get; set; }
        public string CardNo { get; set; }
        public string PerName { get; set; }
        public int BsType { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public string BaseStationNo { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string FileName { get; set; }
        public string FullFileName { get; set; }
        public byte[] ByteResult { get; set; }
    }
}
