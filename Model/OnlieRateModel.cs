﻿using System;
using System.Collections.Generic;

namespace Model
{
    [Serializable]
    public class OnlineRateModel
    {
        public string RegionName { get; set; }//所属区域名称
        public string PointName { get; set; }//监控点名称
        public decimal OnlineHours { get; set; }//在线时间
        public decimal OnlineRate { get; set; }//在线率
        public decimal TotalHours { get; set; }//应在限时间（小时）
        public string Score { get; set; }
        public int? UserId { get; set; }
        public int? RegionId { get; set; }
        public string OnlineRateStr => OnlineRate + "%";
        public int Pm { get; set; } //排名
    }
}
