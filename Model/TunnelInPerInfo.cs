﻿namespace Model
{
    /// <summary>
    /// 隧道内人员信息
    /// </summary>
    public class TunnelInPerInfo
    {
        public int QuotaNum { get; set; }//定员人数
        public int ActualNum { get; set; }//实际人数
        public int OverPerNum { get; set; }//超员人数
        public decimal QuotaTime { get; set; }//定时（小时）
        public string OverTimeNum { get; set; }//超时人数
    }
}
