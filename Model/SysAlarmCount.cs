﻿namespace Model
{
    /// <summary>
    /// 系统报警总数（查询类）
    /// 这个类是查询条件类，返回总条数
    /// </summary>
    public class SysAlarmQueryModel
    {
        public string StartTime { get; set; }//查询开始时间
        public string EndTime { get; set; }//查询结束时间
        public string PerName { get; set; } = "";//人员姓名
        public string DeptNo { get; set; } = "";//部门编号
        public string TitleNo { get; set; } = "";//职务编号

        public string BaseStationNo { get; set; } = "";//基站编号，空时为查询所有
        //报警详情查询用到的条件，报警总数不需要
        public int PageIndex { get; set; } = 0;//索引页
        public int PageSize { get; set; } = 15; //一页显示的行数
    }
    /// <summary>
    /// 报警详情
    /// </summary>
    public class SysAlarmData
    {
        public int LineNo { get; set; }//行号
        public string AlarmTime { get; set; }//报警时间 xxxx-xx-xx xx:xx:xx
        public string IdCardNo { get; set; }//标志卡号
        public string PerName { get; set; }//人员姓名
        public string Dept { get; set; }//部门名称
        public string Title { get; set; }//职务名称
        public string BSName { get; set; }//基站姓名
    }
}
