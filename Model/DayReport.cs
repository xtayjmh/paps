﻿using System;

namespace Model
{
    /// <summary>
    /// 日报考勤
    /// 按标志卡号升序排列
    /// </summary>
    [Serializable]
    public class DayReport
    {
        public BSPerInfo BsPerInfo { get; set; }

        public string EnterTime { get; set; } //进入时间xxxx-xx-xx xx:xx:xx

        public string OutTime { get; set; }//出去时间
        public string WorkingTime { get; set; }//工作时间
    }

    public class NewDayReport
    {
        public string LineNo { get; set; }
        public string IdCardNo { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Dept { get; set; }
        public string EnterTime { get; set; } //进入时间xxxx-xx-xx xx:xx:xx

        public string OutTime { get; set; }//出去时间
        public string WorkingTime { get; set; }//工作时间
    }
}
