﻿using System;

namespace Model
{
    [Serializable]
    public class BSPerInfo
    {
        public int LineNo { get; set; }//行号。从1开始
        public string IdCardNo { get; set; }//标志卡号
        public string Name { get; set; }//姓名
        public string Dept { get; set; }//部门
        public string Title { get; set; }//职务
        public string EnterTime { get; set; }//进入时间（格式为：年-月-日 时：分：秒 即 xxxx-xx-xx xx:xx:xx)
        public string SideName { get; set; }//如果是双洞或者斜井的话，这个字段有值，对应这个人在左、右掌子面
        public string Catelog { get; set; }
        public string SiteName { get; set; }
        public int InsideFlag { get; set; }
        public int SiteType { get; set; }
    }
}
