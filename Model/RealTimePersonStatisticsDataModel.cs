﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 实时人员统计数据
    /// </summary>
    public class RealTimePersonStatisticsDataModel
    {
        public int Total { get; set; }//人员总数。系统内全部人员的数量
        public int TunnelOut { get; set; }//隧道外人数。未进入隧道的人员数量
        public int TunnelIn { get; set; }//隧道内人数。进入隧道的人员数量
        public int Manager { get; set; }//管理人员
        public int KQBS { get; set; }//考勤基站附近的人数
        public int ZZMBS{ get; set; }//掌子面定位基站人数

        public int OtherArea { get; set; }//其他区域人数，等于隧道内人数减去掌子面定位基站的人数

        public int RestrictedArea { get; set; }//限制区域人数
        public string TunnelType { get; set; }//单洞、双洞、斜井
        public int Left { get; set; }//左掌子面人数
        public int Right { get; set; }//右掌子面人数
    }
}
