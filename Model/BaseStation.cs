﻿namespace Model
{
    /// <summary>
    /// 基站信息
    /// </summary>
    public class BaseStation
    {
        public string BaseStationNo { get; set; }//基站编号
        public string BaseStationName { get; set; }//基站名称
    }
}
