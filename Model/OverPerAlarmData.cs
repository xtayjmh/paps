﻿namespace Model
{
    /// <summary>
    /// 超员报警详细信息
    /// </summary>
    public class OverPerAlarmData
    {
        public int LineNo { get; set; }//行号
        public string StartTime { get; set; }//开始时间 xxxx-xx-xx xx:xx:xx
        public string EndTime { get; set; }//截止时间
        public int QuotaNum { get; set; }//定员人数
        public int ActualNum { get; set; }//实际人数
        public int OverPerNum { get; set; }//超出人数
        public int KeepTime { get; set; }//持续时间（单位：小时）
    }
}
