﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    [Serializable]

    public class YearDetail
    {
        public string Name { get; set; }
        public string CardNo { get; set; }
        public List<MonthDetail> MonthDetails { get; set; }
        public int Times => MonthDetails.Sum(i => i.DailyTimes);
        public double Hours => Math.Round(MonthDetails.Sum(i => i.DailyHours), 2);
    }
    [Serializable]

    public class MonthDetail
    {
        public string Name { get; set; }
        public string CardNo { get; set; }
        public List<DailyDetail> DailyDetails { get; set; }
        public int DailyTimes => DailyDetails.Count; //计算当月一共有多少条进出记录
        public double DailyHours => Math.Round(DailyDetails.Sum(i => i.Duration), 2); //求和每天进出的持续时间，小时取整
        public List<DayView> DayViews { get; set; }

    }

    [Serializable]
    public class DailyDetail
    {
        public string Name { get; set; }
        public string CardNo { get; set; }
        public DateTime StartTime { get; set; }//进洞时间
        public DateTime EndTime { get; set; }//出洞时间
        public double Duration => Math.Round(EndTime.Subtract(StartTime).TotalHours, 2); //持续时间
        public int Month { get; set; }
        public int DayIndex { get; set; } //第几天
        public string DeptName { get; set; }
    }
    [Serializable]
    public class DayView
    {
        public int Day { get; set; }
        public int Times { get; set; }
        public double Hours { get; set; }
    }
}
