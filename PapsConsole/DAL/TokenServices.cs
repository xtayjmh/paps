﻿using PapsConsole.Common;
using PapsConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PapsConsole.DAL
{
    public class TokenServices
    {
        //token接口
        private const string tokenUrls = "http://10.30.1.9:9880/baseservicetest/rest/tokenservice/gettoken.json?appid={0}&appsecret={1}";
        /// <summary>
        /// 获取token并缓存7200秒
        /// </summary>
        public static AccessToken GetAccessToken(string urls)
        {
            try
            {
                return HttpResponseInfo.RequestGetService<AccessToken>(HttpContext.Current, urls);
            }
            catch
            {
                return null;
            }
        }
    }
}
