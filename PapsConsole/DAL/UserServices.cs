﻿using PapsConsole.Common;
using PapsConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PapsConsole.DAL
{
    public class UserServices
    {
        //UserInfo接口
        //private const string tokenUrls = "http://api.r93535.com/apiman-gateway/base/resttokenservicegettoken.json/1.0?apikey=16a27f08-2c99-43e9-9ca0-1709f3567702&appid=d48f71f376&appsecret=7eda27149a9d48f71f376c098080c793";
        public static IList<UserInfo> GetUserInfo(string strUserInfoUrls)
        {
            try
            {
                return HttpResponseInfo.RequestGetService<IList<UserInfo>>(HttpContext.Current, strUserInfoUrls);
            }
            catch
            {
                return null;
            }
        }
    }
}
