﻿using PapsConsole.Common;
using PapsConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PapsConsole.DAL
{
    public class UsersService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUrls"></param>
        /// <returns></returns>
        public static List<DepartmentInfo> getDepartmentByUserId(string strUrls)
        {
            try
            {
                return HttpResponseInfo.RequestGetService<List<DepartmentInfo>>(HttpContext.Current, strUrls);
            }
            catch
            {
                return null;
            }
        }
    }
}
