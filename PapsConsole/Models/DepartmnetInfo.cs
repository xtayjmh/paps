﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapsConsole.Models
{
    public class DepartmentInfo
    {
        public string id { get; set; }
        public string name { get; set; }
        public string shortname { get; set; }
        public string parentid { get; set; }
        public string priority { get; set; }
        public string useFlag { get; set; }
        public string grade { get; set; }
        public string categoryId { get; set; }
        public string projectInfoId { get; set; }
        public string projectSectionId { get; set; }
        public string projectAreaId { get; set; }
        public string createDate { get; set; }
        public string updateDate { get; set; }
        public string deleteDate { get; set; }
        public string depLevel { get; set; }
    }
}
