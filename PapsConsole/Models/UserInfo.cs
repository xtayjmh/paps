﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapsConsole.Models
{
    public class UserInfo
    {
        public string id { get; set; }
        public string account { get; set; }
        public string name { get; set; }
        public string sex { get; set; }
        public string priority { get; set; }
        public string departmentId { get; set; }
        public string proName { get; set; }
        public string useFlag { get; set; }
        public string createDate { get; set; }
        public string modifyDate { get; set; }
        public string deleteDate { get; set; }
        public string loginaccount { get; set; }
        public string userlevel { get; set; }
        public string virtualaccount { get; set; }
        public string usertype { get; set; }
        public string mobile { get; set; }

    }
}
