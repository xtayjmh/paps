﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PapsConsole.Models
{
    public class AccessToken
    {
        public string token { get; set; }
        public double expires_in { get; set; }
    }
}
