﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PapsConsole.Common
{
    public class HttpResponseInfo
    {
        /// <summary>
        /// Get请求接口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="strUrl"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static A RequestGetService<A>(HttpContext context, string strUrl)
        {
            //声明调用webapi的方法主体
            HttpClient client = new HttpClient();
            //声明调用api的参数类型
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //同步调用webapi
            var response = client.GetAsync(strUrl).Result;
            //接收api返回的消息状态
            if (!response.IsSuccessStatusCode)
            {
                context.Response.Write(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase, response.RequestMessage));
                return default(A);
            }
            else
            {
                //接收webapi返回的数据
                var strResult = response.Content.ReadAsStringAsync().Result;
                //把接收的数据解密，并且序列化成实体类
                return CommonJson.JsonDeserialize<A>(strResult);
            }
        }
    }
}
