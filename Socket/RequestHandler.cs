﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Newtonsoft.Json;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace Socket
{
    /// <summary>
    /// 客户端和服务器建立连接后，把Session和ClientId一起发过来，这样服务器就可以创建他们俩直接的关系了。
    /// </summary>
    public class HandleClient : CommandBase<TelnetSession, StringRequestInfo>
    {
        /// <inheritdoc />
        public override void ExecuteCommand(TelnetSession session, StringRequestInfo requestInfo)
        {
            var param = requestInfo.Parameters;
            var obj = JsonConvert.DeserializeObject<SocketMsg>(param[0]);
            obj.IsSocketResponse = true;
            obj.QueryModel.MethodName = QueryModelMethodEnum.Welcome;
            session.CustomId = obj.SelfId; //客户端的唯一标识符，现在用的是Menus表中的主键Id,2019-1-10
            session.DeviceCode = obj.DeviceCode;
            var cmdStr = JsonConvert.SerializeObject(obj,
                             new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii }) + "##\r\n";
            var bytes = Encoding.UTF8.GetBytes(cmdStr);
            session.Send(bytes, 0, bytes.Length);

            void Action()
            {
                using (var db = new PapsEntities())
                {
                    var device = db.Device.FirstOrDefault(d => d.DeviceCode == obj.DeviceCode);
                    if (device?.MenuId == null) return;
                    var menu = db.Menus.FirstOrDefault(m => m.ID == device.MenuId);
                    if (menu == null) return;
                    menu.IsActive = true;
                    db.SaveChanges();
                }
            }
            Task.Run(Action);
        }
    }
    /// <summary>
    /// 客户端上线方法
    /// </summary>
    public class OnlineFun : CommandBase<TelnetSession, StringRequestInfo>
    {
        public override void ExecuteCommand(TelnetSession session, StringRequestInfo requestInfo)
        {
            void Action()
            {
                var deviceCode = session.DeviceCode;
                using (var db = new PapsEntities())
                {
                    var device = db.Device.AsNoTracking().FirstOrDefault(d => d.DeviceCode == deviceCode);
                    if (device == null) //首次登录，平台还没有添加对应的设备
                    {
                        db.OnlineRate.Add(new OnlineRate
                        {
                            OnlineTime = DateTime.Now,
                            DeviceCode = deviceCode,
                        });
                    }
                    else
                    {
                        var menu = db.Menus.FirstOrDefault(m => m.ID == device.MenuId);
                        var onlineRecord = db.OnlineRate.AsNoTracking().OrderByDescending(r => r.TableId)
                            .FirstOrDefault(r => r.DeviceCode == deviceCode && r.OfflineTime == null);
                        if (onlineRecord != null) return; //如果有在线率数据，并且没有离线时间，就先不添加新纪录
                        if (menu == null)
                        {
                            //硬件设备和组织结构还没有做映射
                            db.OnlineRate.Add(new OnlineRate
                            {
                                OnlineTime = DateTime.Now,
                                DeviceCode = deviceCode,
                            });
                        }
                        else
                        {
                            menu.IsActive = true;
                            var parent = db.Menus.AsNoTracking().FirstOrDefault(m => m.ID == menu.ParentID);
                            db.OnlineRate.Add(new OnlineRate
                            {
                                OnlineTime = DateTime.Now,
                                DeviceCode = deviceCode,
                                RegionName = parent.Name,
                                PointName = menu.Name,
                                UserId = menu.ID
                            });
                        }
                    }
                    db.SaveChanges();
                }
            }
            Task.Run(Action);
        }
    }

    /// <summary>
    /// 平台和Socket的连接
    /// </summary>
    public class HandleWebInfo : CommandBase<TelnetSession, StringRequestInfo>
    {
        /// <inheritdoc />
        public override void ExecuteCommand(TelnetSession session, StringRequestInfo requestInfo)
        {
            var param = requestInfo.Parameters;
            var obj = JsonConvert.DeserializeObject<SocketMsg>(param[0]);
            obj.IsSocketResponse = true;
            session.SignId = obj.SignId;
            session.Send(JsonConvert.SerializeObject(obj));
        }
    }

    /// <summary>
    /// 处理客户端返回的数据
    /// </summary>
    public class CallBack : CommandBase<TelnetSession, StringRequestInfo>
    {
        /// <inheritdoc />
        public override void ExecuteCommand(TelnetSession session, StringRequestInfo requestInfo)
        {
            void ActionTop()
            {
                var appServer = Program.AppServer;
                var socketMsg = JsonConvert.DeserializeObject<SocketMsg>(requestInfo.Body);
                socketMsg.IsSocketResponse = false;
                var ss = appServer?.GetAllSessions().FirstOrDefault(s => s.SignId == socketMsg.SignId);
                //appServer?.GetAllSessions().Where(s => s.SignId == "PlateForm").ToList();
                //先根据SignId找到对应的平台连接，如果实在找不到的话，就把这个消息发给所有平台连接
                if (ss == null)
                {
                    var allPlateSessions = appServer?.GetAllSessions().Where(s => s.SignId == "PlateForm").ToList();
                    if (allPlateSessions == null) Console.WriteLine($"平台的Socket断了,SignId：{socketMsg.SignId}");
                    else
                    {
                        foreach (var s in allPlateSessions)
                        {
                            if (socketMsg.QueryModel.MethodName == QueryModelMethodEnum.GetExcelPath) //报表文件
                            {
                                var fileByte = socketMsg.ByteResult;
                                File.WriteAllBytes(socketMsg.QueryModel.FullFileName, fileByte);
                            }
                            else if (socketMsg.QueryModel.MethodName == QueryModelMethodEnum.GetBackGroundImage) //图片背景
                            {
                                var imageByte = socketMsg.ByteResult;
                                var path = AppDomain.CurrentDomain.BaseDirectory;
                                File.WriteAllBytes($"{path}\\temp\\{socketMsg.DeviceCode}bg.jpg", imageByte);
                            }

                            socketMsg.QueryModel.ByteResult = null;
                            var msg = JsonConvert.SerializeObject(socketMsg,
                                new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii });

                            s.Send(msg);
                        }
                    }
                }
                else
                {
                    if (socketMsg.QueryModel.MethodName == QueryModelMethodEnum.GetExcelPath) //报表文件
                    {
                        var fileByte = socketMsg.ByteResult;
                        Console.WriteLine("文件路径" + socketMsg.QueryModel.FullFileName);
                        File.WriteAllBytes(socketMsg.QueryModel.FullFileName, fileByte);
                    }
                    else if (socketMsg.QueryModel.MethodName == QueryModelMethodEnum.GetBackGroundImage) //图片背景
                    {
                        var imageByte = socketMsg.ByteResult;
                        var path = AppDomain.CurrentDomain.BaseDirectory;
                        File.WriteAllBytes($"{path}\\temp\\{socketMsg.DeviceCode}bg.jpg", imageByte);
                    }
                    socketMsg.QueryModel.ByteResult = null;
                    var msg = JsonConvert.SerializeObject(socketMsg, new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii });
                    ss.Send(msg);

                    //收到消息表示在线
                    void Action()
                    {
                        using (var db = new PapsEntities())
                        {
                            var menuId = db.Device.FirstOrDefault(d => d.DeviceCode == socketMsg.DeviceCode)?.MenuId;
                            var menu = db.Menus.FirstOrDefault(m => m.ID == menuId && m.IsActive == false); //不在线的工点
                            if (menu == null) return;
                            menu.IsActive = true;
                            db.SaveChanges();
                        }
                    }
                    Task.Run(Action);
                }
            }
            Task.Run(ActionTop);
        }
    }

    /// <summary>
    /// 转发命令
    /// </summary>
    public class TransCmd : CommandBase<TelnetSession, StringRequestInfo>
    {
        /// <inheritdoc />
        public override void ExecuteCommand(TelnetSession session, StringRequestInfo requestInfo)
        {
            void Action()
            {
                var socketMsg = JsonConvert.DeserializeObject<PlateSocketMsg>(requestInfo.Body);
                session.SignId = socketMsg.SignId;
                var ss = SocketSessionManager.SessionList.Where(s => s.DeviceCode == socketMsg.DeviceCode).ToList();
                if (ss.Count == 0)
                {
                    var plateSocketMsg = JsonConvert.DeserializeObject<PlateSocketMsg>(requestInfo.Body);
                    plateSocketMsg.CanConnect = false;
                    session.Send(JsonConvert.SerializeObject(plateSocketMsg));
                }
                else
                {
                    socketMsg.CanConnect = true;
                    var cmdStr = JsonConvert.SerializeObject(socketMsg,
                                     new JsonSerializerSettings()
                                         {StringEscapeHandling = StringEscapeHandling.EscapeNonAscii}) + "##\r\n";
                    var bytes = Encoding.UTF8.GetBytes(cmdStr);
                    ss.ForEach(s => { s.Send(bytes, 0, bytes.Length); });
                }
            }

            Task.Run(Action);
        }
    }
}
