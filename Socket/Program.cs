﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using Model;
using Newtonsoft.Json;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Config;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.SocketEngine;

namespace Socket
{
    class Program
    {
        //public static IBootstrap Bootstrap = BootstrapFactory.CreateBootstrap();
        public static readonly TelnetServer AppServer = new TelnetServer();

        static void Main(string[] args)
        {
            //CheckAdministrator();
            var config = new ServerConfig()
            {
                Name = "SSServer",
                ServerTypeName = "SServer",
                ClearIdleSession = false, //60秒执行一次清理90秒没数据传送的连接，正常客户端3秒会有一个请求
                ClearIdleSessionInterval = 60,
                IdleSessionTimeOut = 90,
                TextEncoding = "GBK",
                MaxRequestLength = int.MaxValue, //最大包长度
                Ip = "Any",
                Port = 44444,
                MaxConnectionNumber = 10000
            };
            AppServer.Setup(config);

            AppServer.Start();

            Console.WriteLine("Socket服务运行中，为保证平台和客户端的正常通信，请勿关闭此程序!");
            Console.WriteLine("Socket服务运行中，为保证平台和客户端的正常通信，请勿关闭此程序!");
            Console.WriteLine("Socket服务运行中，为保证平台和客户端的正常通信，请勿关闭此程序!");
            
            while (Console.ReadKey().KeyChar != 'q')
            {
                Console.WriteLine();
                continue;
            }
        }
        private static void CheckAdministrator()
        {
            var wi = WindowsIdentity.GetCurrent();
            var wp = new WindowsPrincipal(wi);

            bool runAsAdmin = wp.IsInRole(WindowsBuiltInRole.Administrator);

            if (runAsAdmin) return;
            var processInfo = new ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase)
            {
                UseShellExecute = true, Verb = "runas"
            };


            try
            {
                Process.Start(processInfo);
            }
            catch (Exception)
            {
                // ignored
            }

            // Shut down the current process
            Environment.Exit(0);
        }
    }
}
