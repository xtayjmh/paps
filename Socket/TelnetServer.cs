﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Model;
using Newtonsoft.Json;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperSocket.SocketBase.Protocol;

namespace Socket
{
    /// <summary>
    /// 
    /// </summary>
    public class TelnetServer : AppServer<TelnetSession>
    {
        /// <summary>
        /// 
        /// </summary>
        public TelnetServer()
            : base(new CommandLineReceiveFilterFactory(Encoding.Default, new BasicRequestInfoParser(":", ">>")))
        {

        }
        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            return base.Setup(rootConfig, config);
        }

        public override IEnumerable<TelnetSession> GetAllSessions()
        {
            return base.GetAllSessions();
        }

        public override TelnetSession GetSessionByID(string sessionID)
        {
            return base.GetSessionByID(sessionID);
        }

        /// <summary>
        /// 新连接
        /// </summary>
        /// <param name="session"></param>
        protected override void OnNewSessionConnected(TelnetSession session)
        {
            base.OnNewSessionConnected(session);
            session.Send(JsonConvert.SerializeObject(new SocketMsg { SocketId = session.SessionID, QueryModel = new QueryModel { MethodName = QueryModelMethodEnum.Welcome } }));
            SocketSessionManager.SessionList.Add(session);
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="session"></param>
        /// <param name="reason"></param>
        protected override void OnSessionClosed(TelnetSession session, CloseReason reason)
        {
            base.OnSessionClosed(session, reason);
            SocketSessionManager.SessionList.Remove(session);
            var deviceCode = session.DeviceCode;
            var db = new PapsEntities();
            var onlineRate = db.OnlineRate.OrderByDescending(d => d.TableId).FirstOrDefault(d => d.DeviceCode == deviceCode);
            if (onlineRate == null) return;
            onlineRate.OfflineTime = DateTime.Now;
            db.SaveChanges();
        }
        /// <summary>
        /// 开始
        /// </summary>
        protected override void OnStartup()
        {
            base.OnStartup();
        }
        /// <summary>
        /// 结束
        /// </summary>
        protected override void OnStopped()
        {
            base.OnStopped();
            Thread.Sleep(2000);

        }
    }
}