﻿using Microsoft.Web.Administration;
using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Security.AccessControl;
using System.Windows.Forms;

namespace Installer
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }
        string iis = "";//其它方法也会用到，所以定义成公共参数
        string physicalDir = "";
        string webSiteId = "";
        string pwd = "sa";
        string dbServer = ".";
        string dbUser = "sa";
        string dbName = "Paps";
        string targetDir = "";
        protected override void OnAfterInstall(IDictionary stateSaver)
        {
            //System.Diagnostics.Debugger.Launch();//启动调试
            //base.Install(stateSaver);
            physicalDir = this.Context.Parameters["targetdir"].ToString();  //网站物理路径
            physicalDir = physicalDir.Substring(0, physicalDir.Length - 1);
            dbServer = Context.Parameters["server"].ToString();//数据库地址
            dbUser = Context.Parameters["user"].ToString();//账号
            pwd = Context.Parameters["pwd"].ToString();//密码
            targetDir = Context.Parameters["targetdir"].ToString();//安装路径
            iis = Context.Parameters["iis"].ToString();//iis地址
            var ip = Context.Parameters["ip"].ToString();//服务器的ip地址
            var port = Context.Parameters["port"].ToString();//端口号
            var isName = Context.Parameters["isname"].ToString();//站点名称
            try
            {
                NewWebSiteInfo nwsif = new NewWebSiteInfo(ip, port, isName.Trim(), (isName.Trim().Length > 0 ? isName : "Paps"), targetDir);
                CreateNewWebSite(nwsif);//创建站点
                AttachDatabase();//附加数据库
                SetFileRole();//设置站点文件夹访问权限
                WriteWebConfig();//写入WebConfig数据库的连接字符串
            }
            catch (Exception ex)
            {
                //ignore
            }
        }
        #region 判定网站是否存在

        /// <summary>
        /// 确定一个新的网站与现有的网站没有相同的。 
        /// 这样防止将非法的数据存放到IIS里面去 
        /// </summary>
        /// <param name="bindStr">网站邦定信息</param>
        /// <returns>真为可以创建，假为不可以创建</returns>
        public bool EnsureNewSiteEnavaible(string bindStr, string entPath)
        {
            DirectoryEntry ent = new DirectoryEntry(entPath);

            foreach (DirectoryEntry child in ent.Children)
            {
                if (child.SchemaClassName == "IIsWebServer" && child.Properties["ServerBindings"].Value != null && child.Properties["ServerBindings"].Value.ToString() == bindStr)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 设置文件夹权限 处理给EVERONE赋予所有权限
        /// </summary>
        /// <param name="FileAdd">文件夹路径</param>
        public void SetFileRole()
        {
            string FileAdd = this.Context.Parameters["targetdir"].ToString();
            FileAdd = FileAdd.Remove(FileAdd.LastIndexOf('\\'), 1);
            DirectorySecurity fSec = new DirectorySecurity();
            fSec.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            System.IO.Directory.SetAccessControl(FileAdd, fSec);
        }

        #region 获得站点新ID
        /// <summary>
        /// 取得现有最大站点ID，在其上加1即为新站点ID
        /// </summary>
        private string GetNewWebSiteID(string entPath)
        {
            int siteID = 1;
            DirectoryEntry rootEntry = new DirectoryEntry(entPath);
            foreach (DirectoryEntry de in rootEntry.Children)
            {
                if (de.SchemaClassName == "IIsWebServer")
                {
                    int ID = Convert.ToInt32(de.Name);
                    if (ID >= siteID)
                    {
                        siteID = ID + 1;
                    }
                }
            }
            webSiteId = siteID.ToString().Trim();
            return webSiteId;
        }
        #endregion

        #endregion
        /// <summary>
        /// 创建IIS站点
        /// </summary>
        /// <param name="siteInfo">新站点配置信息</param>
        public void CreateNewWebSite(NewWebSiteInfo siteInfo)
        {
            string entPath = String.Format("IIS://{0}/W3SVC", iis);

            //SetFileRole();

            if (!EnsureNewSiteEnavaible(siteInfo.BindString, entPath))
            {
                throw new Exception("该网站已存在" + Environment.NewLine + siteInfo.BindString);
            }

            DirectoryEntry rootEntry = new DirectoryEntry(entPath);

            string newSiteNum = GetNewWebSiteID(entPath);

            DirectoryEntry newSiteEntry = rootEntry.Children.Add(newSiteNum, "IIsWebServer");
            newSiteEntry.CommitChanges();

            newSiteEntry.Properties["ServerBindings"].Value = siteInfo.BindString;
            newSiteEntry.Properties["ServerComment"].Value = siteInfo.CommentOfWebSite;
            newSiteEntry.Properties["ServerAutoStart"].Value = true;//网站是否启动
            newSiteEntry.CommitChanges();

            DirectoryEntry vdEntry = newSiteEntry.Children.Add("root", "IIsWebVirtualDir");
            vdEntry.CommitChanges();
            string ChangWebPath = siteInfo.WebPath.Trim().Remove(siteInfo.WebPath.Trim().LastIndexOf('\\'), 1);
            vdEntry.Properties["Path"].Value = ChangWebPath;


            vdEntry.Invoke("AppCreate", true);//创建应用程序
            //vdEntry.Properties["ServerAutoStart"].Value = true;//网站是否启动
            vdEntry.Properties["AccessRead"][0] = true; //设置读取权限
            vdEntry.Properties["AccessWrite"][0] = true;
            vdEntry.Properties["AccessScript"][0] = true;//执行权限
            vdEntry.Properties["AccessExecute"][0] = false;
            vdEntry.Properties["DefaultDoc"][0] = "";//设置默认文档
            vdEntry.Properties["AppFriendlyName"][0] = "Paps"; //应用程序名称           
            vdEntry.Properties["AuthFlags"][0] = 1;//0表示不允许匿名访问,1表示就可以3为基本身份验证，7为windows继承身份验证
            vdEntry.CommitChanges();

            //操作增加MIME
            //IISOle.MimeMapClass NewMime = new IISOle.MimeMapClass();
            //NewMime.Extension = ".xaml"; NewMime.MimeType = "application/xaml+xml";
            //IISOle.MimeMapClass TwoMime = new IISOle.MimeMapClass();
            //TwoMime.Extension = ".xap"; TwoMime.MimeType = "application/x-silverlight-app";
            //rootEntry.Properties["MimeMap"].Add(NewMime);
            //rootEntry.Properties["MimeMap"].Add(TwoMime);
            //rootEntry.CommitChanges();

            #region 针对IIS7
            DirectoryEntry getEntity = new DirectoryEntry("IIS://" + iis + "/W3SVC/INFO");
            int Version = int.Parse(getEntity.Properties["MajorIISVersionNumber"].Value.ToString());
            if (Version > 6)
            {
                #region 创建应用程序池
                string AppPoolName = "Paps";
                if (!IsAppPoolName(AppPoolName))
                {
                    DirectoryEntry newpool;
                    DirectoryEntry appPools = new DirectoryEntry("IIS://" + iis + "/W3SVC/AppPools");
                    newpool = appPools.Children.Add(AppPoolName, "IIsApplicationPool");
                    newpool.CommitChanges();
                }
                #endregion

                #region 修改应用程序的配置(包含托管模式及其NET运行版本)
                ServerManager sm = new ServerManager();
                sm.ApplicationPools[AppPoolName].ManagedRuntimeVersion = "v4.0";
                sm.ApplicationPools[AppPoolName].ManagedPipelineMode = ManagedPipelineMode.Classic; //托管模式Integrated为集成 Classic为经典
                sm.CommitChanges();
                #endregion

                vdEntry.Properties["AppPoolId"].Value = AppPoolName;
                vdEntry.CommitChanges();
            }

            #endregion


            //启动aspnet_regiis.exe程序 
            string fileName = Environment.GetEnvironmentVariable("windir") + @"\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis.exe";
            ProcessStartInfo startInfo = new ProcessStartInfo(fileName);
            //处理目录路径 
            string path = vdEntry.Path.ToUpper();
            int index = path.IndexOf("W3SVC");
            path = path.Remove(0, index);
            //启动ASPnet_iis.exe程序,刷新脚本映射 
            startInfo.Arguments = "-s " + path;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
            string errors = process.StandardError.ReadToEnd();

            if (errors != string.Empty)
            {
                throw new Exception(errors);
            }

        }
        #region 判断应用程序池是否存在
        /// <summary>
        /// 返回true即应用程序池存在
        /// </summary>
        /// <param name="AppPoolName">应用程序池名</param>
        private bool IsAppPoolName(string AppPoolName)
        {
            bool result = false;
            DirectoryEntry appPools = new DirectoryEntry("IIS://localhost/W3SVC/AppPools");
            foreach (DirectoryEntry getdir in appPools.Children)
            {
                if (getdir.Name.Equals(AppPoolName))
                {
                    result = true;
                }
            }
            return result;
        }
        #endregion

        #region 写入连接字符串至配置文件
        private void WriteWebConfig()
        {
            //加载配置文件
            //string path = physicalDir;
            //string pathpwd = pwd;
            //System.IO.FileInfo FileInfo = new System.IO.FileInfo(physicalDir + "web.config");
            //if (!FileInfo.Exists)
            //{
            //    throw new InstallException("缺少配置文件 :" + physicalDir + "web.config");
            //}
            //System.Xml.XmlDocument xmlDocument = new System.Xml.XmlDocument();
            //xmlDocument.Load(FileInfo.FullName);

            ////写入连接字符串
            //foreach (System.Xml.XmlNode Node in xmlDocument["configuration"]["connectionStrings"])
            //{
            //    if (Node.Name == "add")
            //    {
            //        if (Node.Attributes.GetNamedItem("name").Value == "conn")
            //        {
            //            Node.Attributes.GetNamedItem("connectionString").Value = String.Format("Database={0};Server={1};Uid={2};Pwd={3};", dbName, dbServer, dbUser, pwd);

            //        }

            //    }
            //}
            //xmlDocument.Save(FileInfo.FullName);
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(Context.Parameters["targetdir"] + "Web");
            config.ConnectionStrings.ConnectionStrings["PapsEntities"].ConnectionString = $"metadata=res://*/Takata.csdl|res://*/Takata.ssdl|res://*/Takata.msl;provider=System.Data.SqlClient;provider connection string=\"data source={dbServer};initial catalog={dbName};persist security info=True;user id={dbUser};password={pwd};MultipleActiveResultSets=True;App=EntityFramework\"";

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            ConfigurationManager.RefreshSection("connectionStrings");
        }
        #endregion
        #region 附加数据库
        /// <summary>
        /// 获取数据库登陆连接字符串
        /// </summary>
        /// <param name="databasename"></param>
        /// <returns></returns>
        private string GetConnectionString(string databaseName)
        {
            return "server=" + dbServer + ";database=" + (string.IsNullOrEmpty(databaseName) ? "master" : databaseName) + ";User ID=" + dbUser + ";Password=" + pwd;
        }
        /// <summary>
        /// 执行SQL语句
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="sql"></param>
        void ExecuteSQL(SqlConnection connection, string sql)
        {
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.ExecuteNonQuery();
        }
        private void AttachDatabase()
        {
            //给文件添加"Authenticated Users,Everyone,Users"用户组的完全控制权限 ，要附加的数据库文件必须加权限否则无法附加
            if (File.Exists(targetDir + "App_Data\\Paps.mdf"))
            {
                FileInfo fi = new FileInfo(targetDir + "App_Data\\Paps.mdf");
                FileSecurity fileSecurity = fi.GetAccessControl();
                fileSecurity.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, AccessControlType.Allow));
                fileSecurity.AddAccessRule(new FileSystemAccessRule("Authenticated Users", FileSystemRights.FullControl, AccessControlType.Allow));
                fileSecurity.AddAccessRule(new FileSystemAccessRule("Users", FileSystemRights.FullControl, AccessControlType.Allow));
                fi.SetAccessControl(fileSecurity);
                FileInfo fi1 = new FileInfo(targetDir + "App_Data\\Paps_log.ldf");
                FileSecurity fileSecurity1 = fi1.GetAccessControl();
                fileSecurity1.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, AccessControlType.Allow));
                fileSecurity1.AddAccessRule(new FileSystemAccessRule("Authenticated Users", FileSystemRights.FullControl, AccessControlType.Allow));
                fileSecurity1.AddAccessRule(new FileSystemAccessRule("Users", FileSystemRights.FullControl, AccessControlType.Allow));
                fi1.SetAccessControl(fileSecurity1);
            }

            string connectionString = GetConnectionString(null);

            //保存数据连接词，为卸载做准备
            File.WriteAllText(Path.Combine(targetDir + "\\" + "App_Data\\", "log.txt"), connectionString);

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    //使用数据库文件创建数据库，所以添加的网站项目中需要有App_Data文件夹和数据库文件（Paps.mdf）和日志文件（Paps.ldf）
                    string sql = "sp_attach_db 'Paps','" + targetDir + "App_Data\\Paps.mdf','" + targetDir + "App_Data\\Paps_log.ldf'";
                    //todo 这里将来改成读取一个文本文件，里边保存的是更新的sql语句
                    ExecuteSQL(connection, sql);
                    connection.Close();

                    //修改config文件连接词
                    string webconfigpath = Path.Combine(targetDir, "web.config");
                    string webcofnigstring = File.ReadAllText(webconfigpath).Replace("#constring#", GetConnectionString(dbName));
                    File.WriteAllText(webconfigpath, webcofnigstring);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("安装出错了1！\n" + ex.ToString(), "出错啦！");
            }
        }
        #endregion
    }
}
