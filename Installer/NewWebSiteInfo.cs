﻿using System;

namespace Installer
{
    public class NewWebSiteInfo
    {
        private string hostIP;   // 主机IP
        private string descOfWebSite; // 网站表示。一般为网站的网站名。例如"www.dns.com.cn"

        /// <summary>
        /// 实例化IIS站点配置
        /// </summary>
        /// <param name="hostIP">主机IP</param>
        /// <param name="portNum">网站端口号</param>
        /// <param name="descOfWebSite">网站表示。一般为网站的网站名。例如"www.dns.com.cn"--【主机名(域名)】</param>
        /// <param name="commentOfWebSite">网站注释。一般也为网站的网站名。--【iis网站站点名称】</param>
        /// <param name="webPath">网站的主目录。例如"e:\ mp"</param>
        public NewWebSiteInfo(string hostIP, string portNum, string descOfWebSite, string commentOfWebSite, string webPath)
        {
            this.hostIP = hostIP;
            PortNum = portNum;
            this.descOfWebSite = descOfWebSite;
            CommentOfWebSite = commentOfWebSite;
            WebPath = webPath;
        }

        /// <summary>
        /// 网站标识
        /// </summary>
        public string BindString => $"{hostIP}:{PortNum}:{descOfWebSite}";

        /// <summary>
        /// 网站端口号
        /// </summary>
        public string PortNum { get; }
        /// <summary>
        /// 网站表示。一般为网站的网站名。例如"www.dns.com.cn"
        /// </summary>
        public string CommentOfWebSite { get; }
        /// <summary>
        /// 网站的主目录。例如"e:\ mp"
        /// </summary>
        public string WebPath { get; }
    }
}
