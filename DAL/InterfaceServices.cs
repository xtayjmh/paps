﻿using Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace DAL
{
    public class InterfaceServices
    {
        private static InterfaceServices instance = null;

        //public static Uri Uri { get; set; } = new Uri("http://106.112.136.99:81/SDService.asmx");
        public static Uri Uri { get; set; }

        private static XmlDocument GetResponse(StringBuilder sb, int timeout = 3000)
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            soap.Append("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">");
            soap.Append("<soap:Header/>");
            soap.Append("<soap:Body>");

            soap.Append(sb);

            soap.Append("</soap:Body>");
            soap.Append("</soap:Envelope>");
            if (Uri == null) return new XmlDocument();
            WebRequest webRequest = WebRequest.Create(Uri);
            webRequest.Timeout = timeout; //客户要求的要保证2秒内打开工点，所以这个超时时间设置太长没有意义，网络环境都挺好的，通就是通，不通就不通了
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            WebResponse webResponse;
            try
            {
                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                    requestStream.Write(paramBytes, 0, paramBytes.Length);
                }

                //响应
                webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(myStreamReader.ReadToEnd());
                    return doc;
                }
            }
            catch (WebException e)
            {
                return new XmlDocument();
            }

        }

        public static InterfaceServices Instance => instance ?? (instance = new InterfaceServices());
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        public static List<DeptModel> GetDeptList()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<tem:GetDeptList/>");
            var list = new List<DeptModel> { new DeptModel() { DeptName = "全部", DeptNo = "" } };
            var obj = GetResponse(sb, 3000).GetElementsByTagName("DeptList");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            for (int i = 0; i < obj.Count; i++)
            {
                list.Add(new DeptModel() { DeptNo = obj[i].ChildNodes[0].InnerText, DeptName = obj[i].ChildNodes[1].InnerText });
            }
            return list;
        }
        /// <summary>
        /// 获取职务列表
        /// </summary>
        /// <returns></returns>
        public static List<TitleModel> GetTitleList()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetTitleList/>");
            var list = new List<TitleModel>();
            list.Add(new TitleModel()
            {
                TitleName = "全部",
                TitleNo = ""
            });
            var obj = GetResponse(sb).GetElementsByTagName("Title");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            for (int i = 0; i < obj.Count; i++)
            {
                list.Add(new TitleModel() { TitleNo = obj[i].ChildNodes[0].InnerText, TitleName = obj[i].ChildNodes[1].InnerText });
            }
            return list;
        }
        /// <summary>
        /// 获取基站信息
        /// </summary>
        /// <returns></returns>
        public static List<BaseStation> GetBaseStationList()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetBaseStationList/>");
            var list = new List<BaseStation>();
            list.Add(new BaseStation() { BaseStationName = "全部", BaseStationNo = "" });
            var obj = GetResponse(sb).GetElementsByTagName("Site");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            for (int i = 0; i < obj.Count; i++)
            {
                list.Add(new BaseStation() { BaseStationNo = obj[i].ChildNodes[0].InnerText, BaseStationName = obj[i].ChildNodes[1].InnerText });
            }
            return list;
        }

        /// <summary>
        /// 获取实时人员统计数据
        /// </summary>
        /// <returns></returns>
        public static RealTimePersonStatisticsDataModel GetRealTimePersonStatisticsData()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetRealTimePersonStatisticsData/>");
            var obj = GetResponse(sb).GetElementsByTagName("RealTimePersonStatisticsData")[0];
            var obj1 = GetResponse(sb).GetElementsByTagName("Table")[0];
            if (obj == null && obj1 != null) obj = obj1;
            if (obj == null) return null;
            var model = new RealTimePersonStatisticsDataModel
            {
                Total = int.Parse(obj.ChildNodes[0].InnerText),
                TunnelOut = int.Parse(obj.ChildNodes[1].InnerText),
                TunnelIn = int.Parse(obj.ChildNodes[2].InnerText),
                Manager = int.Parse(obj.ChildNodes[3].InnerText),
                KQBS = int.Parse(obj.ChildNodes[4].InnerText),
                ZZMBS = int.Parse(obj.ChildNodes[5].InnerText),
                OtherArea = int.Parse(obj.ChildNodes[6].InnerText),
                RestrictedArea = int.Parse(obj.ChildNodes[7].InnerText)
            };
            if (obj.ChildNodes[8] != null) model.TunnelType = obj.ChildNodes[8].InnerText;
            if (obj.ChildNodes[9] != null) model.Left = int.Parse(obj.ChildNodes[9].InnerText);
            if (obj.ChildNodes[10] != null) model.Right = int.Parse(obj.ChildNodes[10].InnerText);
            return model;
        }

        /// <summary>
        /// 获取基站人员人信息
        /// </summary>
        /// <param name="bsType">1-考勤基站，获取当前隧道内人员的信息；2-掌子面基站，获取当前掌子面定位基站附近人员的信息</param>
        /// <returns>根据EnterTime降序排序</returns>
        public static List<BSPerInfo> GetBsPerInfos(int bsType)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetBSPerInfo >");
            sb.Append($"<tem:BSType>{bsType}</tem:BSType>");
            sb.Append("</tem:GetBSPerInfo>");

            var obj = GetResponse(sb).GetElementsByTagName("BSPerInfo");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            if (obj.Count == 0) return null;
            var list = new List<BSPerInfo>();
            for (var i = 0; i < obj.Count; i++)
            {
                if (obj[i].Attributes["diffgr:hasChanges"] == null || obj[i].Attributes["diffgr:hasChanges"].Value != "modified")
                {
                    var mode = new BSPerInfo()
                    {
                        LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                        IdCardNo = obj[i].ChildNodes[1].InnerText,
                        Name = obj[i].ChildNodes[2].InnerText,
                        Dept = obj[i].ChildNodes[3].InnerText,
                        Title = obj[i].ChildNodes[4].InnerText,
                        EnterTime = ToDateTimeString(obj[i].ChildNodes[5].InnerText),
                    };
                    if (obj[i].ChildNodes[6] != null) mode.SideName = obj[i].ChildNodes[6].InnerText;
                    if (obj[i].ChildNodes[7] != null) mode.SiteName = obj[i].ChildNodes[7].InnerText;
                    if (obj[i].ChildNodes[8] != null) mode.Catelog = obj[i].ChildNodes[8].InnerText;
                    list.Add(mode);
                }
            }
            return list.ToList();
        }

        /// <summary>
        /// 获取人员定位信息
        /// </summary>
        /// <returns></returns>
        public static List<BSPerInfo> GetPerPosition()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetPerPosition>");
            sb.Append("</tem:GetPerPosition>");

            var obj = GetResponse(sb).GetElementsByTagName("BSPerInfo");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            if (obj.Count == 0) return null;
            var list = new List<BSPerInfo>();
            for (var i = 0; i < obj.Count; i++)
            {
                if (obj[i].Attributes["diffgr:hasChanges"] == null || obj[i].Attributes["diffgr:hasChanges"].Value != "modified")
                {
                    var mode = new BSPerInfo()
                    {
                        LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                        IdCardNo = obj[i].ChildNodes[1].InnerText,
                        Name = obj[i].ChildNodes[2].InnerText,
                        Catelog = obj[i].ChildNodes[3].InnerText,
                        Dept = obj[i].ChildNodes[4].InnerText,
                        Title = obj[i].ChildNodes[5].InnerText,
                        SiteName = obj[i].ChildNodes[6].InnerText,
                    };
                    list.Add(mode);
                }
            }
            return list.ToList();
        }

        /// <summary>
        /// 获取隧道内人员信息
        /// </summary>
        /// <returns></returns>
        public static TunnelInPerInfo GetTunnelInPerInfos()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetTunnelInPerInfo/>");
            var obj = GetResponse(sb).GetElementsByTagName("TunnelInPerInfo")[0];
            var obj1 = GetResponse(sb).GetElementsByTagName("Table")[0];
            if (obj == null && obj1 != null) obj = obj1;
            if (obj == null) return null;
            var model = new TunnelInPerInfo()
            {
                QuotaNum = int.Parse(obj.ChildNodes[0].InnerText),
                ActualNum = int.Parse(obj.ChildNodes[1].InnerText),
                OverPerNum = int.Parse(obj.ChildNodes[2].InnerText),
                QuotaTime = int.Parse(obj.ChildNodes[3].InnerText),
                OverTimeNum = obj.ChildNodes[4].InnerText
            };
            return model;
        }

        /// <summary>
        /// 某月的考勤报表
        /// </summary>
        /// <param name="date">月份。xxxx-xx，2017-01</param>
        /// <param name="deptNo">部门编号，空时表示查询所有</param>
        /// <returns></returns>
        public static List<MonthReport> GetMonthReport(string date, string deptNo)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetMonthReport>");
            sb.Append($"<tem:Date>{date}</tem:Date>");
            sb.Append($"<tem:DeptNo>{deptNo}</tem:DeptNo>");
            sb.Append("</tem:GetMonthReport>");
            var obj = GetResponse(sb, 30000).GetElementsByTagName("tableMonth");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            var list = new List<MonthReport>();
            for (int i = 0; i < obj.Count; i++)
            {
                var x = obj[i].ChildNodes.Count;
                var model = new MonthReport
                {
                    OverTime = int.Parse(obj[i].ChildNodes[x - 1].InnerText),
                    Normal = int.Parse(obj[i].ChildNodes[x - 2].InnerText),
                    LateOrEarly = int.Parse(obj[i].ChildNodes[x - 3].InnerText),
                    Absent = int.Parse(obj[i].ChildNodes[x - 4].InnerText),
                    Name = obj[i].ChildNodes[0].InnerText,
                    Detail = new List<string>()
                };
                for (var j = obj[i].ChildNodes.Count - 5; j > 0; j--)
                {
                    model.Detail.Add(obj[i].ChildNodes[j].InnerText);
                }
                list.Add(model);
            }

            return list;
        }

        /// <summary>
        /// 获取日考勤报表
        /// </summary>
        /// <param name="date">月份。xxxx-xx，2017-01</param>
        /// <param name="deptNo">部门编号，空时表示查询所有</param>
        /// <returns>按标志卡号升序排列</returns>
        public static List<DayReport> GetDayReport(string date, string deptNo)
        {
            var sb = new StringBuilder();
            sb.Append($"<tem:GetDayReport><tem:Date>{date}</tem:Date><tem:DeptNo>{deptNo}</tem:DeptNo ></tem:GetDayReport>");
            var obj = GetResponse(sb).GetElementsByTagName("DayReport");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            var list = new List<DayReport>();
            for (var i = 0; i < obj.Count; i++)
            {
                list.Add(new DayReport
                {
                    BsPerInfo = new BSPerInfo
                    {
                        Dept = obj[i].ChildNodes[3].InnerText,
                        EnterTime = ToDateTimeString(obj[i].ChildNodes[5].InnerText),
                        IdCardNo = obj[i].ChildNodes[1].InnerText,
                        LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                        Name = obj[i].ChildNodes[2].InnerText,
                        Title = obj[i].ChildNodes[4].InnerText
                    },
                    EnterTime = ToDateTimeString(obj[i].ChildNodes[5].InnerText)
                });
                if (obj[i].ChildNodes.Count <= 6) continue;
                list[i].OutTime = ToDateTimeString(obj[i].ChildNodes[6].InnerText);
                list[i].WorkingTime = obj[i].ChildNodes[7].InnerText.Replace(":", "小时") + "分钟";
            }
            return list.OrderBy(i => i.BsPerInfo.IdCardNo).ToList();
        }

        /// <summary>
        /// 获取日考勤报表
        /// </summary>
        /// <param name="date">月份。xxxx-xx，2017-01</param>
        /// <param name="cardNo"></param>
        /// <returns>按标志卡号升序排列</returns>
        public static string GetDayRecord(string date, string cardNo)
        {
            var sb = new StringBuilder();
            sb.Append($"<tem:GetDayReportByCode><tem:Date>{date}</tem:Date><tem:cardNo>{cardNo}</tem:cardNo ></tem:GetDayReportByCode>");
            var objStr = GetResponse(sb).InnerText;
            return objStr;
        }
        /// <summary>
        /// 获取一年所有的进出记录
        /// </summary>
        /// <param name="date"></param>
        /// <param name="cardNo"></param>
        /// <returns></returns>
        public static string GetExcelPath(int year, string keyWord)
        {
            var sb = new StringBuilder();
            sb.Append($"<tem:GetExcelPath><tem:year>{year}</tem:year><tem:keyWord>{keyWord}</tem:keyWord></tem:GetExcelPath>");
            var objStr = GetResponse(sb, 60000).InnerText;
            return objStr;
        }



        /// <summary>
        /// 获取系统报警总数
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        public static int GetSysAlarmCount(SysAlarmQueryModel queryModel)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetSysAlarmCount>");
            sb.Append($"<tem:StartTime>{queryModel.StartTime}</tem:StartTime>");
            sb.Append($"<tem:EndTime>{queryModel.EndTime}</tem:EndTime>");
            sb.Append($"<tem:PerName>{queryModel.PerName}</tem:PerName>");
            sb.Append($"<tem:DeptNo>{queryModel.DeptNo}</tem:DeptNo>");
            sb.Append($"<tem:TitleNo>{queryModel.TitleNo}</tem:TitleNo>");
            sb.Append($"<tem:BaseStationNo>{queryModel.BaseStationNo}</tem:BaseStationNo>");
            sb.Append("</tem:GetSysAlarmCount>");
            var obj = GetResponse(sb).GetElementsByTagName("GetSysAlarmCountResult")[0];
            return int.Parse(obj.InnerText);
        }

        /// <summary>
        /// 获取报警详情
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns>按照报警时间降序排列</returns>
        public static List<SysAlarmData> GetSysAlarmData(SysAlarmQueryModel queryModel)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetSysAlarmData>");
            sb.Append($"<tem:StartTime>{queryModel.StartTime}</tem:StartTime>");
            sb.Append($"<tem:EndTime>{queryModel.EndTime}</tem:EndTime>");
            sb.Append($"<tem:PerName>{queryModel.PerName}</tem:PerName>");
            sb.Append($"<tem:DeptNo>{queryModel.DeptNo}</tem:DeptNo>");
            sb.Append($"<tem:TitleNo>{queryModel.TitleNo}</tem:TitleNo>");
            sb.Append($"<tem:BaseStationNo>{queryModel.BaseStationNo}</tem:BaseStationNo>");
            sb.Append($"<tem:PageIndex>{queryModel.PageIndex}</tem:PageIndex>");
            sb.Append($"<tem:PageSize>{queryModel.PageSize}</tem:PageSize>");
            sb.Append("</tem:GetSysAlarmData>");
            var obj = GetResponse(sb).GetElementsByTagName("SysAlarmData");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            var list = new List<SysAlarmData>();
            for (var i = 0; i < obj.Count; i++)
            {
                list.Add(new SysAlarmData
                {
                    LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                    AlarmTime = ToDateTimeString(obj[i].ChildNodes[1].InnerText),
                    IdCardNo = obj[i].ChildNodes[2].InnerText,
                    PerName = obj[i].ChildNodes[3].InnerText,
                    Dept = obj[i].ChildNodes[4].InnerText,
                    Title = obj[i].ChildNodes[5].InnerText,
                    BSName = obj[i].ChildNodes[6].InnerText
                });
            }
            return list;
        }

        /// <summary>
        ///  超员报警总数
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static int GetOverPerAlarmDataCount(string startTime, string endTime)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetOverPerAlarmCount>");
            sb.Append($"<tem:StartTime>{startTime}</tem:StartTime>");
            sb.Append($"<tem:EndTime>{endTime}</tem:EndTime>");
            sb.Append("</tem:GetOverPerAlarmCount>");
            var obj = GetResponse(sb).GetElementsByTagName("GetOverPerAlarmCountResult")[0];
            return int.Parse(obj.InnerText);
        }

        /// <summary>
        /// 获取超员报警详情
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>按开始时间降序排序</returns>
        public static List<OverPerAlarmData> GetOverPerAlarmData(string startTime, string endTime, int pageIndex, int pageSize)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetOverPerAlarmData>");
            sb.Append($"<tem:StartTime>{startTime}</tem:StartTime>");
            sb.Append($"<tem:EndTime>{endTime}</tem:EndTime>");
            sb.Append($"<tem:PageIndex>{pageIndex}</tem:PageIndex>");
            sb.Append($"<tem:PageSize>{pageSize}</tem:PageSize>");
            sb.Append("</tem:GetOverPerAlarmData>");
            var obj = GetResponse(sb).GetElementsByTagName("OverPerAlarmData");
            var obj1 = GetResponse(sb).GetElementsByTagName("Table");
            if (obj.Count == 0 && obj1.Count > 0) obj = obj1;
            var list = new List<OverPerAlarmData>();
            for (var i = 0; i < obj.Count; i++)
            {
                list.Add(new OverPerAlarmData
                {
                    LineNo = int.Parse(obj[i].ChildNodes[0].InnerText),
                    StartTime = ToDateTimeString(obj[i].ChildNodes[1].InnerText),
                    EndTime = ToDateTimeString(obj[i].ChildNodes[2].InnerText),
                    QuotaNum = int.Parse(obj[i].ChildNodes[3].InnerText),
                    ActualNum = int.Parse(obj[i].ChildNodes[4].InnerText),
                    OverPerNum = int.Parse(obj[i].ChildNodes[5].InnerText),
                    KeepTime = int.Parse(obj[i].ChildNodes[6].InnerText)
                });
            }
            return list;
        }
        /// <summary>
        /// 转换时间格式
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string ToDateTimeString(string str)
        {
            return string.IsNullOrEmpty(str) ? "" : Convert.ToDateTime(str).ToString("yyyy-MM-dd HH:mm:ss");
        }

        private static DateTime StrToDatetime(string str)
        {
            return string.IsNullOrEmpty(str) ? DateTime.MinValue : Convert.ToDateTime(str);
        }

        public static Image BytesToImage(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream(buffer);
            Image image = Image.FromStream(ms);
            return image;
        }

        public static string CreateImageFromBytes(string fileName, byte[] buffer)
        {
            string file = fileName;
            Image image = BytesToImage(buffer);
            ImageFormat format = image.RawFormat;
            if (format.Equals(ImageFormat.Jpeg))
            {
                file += ".jpeg";
            }
            else if (format.Equals(ImageFormat.Png))
            {
                file += ".png";
            }
            else if (format.Equals(ImageFormat.Bmp))
            {
                file += ".bmp";
            }
            else if (format.Equals(ImageFormat.Gif))
            {
                file += ".gif";
            }
            else if (format.Equals(ImageFormat.Icon))
            {
                file += ".icon";
            }
            FileInfo info = new FileInfo(file);
            Directory.CreateDirectory(info.Directory.FullName);
            File.WriteAllBytes(file, buffer);
            return file;
        }

        public static string GetYearData(int year, string keyWord)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetYearData>");
            sb.Append($"<tem:year>{year}</tem:year>");
            sb.Append($"<tem:keyWord>{keyWord}</tem:keyWord>");
            sb.Append("</tem:GetYearData>");
            var objStr = GetResponse(sb, 40000).InnerText;
            return objStr;
        }
        public static string GetMonthData(string month, string cardNo)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetMonthData>");
            sb.Append($"<tem:month>{month}</tem:month>");
            sb.Append($"<tem:cardNo>{cardNo}</tem:cardNo>");
            sb.Append("</tem:GetMonthData>");
            var objStr = GetResponse(sb, 40000).InnerText;
            return objStr;
        }

        public static List<DailyDetail> GetDailyDetail(string type, string workPro, string keyWord, string year)
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetTunnelRecord>");
            sb.Append($"<tem:type>{type}</tem:type>");
            sb.Append($"<tem:workPro>{workPro}</tem:workPro>");
            sb.Append($"<tem:keyWord>{keyWord}</tem:keyWord>");
            sb.Append($"<tem:year>{year}</tem:year>");
            sb.Append("</tem:GetTunnelRecord>");
            var obj = GetResponse(sb, 40000).GetElementsByTagName("TunnelRecord");
            var list = new List<DailyDetail>();
            for (var i = 0; i < obj.Count; i++)
            {
                list.Add(new DailyDetail
                {
                    CardNo = obj[i].ChildNodes[0].InnerText,
                    Name = obj[i].ChildNodes[1].InnerText,
                    StartTime = StrToDatetime(obj[i].ChildNodes[2]?.InnerText),
                    DeptName = obj[i].ChildNodes[3].InnerText,
                    EndTime = obj[i].ChildNodes[4] == null ? StrToDatetime(obj[i].ChildNodes[2]?.InnerText) : StrToDatetime(obj[i].ChildNodes[4].InnerText),
                });
            }

            return list;
        }
        public static RealtimeWatchModel GetRtwData(string type)
        {
            return new RealtimeWatchModel()
            {
                Image = "../Content/img/bg.png",
                Persons = new List<PersonModel>()
                {
                    new PersonModel()
                    {
                        CardNo = "10086",
                        Name = "张三",
                        Position = "",
                        Department = "管理部门",
                        Title = "监理",
                        Type = "监理部门"
                    },
                    new PersonModel()
                    {
                        CardNo = "10087",
                        Name = "陈琦",
                        Position = "V1区",
                        Department = "",
                        Title = "监理",
                        Type = "监理部门"
                    },
                    new PersonModel()
                    {
                        CardNo = "",
                        Name = "赵六",
                        Position = "V2区",
                        Department = "管理部门",
                        Title = "",
                        Type = "监理部门"
                    },
                    new PersonModel()
                    {
                        CardNo = "10089",
                        Name = "",
                        Position = "V3区",
                        Department = "管理部门",
                        Title = "监理",
                        Type = "监理部门"
                    },
                    new PersonModel()
                    {
                        CardNo = "10010",
                        Name = "李四",
                        Position = "掌子面",
                        Department = "管理部门",
                        Title = "监理",
                        Type = ""
                    },
                },
                TunnelNumber = 10,
                TunnelFaceNumber = 2
            };
        }

        public static List<string> GetTypes()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetTypes>");
            sb.Append("</tem:GetTypes>");
            var obj = GetResponse(sb).GetElementsByTagName("Catelog");
            var list = new List<string>();
            for (var i = 0; i < obj.Count; i++)
            {
                var val = obj[i].ChildNodes[0];
                if (val != null) list.Add(val.InnerText);
            }

            return list.Distinct().ToList();
        }

        public static List<WorkStep> GetWorkStep()
        {
            var sb = new StringBuilder();
            sb.Append("<tem:GetWorkStep>");
            sb.Append("</tem:GetWorkStep>");
            var obj = GetResponse(sb).GetElementsByTagName("WorkSteps");
            var list = new List<WorkStep>();
            for (var i = 0; i < obj.Count; i++)
            {
                list.Add(new WorkStep() { WorkStepId = int.Parse(obj[i].ChildNodes[0].InnerText), WorkStepName = obj[i].ChildNodes[1].InnerText });
            }

            return list;
        }

    }
}
