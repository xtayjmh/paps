﻿using DAL.Common;
using Model;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Caching;
using static Model.LoginModel;

namespace DAL
{
    public class LoginServices
    {
        /// <summary>
        /// 获取token并缓存7200秒
        /// </summary>
        public static string GetAccessToken(string tokenUrls)
        {
            try
            {
                //Write("get token Urls:" + tokenUrls);
                //Cache data = new Cache();
                //Cache data = HttpRuntime.Cache;
                //if (data["token"] == null)
                //{
                //Write("data[\"token\"] is null");
                    var response = GetToken(tokenUrls);// HttpResponseInfo.RequestGetService<AccessToken>(HttpContext.Current, tokenUrls);
                    if (!string.IsNullOrEmpty(response?.token))
                    {
                        //Write("get token:" + response.token);
                        //data.Insert("token", response.token, null, DateTime.Now.AddSeconds(response.expires_in), Cache.NoSlidingExpiration);
                        return response.token;
                    }
                    else
                    {
                        return "";
                    }
                //}
                //Write("data[\"token\"] is not null");
                //return true;
            }
            catch(Exception ex)
            {
                //Write("get token Urls error:" + ex.Message);
                return "";
            }
        }
        /// <summary>
        /// 大铁登录接口
        /// </summary>
        /// <returns></returns>
        public static Account GetServiceUser(string loginUrls)
        {
            try
            {
                //Write("get loginUrls:" + loginUrls);
                var response = HttpResponseInfo.RequestGetService<Account>(HttpContext.Current, loginUrls);
                //if (response != null)
                //    Write("get response:" + response.account + "====" + response.userid);
                //else
                //    Write("get loginUrls: nul");
                return response;
            }
            catch
            {
                return null;
            }
        }

        public static Account GetServiceUser2(string loginUrls)
        {
            string result = "";
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(loginUrls);
                webRequest.Method = "GET";
                webRequest.ContentType = "application/json";
                webRequest.KeepAlive = true;

                HttpWebResponse hwRes = webRequest.GetResponse() as HttpWebResponse;
                if (hwRes.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream rStream = hwRes.GetResponseStream();
                    StreamReader sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    //Write("get user info:" + result);
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "Connect Error";
                }

                hwRes.Close();

            }
            catch (Exception ex)
            {
                //Write("get user info:" + ex.Message);
            }
            return CommonJson.JsonDeserialize<Account>(result);
        }


        public static AccessToken GetToken(string tokenUrls)
        {
            string result = "";
            try
            {
                //Write("begin get token " );
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(tokenUrls);
                webRequest.Method = "GET";
                webRequest.ContentType = "application/json";
                webRequest.KeepAlive = true;

                HttpWebResponse hwRes = webRequest.GetResponse() as HttpWebResponse;
                if (hwRes.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    //Write("  get token: "+ hwRes.StatusCode);
                    Stream rStream = hwRes.GetResponseStream();
                    StreamReader sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    //Write("get token:" + result);
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "Connect Error";
                    //Write("  get token: " + hwRes.StatusCode);
                }

                hwRes.Close();

            }
            catch (Exception ex)
            {
                //Write("get token:" + ex.Message);
            }
            return CommonJson.JsonDeserialize<AccessToken>(result);
        }

        //public static void Write(string textLine)
        //{
        //    var path = System.AppDomain.CurrentDomain.BaseDirectory + "\\filelist.txt";
        //    if (!System.IO.File.Exists(path))
        //    {
        //        FileStream stream = System.IO.File.Create(path);
        //        stream.Close();
        //        stream.Dispose();
        //    }
        //    using (StreamWriter writer = new StreamWriter(path, true))
        //    {
        //        writer.WriteLine(textLine);
        //    }
        //}

        public static Account TestAccount()
        {
            return new Account()
            {
                account = "testcarry",
                userid = "1"
            };
        }
    }
}
