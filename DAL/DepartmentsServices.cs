﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DAL.Common;
using Model;

namespace DAL
{
    public class DepartmentsServices
    {

        /// <summary>
        /// 3.根据父部门节点获取所有子部门信息
        /// </summary>
        /// <param name="strUrls"></param>
        /// <returns></returns>
        public static IList<DepartmentInfo> GetAllDepartments(string strUrls)
        {
            try
            {
                return HttpResponseInfo.RequestGetService<IList<DepartmentInfo>>(HttpContext.Current, strUrls);
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 4.根据部门ID获取该部门下的人员信息
        /// </summary>
        /// <param name="strUrls"></param>
        /// <returns></returns>
        public static IList<UserInfo> GetUsersByDeptId(string strUrls)
        {
            try
            {
                return HttpResponseInfo.RequestGetService<IList<UserInfo>>(HttpContext.Current, strUrls);
            }
            catch
            {
                return null;
            }
        }
    }
}
