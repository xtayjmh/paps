﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace DAL
{
    public class DatabaseServices
    {
        private static PapsEntities _entity = new PapsEntities();
        private static List<Menus> menus = _entity.Menus.AsNoTracking().ToList();
        /// <summary>
        /// 用户登录，如果用户名密码没有错误登录成功，更新客户机IP地址
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static Users Login(Users user)
        {
            using (var context = new PapsEntities())
            {
                var userModel = context.Users.FirstOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
                return userModel ?? null;
            }
        }
        public static Users GetUsers(int userId)
        {
            using (var db = new PapsEntities())
            {
                return db.Users.FirstOrDefault(p => p.UserId == userId);
            }
        }
        public static bool IsAdmin(int userId)
        {
            using (var db = new PapsEntities())
            {
                return db.RoleUser.Any(p => p.UserID == userId && p.RoleID == 1);
            }
        }
        /// <summary>
        /// 获取在线率
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="deptTxt"></param>
        /// <param name="workPoint"></param> 
        /// <returns></returns>
        public static List<OnlineRateModel> GetOnlineRate(string startTime, string endTime, string deptTxt, string workPoint)
        {
            using (var context = new PapsEntities())
            {
                var pStartTime = new SqlParameter { ParameterName = "@startTime", Value = startTime };
                var pEndTime = new SqlParameter { ParameterName = "@endTime", Value = endTime };
                var pDeptTxt = new SqlParameter { ParameterName = "@deptTxt", Value = deptTxt ?? "" };
                var pWorkPoint = new SqlParameter { ParameterName = "@workPoint", Value = workPoint ?? "" };
                var list = context.Database.SqlQuery<OnlineRateModel>("GetOnlineRate @startTime,@endTime,@deptTxt,@workPoint", pStartTime, pEndTime, pDeptTxt, pWorkPoint).ToList();
                return list;
            }
        }
        /// <summary>
        /// 客户机登陆
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int ClientLogin(Users user)
        {
            using (var c = new PapsEntities())
            {
                var userMode = c.Menus.FirstOrDefault(i => i.AccLoginName == user.UserName || i.GuestUserName == user.UserName);
                var model = c.OnlineRate.OrderByDescending(i => i.OnlineTime).FirstOrDefault(i => i.UserId == userMode.ID);
                var parent = c.Menus.FirstOrDefault(i => i.ID == userMode.ParentID);
                var parentName = parent?.Name;
                var grandParentName = c.Menus.FirstOrDefault(g => g.ID == parent.ParentID)?.Name; //线路级名称
                if (model != null && model.OfflineTime > DateTime.Now.AddMinutes(-6))
                {
                    model.OfflineTime = DateTime.Now;
                }
                else
                {
                    c.OnlineRate.Add(new OnlineRate()
                    {
                        OnlineTime = DateTime.Now,
                        OfflineTime = DateTime.Now.AddMinutes(5),
                        UserId = userMode.ID,
                        RegionName = grandParentName + "-" + parentName,
                        PointName = userMode.Name,
                    });
                }
                //客户端已开启，则表示在线，可以访问了
                if (userMode != null) userMode.IsActive = true;
                return c.SaveChanges();
            }
        }
        public class MenusMode : Menus
        {
            public List<MenusMode> ChildMenus { get; set; }
        }
        private static void Recursion(List<Menus> allMenus, List<TreeData> list)
        {
            list.ForEach(l =>
            {
                l.nodes = new List<TreeData>();
                var childItems = allMenus.Where(j => j.ParentID == int.Parse(l.tags)).ToList();
                if (childItems.Any())
                {
                    childItems.ForEach(i =>
                    {
                        l.nodes.Add(new TreeData()
                        {
                            text = i.Name,
                            id = i.ID,
                            href = "#",
                            tags = i.ID.ToString(),
                        });
                        Recursion(allMenus, l.nodes);
                    });
                }
                else
                {
                    l.nodes = null;
                }
            });
        }
        public static List<TreeData> GetMenus()
        {
            using (var db = new PapsEntities())
            {
                var menus = db.Menus.Where(i => !i.IsDel).ToList();
                var menusMode = new List<TreeData>();
                menus.Where(m => m.ParentID == 0 && !m.IsDel).ToList().ForEach(i =>
                {
                    menusMode.Add(new TreeData
                    {
                        text = i.Name,
                        id = i.ID,
                        href = "#",
                        tags = i.ID.ToString(),
                    });
                });
                Recursion(menus, menusMode);
                return menusMode;
            }
        }
        public static List<RoleMenu> GetRoleMenu(string roleId)
        {
            using (var db = new PapsEntities())
            {
                var rId = int.Parse(roleId);
                var list = db.RoleMenu.Where(i => i.RoleID == rId).ToList();
                return list;
            }
        }

        public static List<RoleMenu> GetRoleMenuByUserId(string userId)
        {
            using (var db = new PapsEntities())
            {
                var rId = int.Parse(userId);
                var rList = db.RoleUser.Where(i => i.UserID == rId).Select(i => i.RoleID);//获取用户的角色
                var list = db.RoleMenu.Where(i => rList.Contains(i.RoleID)).ToList();//根据用户的角色获取对应的菜单ID
                return list;
            }
        }

        public static RealtimeWatchModel GetRealtimeWatch(string type, string workProcedure, string position)
        {
            var model = new RealtimeWatchModel()
            {
                Persons = new List<PersonModel>() { },
                TunnelFaceNumber = 2,
                TunnelNumber = 10
            };
            return model;
        }

        /// <summary>
        /// 获取在岗时长统计信息
        /// </summary>
        /// <param name="workProcedure"></param>
        /// <param name="position"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static List<string> GetOnDutySummary(string workStepName, int year, int menuId)
        {
            var db = new PapsEntities();
            var ids = new List<int?>();
            var listIds = RecGetIds(menuId, ids); //获取当前选择节点下边的所有工点id
            var devices = db.Device.Where(d => d.MenuId != null && listIds.Contains(d.MenuId)).Select(d => d.DeviceCode).ToList();
            var list = db.OnDutyData.Where(d => devices.Contains(d.DeviceCode) && d.SearchData == year && d.WorkStepName == workStepName).Select(d => d.ResultData).ToList();
            return list;
        }
        /// <summary>
        /// 获取当前菜单包含的子菜单
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private static List<int?> RecGetIds(int menuId, List<int?> ids)
        {
            var child = menus.Where(m => m.ParentID == menuId).ToList();
            if (child.Any())
            {
                child.ForEach(c =>
                {
                    ids.Add(c.ID);
                    RecGetIds(c.ID, ids);
                });
            }
            return ids;
        }

        /// <summary>
        /// 获取隧道内时长累计信息
        /// </summary>
        /// <param name="year"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public static List<string> GetInTunnelSummary(int year, int menuId)
        {
            var db = new PapsEntities();
            var result = new List<string>();
            var ids = new List<int?>();
            var listIds = RecGetIds(menuId, ids);
            var devices = db.Device.Where(d => d.MenuId != null && listIds.Contains(d.MenuId)).Select(d => d.DeviceCode).ToList();
            var list = db.ChartData.Where(c => devices.Contains(c.DeviceCode) && c.SearchData == year).ToList();
            if (list.Any())
            {
                list.ForEach(l =>
                {
                    result.Add(l.ResultData);
                });
            }
            return result;
        }

        public static bool SyncWorkStep(List<WorkStep> list)
        {
            if (list == null || !list.Any()) return false;
            //工序名字为空的不同步
            list.ForEach(w =>
            {
                var model = _entity.WorkStep.FirstOrDefault(i => i.ClientWorkStepId == w.ClientWorkStepId && i.DeviceCode == w.DeviceCode);
                if (model == null && !string.IsNullOrEmpty(w.WorkStepName))
                {
                    _entity.WorkStep.Add(w);
                }
                else
                {
                    if (model != null)
                    {
                        model.WorkStepName = w.WorkStepName;
                    }
                }
            });
            return _entity.SaveChanges() > 0;
        }

        public static List<string> GetWorkStep(int menuId)
        {
            var idList = new List<int?>();
            var ids = RecGetIds(menuId, idList);
            var db = new PapsEntities();
            var devices = db.Device.Where(d => d.MenuId != null && ids.Contains(d.MenuId)).Select(d => d.DeviceCode).ToList();
            var list = _entity.WorkStep.Where(w => devices.Contains(w.DeviceCode)).Select(w => w.WorkStepName).Distinct().ToList();
            return list;
        }

        public static bool SyncWorkStepLog(List<WorkStepLog> list)
        {
            if (list == null || !list.Any()) return false;
            list.ForEach(r =>
            {
                //跳过重复数据
                if (_entity.WorkStepLog.FirstOrDefault(w => w.StartTime == r.StartTime && w.EndTime == r.EndTime && w.DeviceCode == r.DeviceCode && w.WorkStepId == r.WorkStepId) != null) return;
                if (r.EndTime == DateTime.MinValue) return;
                _entity.WorkStepLog.Add(new WorkStepLog
                {
                    StartTime = r.StartTime,
                    EndTime = r.EndTime,
                    ClientId = r.ClientId,
                    WorkStepId = r.WorkStepId,
                    DeviceCode = r.DeviceCode
                });
            });
            return _entity.SaveChanges() > 0;
        }
    }
}