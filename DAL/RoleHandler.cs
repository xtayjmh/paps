﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Model;

namespace DAL
{
    public static class RoleHandler
    {
        /// <summary>
        /// 保存部门-角色的关系
        /// </summary>
        public static bool SaveDeptRoleRelation(List<int> deptIds, List<int> roleIds)
        {
            using (var db = new PapsEntities())
            {
                var ids = string.Join(",", roleIds);
                deptIds.ForEach(i =>
                {
                    var mode = db.DeptRoleRelation.FirstOrDefault(d => d.DeptId == i);
                    if (mode != null)
                    {
                        mode.RoleIds = ids;
                        db.Entry(mode).State = EntityState.Modified;
                    }
                    else
                    {
                        db.DeptRoleRelation.Add(new DeptRoleRelation() { RoleIds = ids, DeptId = i });
                    }
                });//先建立部门，子部门与与角色的关系
                var users = db.DepartmentUsers.Where(i => deptIds.Contains(i.DepartID)).ToList();//获取该部门下所有用户，包括子部门的用户

                if (users.Any())
                {
                    users.ForEach(i =>
                    {
                        var user = db.RoleUser.Where(u => u.UserID == i.UserID).ToList();
                        user.ForEach(u =>
                        {
                            db.RoleUser.Remove(u);
                        });

                        roleIds.ForEach(j =>
                        {
                            db.RoleUser.Add(new RoleUser() { IsDel = false, RoleID = j, UserID = i.UserID });
                        });
                    });
                }
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 递归获取部门id
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public static List<int> RecursionDepts(int deptId)
        {
            var deptIds = new List<int> { deptId };
            using (var db = new PapsEntities())
            {
                var childIds = db.Departments.Where(i => i.ParentID == deptId).Select(i => i.ID).ToList();
                if (childIds.Any())
                {
                    childIds.ForEach(i => { deptIds.Add(i); });
                    RecursionChildDepts(deptIds, childIds);
                }

                return deptIds;
            }
        }

        private static void RecursionChildDepts(List<int> ids, List<int> childIds)
        {
            using (var db = new PapsEntities())
            {
                childIds.ForEach(i =>
                {
                    var grandChildIds = db.Departments.Where(j => j.ParentID == i).Select(j => j.ID).ToList();
                    if (grandChildIds.Any())
                    {
                        grandChildIds.ForEach(ids.Add);
                        RecursionChildDepts(ids, grandChildIds);
                    }
                });
            }
        }

        /// <summary>
        /// 获取所有拥有顶级菜单权限的用户
        /// </summary>
        public static void GetAllUsers()
        {
            using (var db=new PapsEntities())
            {
                var roleMenus = db.RoleMenu.Where(i => i.MenuID == 1).Select(i=>i.RoleID);//获取所有用户定级菜单权限的角色Id
                if (roleMenus.Any())
                {
                    var roleUsers = db.RoleUser.Where(i => roleMenus.Contains(i.RoleID)).Select(i => i.UserID); //获取所有拥有顶级菜单权限的用户的Id
                }
            }
        }
    }
}
