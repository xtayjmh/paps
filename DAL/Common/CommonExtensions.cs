﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Common
{
    public static class CommonExtensions
    {
        public static bool IsNull(this object target)
        {
            return target == null;
        }
        public static string ToStringOrNull(this object target)
        {
            return target.IsNull() ? null : target.ToString();
        }
        /// <summary>
        /// MD516位加密
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string ToStringShortMD5(this object target)
        {
            return target.IsNull() ? "" : Encryption.GetMD5(target.ToString());
        }
        /// <summary>
        /// 字符串加密
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string ToStringEncrypt(this object target)
        {
            return target.IsNull() ? "" : Encryption.Encrypt(target.ToString());
        }
        /// <summary>
        /// 字符串解密
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string ToStringDecrypt(this object target)
        {
            return target.IsNull() ? "" : Encryption.Decrypt(target.ToString().Replace(' ', '+'));
        }
    }
}
