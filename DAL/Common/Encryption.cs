﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Common
{
    public static class Encryption
    {

        public static string GetMD5(string strSource)
        {
            //new 
            MD5 md5 = new MD5CryptoServiceProvider();
            //获取密文字节数组
            byte[] bytResult = md5.ComputeHash(System.Text.Encoding.GetEncoding("MD5").GetBytes(strSource));
            //转换成字符串，并取9到25位 
            string strResult = BitConverter.ToString(bytResult, 4, 8);
            //转换成字符串，32位
            //string strResult = BitConverter.ToString(bytResult);
            //BitConverter转换出来的字符串会在每个字符中间产生一个分隔符，需要去除掉 
            strResult = strResult.Replace("-", "");
            return strResult.ToLower();
        }



        //secret key
        public const string Key = "shinetec";//加密密钥必须为8位


        public static string Encrypt(string str)
        {
            string m_Need_Encode_String = str;
            if (m_Need_Encode_String == null)
            {
                throw new Exception("Error: \n String is empty！！");
            }
            var arrDESKey = Encoding.UTF8.GetBytes(Key);
            var arrDESIV = Encoding.UTF8.GetBytes(Key);
            DESCryptoServiceProvider objDES = new DESCryptoServiceProvider();
            MemoryStream objMemoryStream = new MemoryStream();
            CryptoStream objCryptoStream = new CryptoStream(objMemoryStream, objDES.CreateEncryptor(arrDESKey, arrDESIV), CryptoStreamMode.Write);
            StreamWriter objStreamWriter = new StreamWriter(objCryptoStream);
            objStreamWriter.Write(m_Need_Encode_String);
            objStreamWriter.Flush();
            objCryptoStream.FlushFinalBlock();
            objMemoryStream.Flush();

            return Convert.ToBase64String(objMemoryStream.GetBuffer(), 0, (int)objMemoryStream.Length);
        }


        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="m_Need_Encode_String"></param>
        /// <returns></returns>
        public static string Decrypt(string str)
        {
            string m_Need_Encode_String = str;
            if (m_Need_Encode_String == null)
            {
                throw new Exception("Error: \n String is empty！！");
            }
            var arrDESKey = Encoding.UTF8.GetBytes(Key);
            var arrDESIV = Encoding.UTF8.GetBytes(Key);
            DESCryptoServiceProvider objDES = new DESCryptoServiceProvider();
            byte[] arrInput = Convert.FromBase64String(m_Need_Encode_String);
            MemoryStream objMemoryStream = new MemoryStream(arrInput);
            CryptoStream objCryptoStream = new CryptoStream(objMemoryStream, objDES.CreateDecryptor(arrDESKey, arrDESIV), CryptoStreamMode.Read);
            StreamReader objStreamReader = new StreamReader(objCryptoStream);
            return objStreamReader.ReadToEnd();
        }




    }
}
