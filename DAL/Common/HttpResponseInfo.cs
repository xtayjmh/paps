﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace DAL.Common
{
    public class HttpResponseInfo
    {
        /// <summary>
        /// Get请求接口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context"></param>
        /// <param name="strUrl"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static A RequestGetService<A>(HttpContext context, string strUrl)
        {
            //声明调用webapi的方法主体
            HttpClient client = new HttpClient();
            //
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //同步调用webapi
            var response = client.GetAsync(strUrl).Result;
            //接收api返回的消息状态
            if (!response.IsSuccessStatusCode)
            {
                context.Response.Write(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase, response.RequestMessage));
                return default(A);
            }
            else
            {
                //接收webapi返回的数据
                var strResult = response.Content.ReadAsStringAsync().Result;
                //把接收的数据解密，并且序列化成实体类
                return CommonJson.JsonDeserialize<A>(strResult);
            }
        }
 

    }
}
