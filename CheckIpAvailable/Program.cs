﻿using Model;
using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace CheckIpAvailable
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new PapsEntities())
            {
                var menus = db.Menus.Where(i => i.ItemIp != "" && !i.IsDel);
                var ping = new Ping();
                var AllMenus = db.Menus.ToList();
                var pingTime = DateTime.Now;//请求之前获取当前时间，防止数据多的话，ping不通需要很长的等待时间。
                menus.ToList().ForEach(i =>
                {
                    try
                    {
                        var ip = GetIpAddress(i.ItemIp);
                        var pingReply = ping.Send(ip);
                        i.IsActive = pingReply != null && pingReply.Status == IPStatus.Success;
                        Console.WriteLine($"请求地址：{ip}，请求状态：{pingReply.Status}，是否可用：{i.IsActive}");


                        if (!i.IsActive) return;
                        var model = db.OnlineRate.OrderByDescending(o => o.OnlineTime).FirstOrDefault(o => o.UserId == i.ID);
                        var parent= AllMenus.FirstOrDefault(m => m.ID == i.ParentID);

                        if (model == null)
                        {
                            db.OnlineRate.Add(new OnlineRate()
                            {
                                OnlineTime = pingTime,
                                OfflineTime = pingTime.AddMinutes(30),
                                UserId = i.ID,
                                RegionName = parent?.Name,
                                RegionId = parent?.ID,
                                PointName = i.Name,
                            });
                        }
                        //如果离线时间小于当前时间的30分钟以前，说明已经离线了，需要新增一条新的在线数据
                        else if (model.OfflineTime < pingTime.AddMinutes(-30))
                        {
                            db.OnlineRate.Add(new OnlineRate()
                            {
                                OnlineTime = pingTime,
                                OfflineTime = pingTime.AddMinutes(30),
                                UserId = i.ID,
                                RegionName = parent?.Name,
                                RegionId = parent?.ID,
                                PointName = i.Name,
                            });
                        }
                        //在线有数据，更新离线时间为30分钟以后
                        else
                        {
                            model.OfflineTime = pingTime.AddMinutes(30);
                        }
                    }
                    catch
                    {
                        // ignore
                    }
                });
                db.SaveChanges();
            }
        }

        private static string GetIpAddress(string ipStr)
        {
            if (ipStr.Contains("http")) ipStr = ipStr.Replace("http://", "");//去掉http，如果有的话
            if (ipStr.Contains("/")) ipStr = ipStr.Substring(0, ipStr.LastIndexOf('/'));//去掉service的名字
            if (ipStr.Contains(":")) ipStr = ipStr.Substring(0, ipStr.LastIndexOf(':'));//去掉端口号
            return ipStr.Trim();
        }
    }
}
